//
//  PIMDModuleSelectionViewController.m
//  ValleyGroup
//
//  Created by Rishi on 30/04/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import "PIMDModuleSelectionViewController.h"
#import "PIMDInstallationStoreListViewController.h"
#import "PIMDRecceStoreListViewController.h"
#import "constants.h"

@interface PIMDModuleSelectionViewController ()

@end

@implementation PIMDModuleSelectionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if ([self respondsToSelector:NSSelectorFromString(@"edgesForExtendedLayout")]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    [self.navigationItem setHidesBackButton:YES];

    
    UIBarButtonItem *logoutBtn = [[UIBarButtonItem alloc] initWithTitle:@"Logout" style:UIBarButtonItemStylePlain target:self action:@selector(logoutBtnClicked:)];
    self.navigationItem.rightBarButtonItem = logoutBtn;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)logoutBtnClicked:(id)sender
{
    UIAlertView *logoutAlert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"Are you sure you want to logout ?" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"NO", nil];
    logoutAlert.tag = 1;
    [logoutAlert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1) {
        NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
        if([title isEqualToString:@"Yes"])
        {
            [PIMDAnalyticsLogger logEvent:@"Frontline_Logout_Btn_Pressed"];
            [PIMDGlobalOperations clearFrontlineMemberDetails];
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }
}

- (IBAction)installationModuleBtnPressed:(id)sender {
    [PIMDAnalyticsLogger logEvent:@"Installation_Module_Btn_Pressed"];
    PIMDInstallationStoreListViewController *storelistController = [[PIMDInstallationStoreListViewController alloc] initWithNibName:@"PIMDInstallationStoreListViewController" bundle:nil] ;
    storelistController.title = @"Store Status";
    [self.navigationController pushViewController:storelistController animated:YES];
}

- (IBAction)recceModuleBtnPressed:(id)sender {
    [PIMDAnalyticsLogger logEvent:@"Recce_Module_Btn_Pressed"];
    PIMDRecceStoreListViewController *storelistController = [[PIMDRecceStoreListViewController alloc] initWithNibName:@"PIMDRecceStoreListViewController" bundle:nil] ;
    storelistController.title = @"Store Status";
    [self.navigationController pushViewController:storelistController animated:YES];
}
@end
