//
//  CommonCallback.m
//  ImageOrganiser
//
//  Created by Kirti Nikam on 13/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "CommonCallback.h"
#import "constants.h"

//#import "UAPush.h"

@implementation CommonCallback
+(BOOL) use24HourClock
{
    //if 1  means 12 Hour clock
    //if 0  means 24 Hour clock
    
    BOOL using24HourClock = NO;
    
    NSString *formatStringForHours = [NSDateFormatter dateFormatFromTemplate:@"j" options:0 locale:[NSLocale currentLocale]];
    
    NSRange containsA = [formatStringForHours rangeOfString:@"a"];
    
    using24HourClock = containsA.location != NSNotFound;
    
    return using24HourClock;
}

+(UIImage *)getFlatImage:(UIColor *)color
{
    /* Create a DeviceRGB color space. */
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    /* Create a bitmap context. The context draws into a bitmap which is `width'
     pixels wide and `height' pixels high*/
    CGContextRef composedImageContext = CGBitmapContextCreate(NULL,
                                                              10,
                                                              10,
                                                              8,
                                                              10*4,
                                                              colorSpace,
                                                              kCGImageAlphaPremultipliedFirst);
    
    CGColorSpaceRelease(colorSpace);
    
    
    CGContextSetFillColorWithColor(composedImageContext, [color CGColor]);
    CGContextFillRect(composedImageContext, CGRectMake(0, 0, 10, 10));
    /* Return an image containing a snapshot of the bitmap context `context'.*/
    CGImageRef cgImage = CGBitmapContextCreateImage(composedImageContext);
    
    return [UIImage imageWithCGImage:cgImage];
}
+(void)roundeWithBorder:(UIView *)view
{
    view.layer.borderColor = [UIColor grayColor].CGColor;
    view.layer.borderWidth = 2.0;
    view.layer.cornerRadius = 5.0;
}
+(void)addShadow:(UIView *)view
{
    [view.layer setShadowColor:[[UIColor blackColor] CGColor]];
    [view.layer setShadowOffset:CGSizeMake(0, 3)];
    [view.layer setShadowOpacity:0.125];
    [view.layer setCornerRadius:3];
}

#pragma mark -
#pragma mark - Seperator
+(UIImage *)separatorImage
{
    UIGraphicsBeginImageContext(CGSizeMake(1, 4));
    CGContextRef context = UIGraphicsGetCurrentContext();
    UIGraphicsPushContext(context);
    CGContextSetFillColorWithColor(context, [UIColor colorWithRed:28/255.0 green:28/255.0 blue:27/255.0 alpha:1].CGColor);
    CGContextFillRect(context, CGRectMake(0, 0, 1, 2));
    CGContextSetFillColorWithColor(context, [UIColor colorWithRed:79/255.0 green:79/255.0 blue:77/255.0 alpha:1].CGColor);
    CGContextFillRect(context, CGRectMake(0, 3, 1, 2));
    UIGraphicsPopContext();
    UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return [UIImage imageWithCGImage:outputImage.CGImage scale:2.0 orientation:UIImageOrientationUp];
}

+ (UIImage *)fixOrientation:(UIImage *)oldImage {
    
    // No-op if the orientation is already correct
    if (oldImage.imageOrientation == UIImageOrientationUp) return oldImage;
    
    // We need to calculate the proper transformation to make the image upright.
    // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (oldImage.imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, oldImage.size.width, oldImage.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, oldImage.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
            
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, oldImage.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationUpMirrored:
            break;
    }
    
    switch (oldImage.imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, oldImage.size.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
            
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, oldImage.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationDown:
        case UIImageOrientationLeft:
        case UIImageOrientationRight:
            break;
    }
    
    // Now we draw the underlying CGImage into a new context, applying the transform
    // calculated above.
    CGContextRef ctx = CGBitmapContextCreate(NULL, oldImage.size.width, oldImage.size.height,
                                             CGImageGetBitsPerComponent(oldImage.CGImage), 0,
                                             CGImageGetColorSpace(oldImage.CGImage),
                                             CGImageGetBitmapInfo(oldImage.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (oldImage.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            // Grr...
            CGContextDrawImage(ctx, CGRectMake(0,0,oldImage.size.height,oldImage.size.width), oldImage.CGImage);
            break;
            
        default:
            CGContextDrawImage(ctx, CGRectMake(0,0,oldImage.size.width,oldImage.size.height), oldImage.CGImage);
            break;
    }
    
    // And now we just create a new UIImage from the drawing context
    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
    UIImage *img = [UIImage imageWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    return img;
}

+(UIImage *)resizeImage:(UIImage*)image newSize:(CGSize)newSize {
    CGRect newRect = CGRectIntegral(CGRectMake(0, 0, newSize.width, newSize.height));
    CGImageRef imageRef = image.CGImage;
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Set the quality level to use when rescaling
    CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
    CGAffineTransform flipVertical = CGAffineTransformMake(1, 0, 0, -1, 0, newSize.height);
    
    CGContextConcatCTM(context, flipVertical);
    // Draw into the context; this scales the image
    CGContextDrawImage(context, newRect, imageRef);
    
    // Get the resized image from the context and a UIImage
    CGImageRef newImageRef = CGBitmapContextCreateImage(context);
    UIImage *newImage = [UIImage imageWithCGImage:newImageRef];
    
    CGImageRelease(newImageRef);
    UIGraphicsEndImageContext();
    
    return newImage;
}


#pragma mark -
#pragma mark - Progress HUD
+(void)showProgressHud:(NSString *)title subtitle:(NSString *)subTitle{
    //random color
    //    CGFloat red =  arc4random_uniform(256)/255.f;
    //    CGFloat blue = arc4random_uniform(256)/255.f;
    //    CGFloat green = arc4random_uniform(256)/255.f;
    //    CGColorRef color = CGColorRetain([UIColor colorWithRed:red green:green blue:blue alpha:1.0].CGColor);
    //    [[[MMProgressHUD sharedHUD] overlayView] setOverlayColor:color];
    //    CGColorRelease(color);
    [MMProgressHUD setPresentationStyle:MMProgressHUDPresentationStyleShrink];
    [MMProgressHUD showWithTitle:title status:subTitle];
}
+(void)showProgressHudWithCancel:(NSString *)title subtitle:(NSString *)subTitle
{
    [MMProgressHUD setPresentationStyle:MMProgressHUDPresentationStyleShrink];
    [MMProgressHUD showWithTitle:title
                          status:@""
             confirmationMessage:[NSString stringWithFormat:@"Cancel %@?",subTitle]
                     cancelBlock:^{
                         DebugLog(@"Task was cancelled!");
                     }];
    
}

+(void)hideProgressHudWithSuccess
{
    [MMProgressHUD dismissWithSuccess:@"Success!"];
}

+(void)hideProgressHudWithError
{
    [MMProgressHUD dismissWithError:@"Error!"];
}
+(void)hideProgressHud
{
    [MMProgressHUD dismiss];
}

+(BOOL)validateEmail: (NSString *) email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    BOOL isValid = [emailTest evaluateWithObject:email];
    return isValid;
}



#pragma animation

+(void)viewtransitionInCompletion:(UIView *)view completion:(void(^)(void))lcompletion{
    CGFloat y = view.center.y;
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"position.y"];
    animation.values = @[@(y - view.bounds.size.height), @(y + 20), @(y - 10), @(y)];
    animation.keyTimes = @[@(0), @(0.5), @(0.75), @(1)];
    animation.timingFunctions = @[[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut], [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear], [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
    animation.duration = 0.4;
    animation.delegate = self;
    [animation setValue:lcompletion forKey:@"handler"];
    [view.layer addAnimation:animation forKey:@"dropdown"];
}

+(void)viewtransitionOutCompletion:(UIView *)view completion:(void(^)(void))completion
{
    CGPoint point = view.center;
    point.y += view.bounds.size.height;
    [UIView animateWithDuration:0.3
                          delay:0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         view.center = point;
                         CGFloat angle = ((CGFloat)arc4random_uniform(100) - 50.f) / 100.f;
                         view.transform = CGAffineTransformMakeRotation(angle);
                     }
                     completion:^(BOOL finished) {
                         if (completion) {
                             completion();
                         }
                     }];
}
+(void)teardown:(UIView *)view
{
    [view removeFromSuperview];
    view = nil;
}

@end
