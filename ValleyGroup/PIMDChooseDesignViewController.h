//
//  PIMDChooseDesignViewController.h
//  ValleyGroup
//
//  Created by Rishi on 05/05/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PIMDChooseDesignViewController : UIViewController
@property (nonatomic, copy) NSString *passedstoreid;
@property (strong, nonatomic) NSMutableArray *designArray;
@property (strong, nonatomic) IBOutlet UITableView *chooseDesignTableView;
@end
