//
//  PIMDStoreObject.h
//  ValleyGroup
//
//  Created by Rishi on 21/04/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PIMDStoreObject : NSObject

@property (nonatomic, copy) NSString *storeid;
@property (nonatomic, copy) NSString *storename;
@property (nonatomic, copy) NSString *storecity;
@property (nonatomic, copy) NSString *storestatus;
@property (nonatomic, copy) NSString *storeAddress;
@property (nonatomic, copy) NSString *storeDescription;
@property (nonatomic, copy) NSString *projectid;
@property (nonatomic, copy) NSString *projectname;
@end
