//
//  PIMDMapViewController.m
//  ValleyGroup
//
//  Created by Rishi on 26/03/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import "PIMDMapViewController.h"

@interface PIMDMapViewController ()

@end

@implementation PIMDMapViewController
@synthesize locationPickerView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // The LocationPickerView can be created programmatically (see below) or
    // using Storyboards/XIBs (see Storyboard file).
    self.locationPickerView = [[LocationPickerView alloc] initWithFrame:self.view.bounds];
    self.locationPickerView.tableViewDataSource = self;
    self.locationPickerView.tableViewDelegate = self;
    
    // Optional parameters
    self.locationPickerView.delegate = self;
    self.locationPickerView.shouldAutoCenterOnUserLocation = YES;
    self.locationPickerView.shouldCreateHideMapButton = YES;
    self.locationPickerView.pullToExpandMapEnabled = YES;
    self.locationPickerView.defaultMapHeight = 220.0;           // larger than normal
    self.locationPickerView.parallaxScrollFactor = 0.3;         // little slower than normal.
    
    // Optional setup
    self.locationPickerView.mapViewDidLoadBlock = ^(LocationPickerView *locationPicker) {
        locationPicker.mapView.mapType = MKMapTypeStandard;
        locationPicker.mapView.userTrackingMode = MKUserTrackingModeFollow;
    };
    self.locationPickerView.tableViewDidLoadBlock = ^(LocationPickerView *locationPicker) {
        locationPicker.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    };
    
    
     [self performSelector:@selector(getLocationValues) withObject:nil afterDelay:2.0];
    // set custom close button
    /*
     UIButton *customCloseButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
     [customCloseButton setTitle:@"close" forState:UIControlStateNormal];
     [customCloseButton setBackgroundColor:[UIColor blueColor]];
     [customCloseButton setFrame:CGRectMake(0, 0, 80, 80)];
     [self.locationPickerView setCustomCloseButton:customCloseButton atPoint:CGPointMake(100, 400)];
     */
    
    [self.view addSubview:self.locationPickerView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)getLocationValues
{
    if ([CLLocationManager locationServicesEnabled])
    {
        //        [self showProgressHUDWithMessage:@"Please wait ... \n shoplocal is trying to find your location."];
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        locationManager.distanceFilter = kCLDistanceFilterNone;
        [locationManager startUpdatingLocation];
        [self performSelector:@selector(stopUpdatingCoreLocation:) withObject:@"Timed Out" afterDelay:15];
    }
    else
    {
        UIAlertView *servicesDisabledAlert = [[UIAlertView alloc] initWithTitle:@"Valley Group" message:@"Location serviecs are disabled on your device, shoplocal needs your location to bring you information from nearby, please enable location services." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [servicesDisabledAlert show];
    }
}

#pragma mark -
#pragma mark CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations
{
    //[CommonCallback showProgressHud:@"Loading" subtitle:@""];
    CLLocation* newLocation = [locations lastObject];
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(newLocation.coordinate, 800, 800);
    [locationPickerView.mapView setRegion:[locationPickerView.mapView regionThatFits:region] animated:YES];
    
}


- (void) locationManager:(CLLocationManager *) manager didFailWithError:(NSError *) error
{
    DebugLog(@"Error: %@", [error description]);
	switch ([error code])
	{
		case kCLErrorDenied:
			//[errorString appendFormat:@"%@\n", NSLocalizedString(@"LocationDenied", nil)];
			break;
			
			
		case kCLErrorLocationUnknown:
			//[errorString appendFormat:@"%@\n", NSLocalizedString(@"LocationUnknown", nil)];
			break;
			
		case kCLErrorNetwork:
			//[errorString appendFormat:@"%@\n", NSLocalizedString(@"LocationUnknown", nil)];
			break;
            
		case kCLErrorHeadingFailure:
			//[errorString appendFormat:@"%@\n", NSLocalizedString(@"LocationUnknown", nil)];
			break;
			
		default:
			//[errorString appendFormat:@"%@ %d\n", NSLocalizedString(@"GenericLocationError", nil), [error code]];
			break;
	}
    [self stopUpdatingCoreLocation:nil];
    //[CommonCallback hideProgressHud];
}

- (void)stopUpdatingCoreLocation:(NSString *)state
{
    //    [self hideProgressHUD:YES];
    [locationManager stopUpdatingLocation];
    //[self saveCurrentLocation:[locationManager location]];
    // Add an annotation
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    point.coordinate = [locationManager location].coordinate;
    point.title = @"You are here";
    //point.subtitle = @"I'm here!!!";
    [locationPickerView.mapView addAnnotation:point];
}


#pragma mark - UITableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 20;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reusable"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"reusable"];
    }
    
    switch (indexPath.row) {
        case 0:
        {
            cell.textLabel.text = [NSString stringWithFormat:@"%ld. Delhi", (long)indexPath.row];
        }
            break;
            
        case 1:
        {
            cell.textLabel.text = [NSString stringWithFormat:@"%ld. Mumbai", (long)indexPath.row];
        }
            break;
            
        case 2:
        {
            cell.textLabel.text = [NSString stringWithFormat:@"%ld. Chennai", (long)indexPath.row];
        }
            break;
            
        case 3:
        {
            cell.textLabel.text = [NSString stringWithFormat:@"%ld. Banglore", (long)indexPath.row];
        }
            break;
            
        case 4:
        {
            cell.textLabel.text = [NSString stringWithFormat:@"%ld. Kolkata", (long)indexPath.row];
        }
            break;
            
        case 5:
        {
            cell.textLabel.text = [NSString stringWithFormat:@"%ld. Jaipur", (long)indexPath.row];
        }
            break;
            
        case 6:
        {
            cell.textLabel.text = [NSString stringWithFormat:@"%ld. Lucknow", (long)indexPath.row];
        }
            break;
            
        case 7:
        {
            cell.textLabel.text = [NSString stringWithFormat:@"%ld. Nagpur", (long)indexPath.row];
        }
            break;
            
        case 8:
        {
            cell.textLabel.text = [NSString stringWithFormat:@"%ld. Kolkata", (long)indexPath.row];
        }
            break;
            
        case 9:
        {
            cell.textLabel.text = [NSString stringWithFormat:@"%ld. Kolkata", (long)indexPath.row];
        }
            break;
            
        case 10:
        {
            cell.textLabel.text = [NSString stringWithFormat:@"%ld. Kolkata", (long)indexPath.row];
        }
            break;
        

            
        default:
            break;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.contentView.backgroundColor = [UIColor whiteColor];
}

#pragma mark - LocationPickerViewDelegate

/** Called when the mapView is about to be expanded (made fullscreen).
 Use this to perform custom animations or set attributes of the map/table. */
- (void)locationPicker:(LocationPickerView *)locationPicker
     mapViewWillExpand:(MKMapView *)mapView
{
    self.navigationItem.title = @"Map Expanding";
}

/** Called when the mapView was expanded (made fullscreen). Use this to
 perform custom animations or set attributes of the map/table. */
- (void)locationPicker:(LocationPickerView *)locationPicker
      mapViewDidExpand:(MKMapView *)mapView
{
    self.navigationItem.title = @"Map Expanded";
}

/** Called when the mapView is about to be hidden (made tiny). Use this to
 perform custom animations or set attributes of the map/table. */
- (void)locationPicker:(LocationPickerView *)locationPicker
   mapViewWillBeHidden:(MKMapView *)mapView
{
    self.navigationItem.title = @"Map Shrinking";
}

/** Called when the mapView was hidden (made tiny). Use this to
 perform custom animations or set attributes of the map/table. */
- (void)locationPicker:(LocationPickerView *)locationPicker
      mapViewWasHidden:(MKMapView *)mapView
{
    self.navigationItem.title = @"Map Normal";
}

- (void)locationPicker:(LocationPickerView *)locationPicker mapViewDidLoad:(MKMapView *)mapView
{
    mapView.mapType = MKMapTypeStandard;
}

- (void)locationPicker:(LocationPickerView *)locationPicker tableViewDidLoad:(UITableView *)tableView
{
    tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
}


@end
