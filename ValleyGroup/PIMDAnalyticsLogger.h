//
//  PIMDAnalyticsLogger.h
//  ValleyGroup
//
//  Created by Rishi on 27/05/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Flurry.h"
#import "LocalyticsSession.h"

@interface PIMDAnalyticsLogger : NSObject

+(void)logEvent:(NSString*) eventName;
+(void)logEvent:(NSString*) eventName  withParams:(NSDictionary*) eventParams;
+(void)setUser:(NSString*) eventName;
+(void)setGender:(NSString*) gender;
+(void)setAge:(int) age;
+(void)logPageViews:(id)navigationController;
+(void)logLocationValues:(CLLocation *)location;
+ (void)localyticsSessionWillResignActive;
+ (void)localyticsSessionDidEnterBackground;
+ (void)localyticsSessionWillEnterForeground;
+ (void)localyticsSessionDidBecomeActive:(NSString *)appId;
+ (void)localyticsSessionWillTerminate;

@end
