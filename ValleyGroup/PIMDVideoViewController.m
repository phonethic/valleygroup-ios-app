//
//  PIMDVideoViewController.m
//  ValleyGroup
//
//  Created by Rishi on 22/04/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import "PIMDVideoViewController.h"
#import "CommonCallback.h"
#import "constants.h"
#import "PIMDAppDelegate.h"
#import "PIMDTransitObject.h"
#import "PIMDMoreViewController.h"

#define GET_STORE_STATUS(ID) [NSString stringWithFormat:@"%@%@%@store_api/store_status?store_id=%@",LIVE_SERVER,URL_PREFIX,API_VERSION,ID]

@interface PIMDVideoViewController ()
@property(strong) NSDictionary *splashJson;
@end

@implementation PIMDVideoViewController
@synthesize videoArray;
@synthesize videoWebView;
@synthesize passedstoreid;
@synthesize moreBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if ([self respondsToSelector:NSSelectorFromString(@"edgesForExtendedLayout")]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    moreBtn.titleLabel.font    = DEFAULT_BOLD_FONT(20);

    videoWebView.scalesPageToFit = TRUE;
    
    videoArray = [[NSMutableArray alloc] init];
    
    if ([PIMD_APP_DELEGATE networkavailable]) {
        [self sendStoreTransitRequest];
    } else {
        [PIMDGlobalOperations showOfflineAlert];
    }
    
}


-(void)sendStoreTransitRequest
{
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:GET_STORE_STATUS(passedstoreid)]];
    DebugLog(@"%@",GET_STORE_STATUS(passedstoreid));
    [urlRequest setValue:API_KEY forHTTPHeaderField:@"X-API-KEY"];
    [urlRequest setValue:[PIMDGlobalOperations getCustomerAuthId] forHTTPHeaderField:@"customer_id"];
    [urlRequest setValue:[PIMDGlobalOperations getCustomerAuthCode] forHTTPHeaderField:@"auth_id"];
    DebugLog(@"%@",[PIMDGlobalOperations getCustomerAuthId]);
    DebugLog(@"%@",[PIMDGlobalOperations getCustomerAuthCode]);
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: urlRequest
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        [CommonCallback hideProgressHud];
                                                        DebugLog(@"%@",self.splashJson);
                                                        if(self.splashJson  != nil)
                                                        {
                                                            [videoArray removeAllObjects];
                                                            if([[self.splashJson  objectForKey:@"success"] isEqualToString:@"true"])
                                                            {
                                                                NSArray *dataArray = [self.splashJson  objectForKey:@"data"];
                                                                for (int index = 0; index < [dataArray count]; index++) {
                                                                    NSDictionary *dict = [dataArray objectAtIndex:index];
                                                                    PIMDTransitObject *statusObj = [[PIMDTransitObject alloc] init];
                                                                    statusObj.storeid = [dict objectForKey:@"store_id"];
                                                                    statusObj.storename = [dict objectForKey:@"store_name"];
                                                                    statusObj.videourl = [dict objectForKey:@"video_url"];
                                                                    [videoArray addObject:statusObj];
                                                                }
                                                                
                                                                if([videoArray count] > 0)
                                                                {
                                                                    [self openVideoWebView];
                                                                }
                                                                
                                                            } else {
                                                                DebugLog(@"Login Failed");
                                                                if(self.splashJson != nil && [[self.splashJson  objectForKey:@"success"] isEqualToString:@"false"]) {
                                                                    if([self.splashJson  objectForKey:@"code"] != nil &&  [[self.splashJson  objectForKey:@"code"] isEqualToString:FRONTLINE_INVALID_USER_AUTH_CODE])
                                                                    {
                                                                        if([PIMDGlobalOperations isFrontlineMemberLoggedIn]){
                                                                            [PIMDGlobalOperations showAlert:FRONTLINE_INVALID_USER_AUTH_MESSAGE];
                                                                            [PIMDGlobalOperations clearFrontlineMemberDetails];
                                                                            [self performSelector:@selector(popToLoginController) withObject:nil afterDelay:1.0];
                                                                        }
                                                                    } else {
                                                                        NSString *message = [self.splashJson objectForKey:@"message"];
                                                                        if([message length] > 0)
                                                                        {
                                                                            [PIMDGlobalOperations showAlert:message];
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            
                                                        }  else {
                                                            [PIMDGlobalOperations showAlert:@"No response from server. Please try again later."];
                                                        }
                                                        
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        //[hud hide:YES];
                                                        [CommonCallback hideProgressHud];
                                                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                                                     message:[NSString stringWithFormat:@"%@",[error localizedDescription]]
                                                                                                    delegate:nil
                                                                                           cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                        [av show];
                                                        
                                                    }];
    
    
    
    [operation start];
}

-(void) popToLoginController
{
    DebugLog(@"popToRootViewControllerAnimated:YES");
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)openVideoWebView
{
   DebugLog(@"%@",((PIMDTransitObject *)[videoArray objectAtIndex:0]).videourl);
    
   PIMDTransitObject *obj = [videoArray objectAtIndex:0];
    
    if([obj.videourl length] > 0)
    {
        NSURL *urlAddress = [NSURL URLWithString:obj.videourl];

        NSURLRequest *requestObj = [NSURLRequest requestWithURL:urlAddress];

        //Load the request in the UIWebView.
        [videoWebView loadRequest:requestObj];
    }
}

#pragma mark - Web View
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    DebugLog(@"webViewDidStartLoad");
    //[CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    videoWebView.scalesPageToFit = TRUE;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    DebugLog(@"webViewDidFinishLoad");
    //[CommonCallback hideProgressHud];
    videoWebView.scalesPageToFit = TRUE;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    DebugLog(@"didFailLoadWithError : %@",error);
    //[CommonCallback hideProgressHudWithError];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)moreBtnPressed:(id)sender {
    [PIMDAnalyticsLogger logEvent:@"Customer_More_Btn_Pressed"];
    PIMDMoreViewController *storelistController = [[PIMDMoreViewController alloc] initWithNibName:@"PIMDMoreViewController" bundle:nil] ;
    storelistController.title = @"Store Options";
    storelistController.passedstoreid = passedstoreid;
    [self.navigationController pushViewController:storelistController animated:YES];
}
@end
