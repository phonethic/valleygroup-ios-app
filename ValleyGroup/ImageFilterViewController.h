//
//  ImageFilterViewController.h
//  shoplocal
//
//  Created by Kirti Nikam on 30/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "NLImageCropperView.h"

//Commenting cropping. anyways below BJImageCropper.h is the best to use cropping
//#import "BJImageCropper.h"

@protocol CropImageFilterViewControllerDelegate <NSObject>
@required
-(void)saveImageToPhotoAlbum:(UIImage *)image;
@end

@interface ImageFilterViewController : UIViewController
{
    int rotationInt;

//    NLImageCropperView* _imageCropper;
//    BJImageCropper *imageCropper;
}
//@property (nonatomic, strong) BJImageCropper *imageCropper;

@property(nonatomic,assign) id<CropImageFilterViewControllerDelegate> delegate;
@property (nonatomic,strong) UIImage *originalImage;

@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UIButton *saveBtn;
@property (strong, nonatomic) IBOutlet UIButton *cancelBtn;
@property (strong, nonatomic) IBOutlet UIButton *antiClockWiseRotateBtn;
@property (strong, nonatomic) IBOutlet UIButton *clockWiseRotateBtn;
@property (strong, nonatomic) IBOutlet UIView *toolBarView;

- (IBAction)saveBtnClicked:(id)sender;
- (IBAction)cancelBtnClicked:(id)sender;
- (IBAction)antiClockWiseRotateBtnClicked:(id)sender;
- (IBAction)clockWiseRotateBtnClicked:(id)sender;
@end
