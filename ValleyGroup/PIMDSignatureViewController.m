//
//  PIMDSignatureViewController.m
//  ValleyGroup
//
//  Created by Rishi on 04/06/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import "PIMDSignatureViewController.h"
#import "PIMDAppDelegate.h"
#import "SignatureView.h"
#import "constants.h"

@interface PIMDSignatureViewController ()

@end

@implementation PIMDSignatureViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if([self respondsToSelector:@selector(setEdgesForExtendedLayout:)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        
    }
        
    [self.signatureView setLineWidth:2.0];
    
    self.signatureView.foregroundLineColor = [UIColor colorWithRed:0.204 green:0.596 blue:0.859 alpha:1.000];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clearBtnPressed:(id)sender {
    [self.signatureView clear];
}

- (IBAction)doneBtnPressed:(id)sender {
    
    if ([PIMD_APP_DELEGATE networkavailable]) {
        UIAlertView *addImageToGalleryAlert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"Add this signature for approval ?" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
        addImageToGalleryAlert.tag = 1;
        [addImageToGalleryAlert show];
    } else {
        [PIMDGlobalOperations showOfflineAlert];
    }

}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if (alertView.tag == 1){
        if([title isEqualToString:@"Yes"])
        {
            if(self.delegate && [self.delegate respondsToSelector:@selector(addImageToSignatureView:)])
            {
                UIImage *signImage = [self.signatureView signatureImage];
                [self.delegate addImageToSignatureView:signImage];
            }
            [self.navigationController popViewControllerAnimated:YES];
            self.delegate = nil;
        }
    }
}

@end
