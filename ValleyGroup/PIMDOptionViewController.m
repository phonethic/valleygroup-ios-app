//
//  PIMDOptionViewController.m
//  ValleyGroup
//
//  Created by Rishi on 18/04/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import "PIMDOptionViewController.h"
#import "PIMDAppDelegate.h"
#import "PIMDWebViewController.h"

@interface PIMDOptionViewController () {
    UIView *rootView;
}

@end

@implementation PIMDOptionViewController
@synthesize teamBtn;
@synthesize clientBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    teamBtn.frame = CGRectMake(teamBtn.frame.origin.x,130,teamBtn.frame.size.width,teamBtn.frame.size.height);
//    CGRect windowFrame = [[UIScreen mainScreen] bounds];
//    double yaxis = (((windowFrame.size.height+64) - ((CGRectGetHeight(teamBtn.frame)*2)+8)) / 2);
//    teamBtn.frame = CGRectMake(teamBtn.frame.origin.x,yaxis,teamBtn.frame.size.width,teamBtn.frame.size.height);
//    clientBtn.frame = CGRectMake(clientBtn.frame.origin.x, CGRectGetMaxY(teamBtn.frame)+8,clientBtn.frame.size.width,clientBtn.frame.size.height);
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    rootView = self.view;
    [self showIntroWithCrossDissolve];
    
    teamBtn.titleLabel.font     = DEFAULT_FONT(28);
    clientBtn.titleLabel.font   = DEFAULT_FONT(28);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showIntroWithCrossDissolve {
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    EAIntroPage *page1 = [EAIntroPage page];
    page1.title = @"";
    page1.desc = @"";
    page1.bgImage = [UIImage imageNamed:@"Valley-Info1.jpg"];
    //page1.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"title1"]];
    
    EAIntroPage *page2 = [EAIntroPage page];
    page2.title = @"";
    page2.desc = @"";
    page2.bgImage = [UIImage imageNamed:@"Valley-Info2.jpg"];
    //page2.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"title2"]];
    
    EAIntroPage *page3 = [EAIntroPage page];
    page3.title = @"";
    page3.desc = @"";
    page3.bgImage = [UIImage imageNamed:@"Valley-Info3.jpg"];
    //page3.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"title3"]];
    
    EAIntroPage *page4 = [EAIntroPage page];
    page4.title = @"";
    page4.desc = @"";
    page4.bgImage = [UIImage imageNamed:@"Valley-Info4.jpg"];
    //page4.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"title4"]];
    
    EAIntroPage *page5 = [EAIntroPage page];
    page5.title = @"";
    page5.desc = @"";
    page5.bgImage = [UIImage imageNamed:@"Valley-Info5.jpg"];
    //page4.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"title4"]];
    
    EAIntroView *intro = [[EAIntroView alloc] initWithFrame:rootView.bounds andPages:@[page1,page2,page3,page4,page5]];
    intro.swipeToExit = NO;
    [intro setDelegate:self];
    
    [intro showInView:rootView animateDuration:0.0];
}

- (void)introDidFinish:(EAIntroView *)introView {
    DebugLog(@"introDidFinish callback");
    [[UIApplication sharedApplication] setStatusBarHidden:FALSE withAnimation:UIStatusBarAnimationSlide];
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
}

- (void)intro:(EAIntroView *)introView pageAppeared:(EAIntroPage *)page withIndex:(NSInteger)pageIndex
{
    DebugLog(@"pageAppeared --- > %ld",(long)pageIndex);
    if(pageIndex == 4)
    {
        [introView.skipButton setTitle:@"Enter" forState:UIControlStateNormal];
    } else {
        [introView.skipButton setTitle:@"Skip" forState:UIControlStateNormal];
    }
}

- (void)intro:(EAIntroView *)introView pageStartScrolling:(EAIntroPage *)page withIndex:(NSInteger)pageIndex
{
    DebugLog(@"pageStartScrolling --- > %ld",(long)pageIndex);
}

- (void)intro:(EAIntroView *)introView pageEndScrolling:(EAIntroPage *)page withIndex:(NSInteger)pageIndex
{
    DebugLog(@"pageEndScrolling --- > %ld",(long)pageIndex);
    
}

- (IBAction)teamBtnPressed:(id)sender {
    [PIMD_APP_DELEGATE pushViewController:FRONTLINE_TEAM];
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"Coming soon" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//    [alert show];
}

- (IBAction)clientBtnPressed:(id)sender {
    [PIMD_APP_DELEGATE pushViewController:CLIENT];
}

- (IBAction)phonethicsBtnPressed:(id)sender {
    [PIMDAnalyticsLogger logEvent:@"Phonethics_Btn_Pressed"];
    PIMDWebViewController *webviewController = [[PIMDWebViewController alloc] initWithNibName:@"PIMDWebViewController" bundle:nil] ;
    webviewController.title = @"Phonethics";
    [self.navigationController pushViewController:webviewController animated:YES];
}
@end
