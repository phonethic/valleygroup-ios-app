//
//  PIMDTeamLoginViewController.m
//  ValleyGroup
//
//  Created by Rishi on 15/04/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import "PIMDTeamLoginViewController.h"
#import "PIMDAppDelegate.h"
#import "PIMDStoreObject.h"
#import "AFNetworking.h"
#import "CommonCallback.h"
#import "PIMDModuleSelectionViewController.h"
#import "PIMDWebViewController.h"

#define LOGIN_FRONTLINE_TEAM [NSString stringWithFormat:@"%@%@member_api/login_member",URL_PREFIX,API_VERSION]

@interface PIMDTeamLoginViewController ()

@end

@implementation PIMDTeamLoginViewController
@synthesize loginView;
@synthesize loginBtn;
@synthesize nameTextField;
@synthesize passwordTextField;
@synthesize nameView,passView;
@synthesize frontlineScrollView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [switchbutton setImage:[UIImage imageNamed:@"switch_2.png"] forState:UIControlStateNormal];
    if([PIMDGlobalOperations isFrontlineMemberLoggedIn])
    {
        PIMDModuleSelectionViewController *moduleController = [[PIMDModuleSelectionViewController alloc] initWithNibName:@"PIMDModuleSelectionViewController" bundle:nil] ;
        moduleController.title = @"Select Module";
        [self.navigationController pushViewController:moduleController animated:YES];
    } else {
        [nameTextField setText:[PIMDGlobalOperations getFrontlineMemberPhone]];
        [passwordTextField setText:@""];
    }
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    DebugLog(@"%f %f",frontlineScrollView.contentSize.height,frontlineScrollView.contentOffset.y);
    if ([[UIScreen mainScreen] bounds].size.height < 568)
    {
        if([textField isEqual:nameTextField])
        {
            [frontlineScrollView setContentOffset:CGPointMake(0,nameView.frame.origin.y+30) animated:YES];
        }
        else if(textField == passwordTextField) {
            [frontlineScrollView setContentOffset:CGPointMake(0,passView.frame.origin.y+40) animated:YES];
        }
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == nameTextField) {
        [passwordTextField becomeFirstResponder];
	}
    //    else if (textField == designationTextField) {
    //        [passwordTextField becomeFirstResponder];
    //	}
    else {
        [passwordTextField resignFirstResponder];
        [self scrollTobottom];
    }
   	return YES;
}

-(void)scrollTobottom
{
    DebugLog(@"scrollview **** - > %f %f",frontlineScrollView.contentSize.height,frontlineScrollView.contentOffset.y);
    //    if(customerloginScrollView.contentSize.height > customerloginScrollView.frame.size.height)  //if scrollview content height is greater than scrollview total height then remove the added height from scrollview
    //    {
    //        [customerloginScrollView setContentSize: CGSizeMake(customerloginScrollView.frame.size.width, customerloginScrollView.frame.size.height - 100)];
    //    }
    if ([[UIScreen mainScreen] bounds].size.height < 568)
    {
        CGPoint bottomOffset = CGPointMake(0, 0);
        [frontlineScrollView setContentOffset:bottomOffset animated:YES];
        DebugLog(@"scrollview ### - > %f %f",frontlineScrollView.contentSize.height,frontlineScrollView.frame.size.height);
    }
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if ([self respondsToSelector:NSSelectorFromString(@"edgesForExtendedLayout")]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    switchbutton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [switchbutton setImage:[UIImage imageNamed:@"switch_2.png"] forState:UIControlStateNormal];
    [switchbutton addTarget:self action:@selector(changeViewController) forControlEvents:UIControlEventTouchUpInside];
    [switchbutton setFrame:CGRectMake(0, 5, 40, 24)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:switchbutton];
    
    UITapGestureRecognizer *viewTap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapDetected:)];
    viewTap.numberOfTapsRequired = 1;
    viewTap.cancelsTouchesInView = NO;
    [self.view  addGestureRecognizer:viewTap];
    
    loginBtn.titleLabel.font    = DEFAULT_BOLD_FONT(20);
    nameTextField.font          = DEFAULT_FONT(20);
    passwordTextField.font      = DEFAULT_FONT(20);

}

- (void)tapDetected:(UIGestureRecognizer *)sender {
    [self.view endEditing:YES];
    [self scrollTobottom];
}

-(void)changeViewController {
    [switchbutton setImage:[UIImage imageNamed:@"switch_1.png"] forState:UIControlStateNormal];
    [self performSelector:@selector(switchController) withObject:nil afterDelay:0.2];
}

-(void)switchController
{
    [PIMD_APP_DELEGATE switchViewController:CLIENT];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)loginBtnPressed:(id)sender {
    if ([PIMD_APP_DELEGATE networkavailable]) {
        if([nameTextField.text isEqualToString:@""] || [passwordTextField.text isEqualToString:@""])
        {
            [PIMDGlobalOperations showAlert:@"Please fill all the fields."];
        }
        else if([nameTextField.text length] < 10)
        {
            [PIMDGlobalOperations showAlert:@"Mobile number must be at least 10 characters in length."];
        }
        else if([passwordTextField.text length] < 6)
        {
            [PIMDGlobalOperations showAlert:@"Password field must be at least 6 characters in length."];
        } else {
            [PIMDAnalyticsLogger logEvent:@"Frontline_Login_Btn_Pressed"];
            [self sendFrontlineTeamLoginRequest];
        }
    } else {
        [PIMDGlobalOperations showOfflineAlert];
    }
}

- (IBAction)phonethicsBtnPressed:(id)sender {
    [PIMDAnalyticsLogger logEvent:@"Phonethics_Btn_Pressed"];
    PIMDWebViewController *webviewController = [[PIMDWebViewController alloc] initWithNibName:@"PIMDWebViewController" bundle:nil] ;
    webviewController.title = @"Phonethics";
    [self.navigationController pushViewController:webviewController animated:YES];
}

-(void)sendFrontlineTeamLoginRequest
{
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:API_KEY];
    [httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:nameTextField.text forKey:@"mobile"];
    [params setObject:passwordTextField.text forKey:@"password"];
    NSLog(@"%@",LOGIN_FRONTLINE_TEAM);
    [httpClient postPath:LOGIN_FRONTLINE_TEAM parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"json ->%@",json);
        if(json != nil) {
            
            if([[json  objectForKey:@"success"] isEqualToString:@"true"])
            {
               [PIMDAnalyticsLogger logEvent:@"Frontline_Login_Success"];
                NSDictionary* data = [json objectForKey:@"data"];
                NSString * resultid = [data objectForKey:@"id"];
                NSString * resultcode = [data objectForKey:@"auth_code"];
                DebugLog(@"%@ %@",resultid,resultcode);
                [PIMDGlobalOperations setFrontlineMemberDetails:[nameTextField text] pass:[passwordTextField text]];
                [PIMDGlobalOperations setFrontlineMemberAuthDetails:resultid code:resultcode];
                PIMDModuleSelectionViewController *moduleController = [[PIMDModuleSelectionViewController alloc] initWithNibName:@"PIMDModuleSelectionViewController" bundle:nil] ;
                moduleController.title = @"Select Module";
                [self.navigationController pushViewController:moduleController animated:YES];
            } else {
                DebugLog(@"Login Failed");
                [PIMDAnalyticsLogger logEvent:@"Frontline_Login_Failed"];
                NSString *message = [json objectForKey:@"message"];
                if([message length] > 0)
                {
                    [PIMDGlobalOperations showAlert:message];
                }
            }
            
        }  else {
            [PIMDGlobalOperations showAlert:@"No response from server. Please try again later."];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        [CommonCallback hideProgressHud];
    }];
}

@end
