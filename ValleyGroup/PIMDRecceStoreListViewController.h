//
//  PIMDRecceStoreListViewController.h
//  ValleyGroup
//
//  Created by Rishi on 05/05/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PIMDRecceStoreListViewController : UIViewController <UITextFieldDelegate>
{
    BOOL isDragging;
    BOOL isLoading;
}
@property (strong, nonatomic) NSMutableArray *storeArray;
@property (strong, nonatomic) NSMutableArray *searchResults;
@property (strong, nonatomic) IBOutlet UITableView *recceStoreTableView;
@property (strong, nonatomic) IBOutlet UIView *searchView;
@property (strong, nonatomic) IBOutlet UITextField *searchTextField;
@property (strong, nonatomic) IBOutlet UIButton *searchBtn;
@property (strong, nonatomic) IBOutlet UIView *viewSearchResults;
@property (strong, nonatomic) IBOutlet UILabel *lblSearchResults;

//Pull to refresh
@property (nonatomic, copy) NSString *textPull;
@property (nonatomic, copy) NSString *textRelease;
@property (nonatomic, copy) NSString *textLoading;

@property (nonatomic, retain) UIView *refreshHeaderView;
@property (nonatomic, retain) UILabel *refreshLabel;
@property (nonatomic, retain) UIImageView *refreshArrow;
@property (nonatomic, retain) UIActivityIndicatorView *refreshSpinner;

- (IBAction)searchTextChanged:(UITextField*)sender;
- (IBAction)searchBtnPressed:(id)sender;
@end
