//
//  PIMDWebViewController.m
//  ValleyGroup
//
//  Created by Rishi on 27/05/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import "PIMDWebViewController.h"
#import "PIMDAppDelegate.h"
#import "CommonCallback.h"

#define PHONETHICS_LINK @"http://phonethics.in/digital-agency-contact-us/"

@interface PIMDWebViewController ()

@end

@implementation PIMDWebViewController
@synthesize webView;
@synthesize webviewToolbar;
@synthesize backButton,forwardButton,refreshButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [webviewToolbar setHidden:TRUE];
    [webView setHidden:TRUE];
    
    if([PIMD_APP_DELEGATE networkavailable]) {
        NSURL *url = [NSURL URLWithString:PHONETHICS_LINK];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        [webView setDelegate:self];
        [webView loadRequest:requestObj];
        [webView setHidden:FALSE];
        [webviewToolbar setHidden:FALSE];
        [backButton setEnabled:FALSE];
        [forwardButton setEnabled:FALSE];
        [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    } else {
        UIAlertView *errorView = [[UIAlertView alloc]
                                  initWithTitle:@"No Network Connection"
                                  message:@"Please check your internet connection and try again."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [errorView show];
        [CommonCallback hideProgressHud];
    }

}

#pragma webView delegate Methods
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    //DebugLog(@" FaceBook: request : %@",webView.request.URL);
    DebugLog(@"============%@====================",@"webViewDidStartLoad");
    [refreshButton setEnabled:FALSE];
}

- (void)webViewDidFinishLoad:(UIWebView *)lwebView
{
    [CommonCallback hideProgressHud];
    if(webView.canGoBack)
    {
        [backButton setEnabled:TRUE];
    } else {
        [backButton setEnabled:FALSE];
    }
    if(webView.canGoForward)
    {
        [forwardButton setEnabled:TRUE];
    } else {
        [forwardButton setEnabled:FALSE];
    }
    [refreshButton setEnabled:TRUE];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [CommonCallback hideProgressHud];
    [refreshButton setEnabled:TRUE];
}

#pragma UIButton delegate Method
- (IBAction)webviewbackBtnPressed:(id)sender {
    [webView goBack];
}

- (IBAction)webviewfwdBtnPressed:(id)sender {
    [webView goForward];
}

- (IBAction)webviewrefreshBtnPressed:(id)sender {
    [webView reload];
}

-(void)dealloc{
    [webView stopLoading];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
