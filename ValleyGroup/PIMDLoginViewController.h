//
//  PIMDLoginViewController.h
//  ValleyGroup
//
//  Created by Rishi on 26/03/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EAIntroView.h"
#import "PIMDMapViewController.h"

@interface PIMDLoginViewController : UIViewController <EAIntroDelegate> {
    UIButton *switchbutton;
}

@property (strong, nonatomic) IBOutlet UIView *loginView;
@property (strong, nonatomic) IBOutlet UITextField *nameTextField;
@property (strong, nonatomic) IBOutlet UITextField *designationTextField;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;
@property (strong, nonatomic) IBOutlet UIButton *loginBtn;
@property (strong, nonatomic) IBOutlet UIScrollView *customerloginScrollView;
@property (strong, nonatomic) IBOutlet UIView *nameVIew;
@property (strong, nonatomic) IBOutlet UIView *passView;

- (IBAction)loginBtnPressed:(id)sender;
- (IBAction)phonethicsBtnPressed:(id)sender;

@end
