

#import "Reachability.h"


#define SYSTEM_VERSION_LESS_THAN(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)



//FLURRY ID
//#define FLURRY_APPID @"VR4D252MBHZKVTJCTN8B"
#define FLURRY_APPID @"23423"

//LOCALYTICS ID
//#define LOCALYTICS_APPID @"a130824c3478358ab75a2a6-2e3a5cd0-e570-11e3-1efb-004a77f8b47f"
#define LOCALYTICS_APPID @"23444"

//ERROR CODES
//====================== CUSTOMER =========================
#define CUSTOMER_INVALID_USER_AUTH_CODE @"-221"
#define CUSTOMER_INVALID_USER_AUTH_MESSAGE @"Invalid credential. Kindly Re-login"

//====================== MERCHANT =========================
#define FRONTLINE_INVALID_USER_AUTH_CODE @"-116"
#define FRONTLINE_INVALID_USER_AUTH_MESSAGE @"Invalid credential. Kindly Re-login"


/////////===========================================================================

#define NETWORK_NOTIFICATION @"NetWorkchangeNotification"

#define ALERT_TITLE @"Valley Group"
#define ALERT_MESSAGE_NO_NETWORK @"No internet. Please connect to the internet and try again."

#define BROWN_OFFWHITE_COLOR [UIColor colorWithRed:113/255.0 green:91/255.0  blue:87/255.0 alpha:0.2]

#define RED_TEXT_COLOR [UIColor colorWithRed:217/255.0 green:72/255.0  blue:76/255.0 alpha:1.0]

/////////=============== BG = light gray, Button = red and selected gray color==========
#define DEFAULT_BACKGROUND_COLOR [UIColor colorWithRed:235/255.0 green:235/255.0  blue:235/255.0 alpha:1.0]

#define DEFAULT_COLOR [UIColor colorWithRed:244/255.0 green:52/255.0  blue:67/255.0 alpha:1.0]

#define DEFAULT_SELECTED_COLOR [UIColor colorWithRed:70/255.0 green:70/255.0  blue:70/255.0 alpha:1.0]

/////////===========================================================================

//Font : Arial
//ArialMT
//Arial-BoldItalicMT
//Arial-ItalicMT
//Arial-BoldMT

#define DEFAULT_FONT(SIZE) [UIFont fontWithName:@"ArialMT" size:SIZE]
#define DEFAULT_BOLD_FONT(SIZE) [UIFont fontWithName:@"Arial-BoldMT" size:SIZE]

#define KALINGA_FONT(SIZE) [UIFont fontWithName:@"Kalinga" size:SIZE]
#define KALINGA_BOLD_FONT(SIZE) [UIFont fontWithName:@"Kalinga-Bold" size:SIZE]
#define ABADI_FONT(SIZE) [UIFont fontWithName:@"AbadiMT-CondensedLight" size:SIZE]


#define HUD_TITLE @"Loading"
#define HUD_SUBTITLE @"Please wait ..."

//================Pull to refresh
#define REFRESH_HEADER_HEIGHT 52.0f
#define REFRESH_LABEL_FONT DEFAULT_FONT
#define PULLTOREFRESH_BARROW @"arrowbrown.png"
#define PULLTOREFRESH_WARROW @"arrowwhite.png"

//================

#define API_KEY @"vgiapp"

//#define CONNECT_STAGE
#ifdef CONNECT_STAGE
    #define LIVE_SERVER @"http://stage.phonethics.in/"
    #define URL_PREFIX @"valleygrp/"
    #define API_VERSION @"api-1/"
#else
    #define LIVE_SERVER @"http://apps.phonethics.in/"
    #define URL_PREFIX @"valleygroup/"
    #define API_VERSION @"api-1/"
#endif

//Google API http://maps.googleapis.com/maps/api/geocode/json?components=postal_code:160002|country:IN

/*

when user select frontline to login                     Frontline_Login_Btn_Pressed

When user successfully logged in as frontline member	Frontline_Login_Success,Frontline_Login_Failed


When user clicks on Logout button                       Frontline_Logout_Btn_Pressed

When frontline member select module                     Installation_Module_Btn_Pressed, Recce_Module_Btn_Pressed

When frontline member selects a store from StoreList	Installation_Selected_Store(StoreName, StoreAdd),  Recce_Selected_Store(StoreName, StoreAdd)


When frontline member select desgin model				Recce_Choose_Design(*StoreName*,DesignName)

When frontline member select add location				Recce_Add_Location_Image1, Recce_Add_Location_Image2, Recce_Add_Location_Image3


When frontline member add comments                      Recce_Add_Comments(*StoreName*)

When frontline member click submit                      Recce_Submit_Btn_Pressed("Recce_Submit_Success","Recce_Submit_Failed")


When frontline member click inspection image
 in Installation module                                 Installation_Inspection_Image_Btn_Pressed("Installation_Inspection_Success","Installation_Inspection_Failed")



When frontline member click approval image in
 Installation module                                    Installation_Approval_Image_Btn_Pressed("Installation_Approval_Success","Installation_Approval_Failed")


When frontline member click submit 
 button in Installation module                          Installation_Submit_Btn_Pressed("Installation_Submit_Success","Installation_Submit_Failed")



*/

