//
//  PIMDChooseDesignViewController.m
//  ValleyGroup
//
//  Created by Rishi on 05/05/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import "PIMDChooseDesignViewController.h"
#import "constants.h"
#import "CommonCallback.h"
#import "PIMDDesignObject.h"
#import "PIMDUploadImagesViewController.h"
#import "PIMDAppDelegate.h"

#define GET_RECCE_STORE_DESIGN_LIST(ID) [NSString stringWithFormat:@"%@%@%@store_api/recce_design?store_id=%@",LIVE_SERVER,URL_PREFIX,API_VERSION,ID]

#define THUMBNAIL_LINK(IMG_LINK) [NSString stringWithFormat:@"%@%@%@",LIVE_SERVER,URL_PREFIX,IMG_LINK]

@interface PIMDChooseDesignViewController ()
@property(strong) NSDictionary *splashJson;
@end

@implementation PIMDChooseDesignViewController
@synthesize designArray;
@synthesize chooseDesignTableView;
@synthesize passedstoreid;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if([self respondsToSelector:@selector(setEdgesForExtendedLayout:)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        
    }
    
    designArray = [[NSMutableArray alloc] init];
    if ([PIMD_APP_DELEGATE networkavailable]) {
        [self sendDesignListRequest];
    } else {
        [PIMDGlobalOperations showOfflineAlert];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)sendDesignListRequest
{
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:GET_RECCE_STORE_DESIGN_LIST(passedstoreid)]];
    DebugLog(@"%@",GET_RECCE_STORE_DESIGN_LIST(passedstoreid));
    [urlRequest setValue:API_KEY forHTTPHeaderField:@"X-API-KEY"];
    [urlRequest setValue:[PIMDGlobalOperations getFrontlineMemberId] forHTTPHeaderField:@"member_id"];
    [urlRequest setValue:[PIMDGlobalOperations getFrontlineMemberAuthId] forHTTPHeaderField:@"auth_id"];
    DebugLog(@"%@",[PIMDGlobalOperations getFrontlineMemberId]);
    DebugLog(@"%@",[PIMDGlobalOperations getFrontlineMemberAuthId]);
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: urlRequest
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        [CommonCallback hideProgressHud];
                                                        DebugLog(@"%@",self.splashJson);
                                                        if(self.splashJson  != nil)
                                                        {
                                                            [designArray removeAllObjects];
                                                            if([[self.splashJson  objectForKey:@"success"] isEqualToString:@"true"])
                                                            {
                                                                NSArray *dataArray = [self.splashJson  objectForKey:@"data"];
                                                                for (int index = 0; index < [dataArray count]; index++) {
                                                                    NSDictionary *dict = [dataArray objectAtIndex:index];
                                                                    PIMDDesignObject *designObj = [[PIMDDesignObject alloc] init];
                                                                    designObj.designid = [dict objectForKey:@"design_id"];
                                                                    designObj.storeid = [dict objectForKey:@"store_id"];
                                                                    designObj.designname = [dict objectForKey:@"name"];
                                                                    designObj.imageUrl = [dict objectForKey:@"image_url"];
                                                                    designObj.thumbUrl = [dict objectForKey:@"thumb_url"];
                                                                    [designArray addObject:designObj];
                                                                }
                                                                
                                                                DebugLog(@"%@",((PIMDDesignObject *)[designArray objectAtIndex:0]).designname);
                                                            } else {
                                                                DebugLog(@"Login Failed");
                                                                NSString *message = [self.splashJson objectForKey:@"message"];
                                                                if([message length] > 0)
                                                                {
                                                                    [PIMDGlobalOperations showAlert:message];
                                                                }
                                                            }
                                                            
                                                        }  else {
                                                            [PIMDGlobalOperations showAlert:@"No response from server. Please try again later."];
                                                        }
                                                        [chooseDesignTableView reloadData];
                                                        
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        //[hud hide:YES];
                                                        [CommonCallback hideProgressHud];
                                                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                                                     message:[NSString stringWithFormat:@"%@",[error localizedDescription]]
                                                                                                    delegate:nil
                                                                                           cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                        [av show];
                                                        
                                                    }];
    
    
    
    [operation start];
}

#pragma mark - Table view data source
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 140;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [designArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UILabel *lblTitle;
    UIView *backView;
    UIImageView *thumbImg;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.backgroundColor = [UIColor clearColor];
        
        backView = [[UIView alloc] initWithFrame:CGRectMake(0, 2, 280, 136)];
        backView.tag = 101;
        backView.backgroundColor    =  [UIColor whiteColor];
        backView.clipsToBounds      = YES;
        [cell.contentView addSubview:backView];
        
        thumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 140, 140)];
        thumbImg.tag = 104;
        thumbImg.contentMode = UIViewContentModeScaleAspectFit;
        //thumbImg.image   = [UIImage imageNamed:@"milestone.png"];
        [backView addSubview:thumbImg];
        
        
        lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(150,50,100, 40.0)];
        lblTitle.text                   = @"";
        lblTitle.tag                    = 102;
        lblTitle.font                   = DEFAULT_FONT(20);
        lblTitle.textAlignment          = NSTextAlignmentLeft;
        lblTitle.textColor              = DEFAULT_COLOR;
        lblTitle.highlightedTextColor   = [UIColor whiteColor];
        lblTitle.backgroundColor        = [UIColor clearColor];
        lblTitle.numberOfLines          = 1;
        [backView addSubview:lblTitle];
        
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor     = DEFAULT_SELECTED_COLOR;
        cell.selectedBackgroundView     = bgColorView;

    }
    PIMDDesignObject *designObj = nil;
    designObj = [designArray objectAtIndex:indexPath.row];
    
    backView        = (UILabel *)[cell viewWithTag:101];
    lblTitle        = (UILabel *)[backView viewWithTag:102];
    thumbImg        = (UIImageView *)[backView viewWithTag:104];
    
    lblTitle.text = designObj.designname;
    
    if(designObj.thumbUrl != nil && ![designObj.thumbUrl isEqualToString:@""])
    {
        __weak UIImageView *thumbImg_ = thumbImg;
        [thumbImg_ setImageWithURL:[NSURL URLWithString:THUMBNAIL_LINK(designObj.thumbUrl)] placeholderImage:[UIImage imageNamed:@"Shoplocal_Post_Placeholder.jpg"] completed:^(UIImage *image,NSError *error, SDImageCacheType cacheType) {
            if (error) {
                thumbImg_.image = [UIImage imageNamed:@"Shoplocal_Post_Placeholder.jpg"];
            }
        }];
    } else {
        thumbImg.image = [UIImage imageNamed:@"Shoplocal_Post_Placeholder.jpg"];
    }

    
    return cell;
}


#pragma mark - Table view delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DebugLog(@"%@",[designArray objectAtIndex:indexPath.row]);
    PIMDDesignObject *designObj = [designArray objectAtIndex:indexPath.row];
    DebugLog(@"status-->%@",designObj.storeid);
    
    if([designObj.storeid length] > 0 && [designObj.designname length] > 0)
    {
        NSDictionary *milestonestoreParams = [NSDictionary dictionaryWithObjectsAndKeys:designObj.storeid,@"StoreId", designObj.designname,@"DesignName", nil];
        DebugLog(@"%@",milestonestoreParams);
        [PIMDAnalyticsLogger logEvent:@"Recce_Choose_Design" withParams:milestonestoreParams];
    }
    
    PIMDUploadImagesViewController *designController = [[PIMDUploadImagesViewController alloc] initWithNibName:@"PIMDUploadImagesViewController" bundle:nil] ;
    designController.title = @"Store Status";
    designController.passedstoreid = designObj.storeid;
    designController.passeddesignid = designObj.designid;
    [self.navigationController pushViewController:designController animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
