//
//  PIMDCommentsSubmitViewController.h
//  ValleyGroup
//
//  Created by Rishi on 07/05/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PIMDCommentsSubmitViewController : UIViewController <UITextViewDelegate>

@property (strong, nonatomic) IBOutlet UITextView *commentsTextView;
@property (nonatomic, copy) NSString *passedstoreid;
@property (nonatomic, copy) NSString *passeddesignid;
@property (nonatomic, copy) NSString *recceId;

- (IBAction)submitBtnPressed:(id)sender;

@end
