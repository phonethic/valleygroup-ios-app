//
//  PIMDInstallationFormDetailViewController.m
//  ValleyGroup
//
//  Created by Rishi on 09/05/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import "PIMDInstallationFormDetailViewController.h"
#import "constants.h"
#import "CommonCallback.h"
#import "PIMDQuestionObject.h"
#import "PIMDAppDelegate.h"
#import "PIMDSignatureViewController.h"
#import "PIMDModuleSelectionViewController.h"

#define GET_INSTALLATION_FORM_QUESTIONS(SID,CID) [NSString stringWithFormat:@"%@%@%@store_api/inspection_questions?store_id=%@&category_id=%@",LIVE_SERVER,URL_PREFIX,API_VERSION,SID,CID]
#define POST_INSTALLATION_FORM_QUESTIONS [NSString stringWithFormat:@"%@%@store_api/inspection_questions",URL_PREFIX,API_VERSION]
#define ADD_GALLERY [NSString stringWithFormat:@"%@%@store_api/inspection_img",URL_PREFIX,API_VERSION]

@interface PIMDInstallationFormDetailViewController ()
@property(strong) NSDictionary *splashJson;
@end

@implementation PIMDInstallationFormDetailViewController
@synthesize newMedia;
@synthesize passedstoreid;
@synthesize passedcategoryid;
@synthesize questionsArray;
@synthesize multiView,submitView;
@synthesize multilbl;
@synthesize multiBtn1;
@synthesize multiBtn2;
@synthesize multiBtn3;
@synthesize multiBtn4;
@synthesize multiBtn5;
@synthesize currentObjID;
@synthesize nextBtn,prevBtn,submitBtn;
@synthesize commentsTextView;
@synthesize formImageView,signatureImageView;
@synthesize aprrovalid;
@synthesize approvalImageAdded;
@synthesize inspectionImageAdded;
@synthesize approvallbl;
@synthesize damagelbl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    NSArray *allTouches = [touches allObjects];
    for (UITouch *touch in allTouches)
    {
        DebugLog(@"%d",touch.view.tag);
        if(touch.view.tag == 111)
        {
            if ([PIMD_APP_DELEGATE networkavailable]) {
                [PIMDAnalyticsLogger logEvent:@"Installation_Inspection_Image_Btn_Pressed"];
                [self addinstallationInspectionImage];
            } else {
                [PIMDGlobalOperations showOfflineAlert];
            }
        } else if (touch.view.tag == 222) {
            if ([PIMD_APP_DELEGATE networkavailable]) {
                [PIMDAnalyticsLogger logEvent:@"Installation_Approval_Image_Btn_Pressed"];
                PIMDSignatureViewController *signController = [[PIMDSignatureViewController alloc] initWithNibName:@"PIMDSignatureViewController" bundle:nil] ;
                signController.title = @"Approval";
                signController.delegate = self;
                [self.navigationController pushViewController:signController animated:YES];
            } else {
                [PIMDGlobalOperations showOfflineAlert];
            }
        }
    }
}

- (void)setViewMovedUp:(BOOL)movedUp{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    // Make changes to the view's frame inside the animation block. They will be animated instead
    // of taking place immediately.
    CGRect rect = self.multiView.frame;
    NSLog(@"%f ", rect.origin.y);
    if (movedUp){
        if(rect.origin.y == 20)
            rect.origin.y = self.multiView.frame.origin.y - 100;
    }
    else{
        if(rect.origin.y < 0)
            rect.origin.y = self.multiView.frame.origin.y + 100;
    }
    self.multiView.frame = rect;
    [UIView commitAnimations];
}

- (void)setTextViewMovedUp:(BOOL)movedUp{
    CGRect rect = self.commentsTextView.frame;
    NSLog(@"%f ", rect.origin.y);
    if (movedUp){
        if(rect.origin.y == 216)
            rect.origin.y = self.commentsTextView.frame.origin.y - 60;
    }
    else{
        if(rect.origin.y < 200)
            rect.origin.y = self.commentsTextView.frame.origin.y + 60;
    }
    self.commentsTextView.frame = rect;
    [UIView commitAnimations];
}

-(void)addinstallationInspectionImage
{
    UIAlertView *addImageToGalleryAlert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"Choose your option" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Choose Existing", @"Take Picture", nil];
    addImageToGalleryAlert.tag = 3;
    [addImageToGalleryAlert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if (alertView.tag == 3){
        if([title isEqualToString:@"Choose Existing"])
        {
            [self useCameraRoll:alertView.tag];
        } else if([title isEqualToString:@"Take Picture"])
        {
            [self useCamera:alertView.tag];
        } 
    } else if (alertView.tag == 1) {
        if([title isEqualToString:@"Yes"])
        {
            if ([PIMD_APP_DELEGATE networkavailable]) {
                [PIMDAnalyticsLogger logEvent:@"Installation_Submit_Btn_Pressed"];
                [self sendAnswersRequest];
            } else {
                [PIMDGlobalOperations showOfflineAlert];
            }
        }
    }
}


#pragma mark - Image Picker
- (void)useCamera:(int)itag
{
    if ([UIImagePickerController isSourceTypeAvailable:
         UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate        = self;
        imagePicker.sourceType      =    UIImagePickerControllerSourceTypeCamera;
        imagePicker.mediaTypes      =    [NSArray arrayWithObjects:(NSString *) kUTTypeImage,nil];
        imagePicker.allowsEditing   =   YES;
        imgPickerTag = itag;
        [self presentViewController:imagePicker animated:YES completion:nil];
        newMedia = YES;
    }
}

- (void)useCameraRoll:(int)itag
{
    if ([UIImagePickerController isSourceTypeAvailable:
         UIImagePickerControllerSourceTypeSavedPhotosAlbum])
    {
        UIImagePickerController *imagePicker =  [[UIImagePickerController alloc] init];
        imagePicker.delegate        = self;
        imagePicker.sourceType      =    UIImagePickerControllerSourceTypePhotoLibrary;
        imagePicker.mediaTypes      =   [NSArray arrayWithObjects:(NSString *) kUTTypeImage, nil];
        imagePicker.allowsEditing   = YES;
        imgPickerTag = itag;
        [self presentViewController:imagePicker animated:YES completion:nil];
        newMedia = NO;
    }
}


-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    [self dismissViewControllerAnimated:YES completion:nil];
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
        //        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
        
        UIImage *originalImage = [CommonCallback fixOrientation:image];
        ImageFilterViewController *filterViewController = [[ImageFilterViewController alloc] initWithNibName:@"ImageFilterViewController" bundle:nil];
        filterViewController.delegate = self;
        filterViewController.originalImage = originalImage;
        
        [UIView  transitionWithView:self.navigationController.view duration:0.8  options:UIViewAnimationOptionTransitionFlipFromLeft
                         animations:^(void) {
                             BOOL oldState = [UIView areAnimationsEnabled];
                             [UIView setAnimationsEnabled:NO];
                             [self.navigationController pushViewController:filterViewController animated:YES];
                             [UIView setAnimationsEnabled:oldState];
                         }
                         completion:nil];
    }
    else if ([mediaType isEqualToString:(NSString *)kUTTypeMovie])
    {
		// Code here to support video if enabled
	}
}

-(void)image:(UIImage *)image finishedSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if (error) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"Save failed"
                              message: @"Failed to save image"
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma CropImageFilterViewControllerDelegate method
-(void)saveImageToPhotoAlbum:(UIImage *)editedImage
{
    DebugLog(@"newPost : saveToalbum %@",editedImage);
    //    if (imgPickerTag == 2) {
    //        iconImageView.image = editedImage;
    //    }else
    if (imgPickerTag == 3) {
        [self sendStoreImageRequest:editedImage];
    }
    imgPickerTag = 0;
    if (newMedia)
        UIImageWriteToSavedPhotosAlbum(editedImage,
                                       self,
                                       @selector(image:finishedSavingWithError:contextInfo:),
                                       nil);
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //myButton.titleLabel. numberOfLines = 0; // Dynamic number of lines
    //myButton.titleLabel.lineBreakMode = UILineBreakModeWordWrap;
    if ([self respondsToSelector:NSSelectorFromString(@"edgesForExtendedLayout")]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    UITapGestureRecognizer *viewTap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapDetected:)];
    viewTap.numberOfTapsRequired = 1;
    viewTap.cancelsTouchesInView = NO;
    [self.view  addGestureRecognizer:viewTap];
    
    for(UIView *v in multiView.subviews)
    {
        if([v isKindOfClass:[UIButton class]])
        {
            ((UITextField*)v).hidden = YES;
        } else if ([v isKindOfClass:[UITextView class]]) {
            ((UITextView*)v).hidden = YES;
        }
    }
    
    for (UIButton *btn in _buttonsCollection) {
        if([btn.titleLabel.text isEqualToString:@"Next"] || [btn.titleLabel.text isEqualToString:@"Previous"] || [btn.titleLabel.text isEqualToString:@"Submit"])
        {
            btn.titleLabel.font = DEFAULT_BOLD_FONT(20);
        } else {
            btn.titleLabel.font = DEFAULT_FONT(16);
        }
    }
    
    approvallbl.font        = DEFAULT_FONT(16);
    damagelbl.font          = DEFAULT_FONT(16);
    commentsTextView.font   = DEFAULT_FONT(16);
    multilbl.font           = DEFAULT_FONT(16);
    
    [CommonCallback roundeWithBorder:commentsTextView];
    
    currentObjID = 0;
    aprrovalid = @"";
    approvalImageAdded = @"0";
    inspectionImageAdded = @"0";
    
    questionsArray = [[NSMutableArray alloc] init];
    
    if ([PIMD_APP_DELEGATE networkavailable]) {
        [self sendInstallationQuestionsRequest];
    } else {
        [PIMDGlobalOperations showOfflineAlert];
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)tapDetected:(UIGestureRecognizer *)sender {
    [self.view endEditing:YES];
}

-(void)sendInstallationQuestionsRequest
{
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:GET_INSTALLATION_FORM_QUESTIONS(passedstoreid,passedcategoryid)]];
    DebugLog(@"%@",GET_INSTALLATION_FORM_QUESTIONS(passedstoreid,passedcategoryid));
    [urlRequest setValue:API_KEY forHTTPHeaderField:@"X-API-KEY"];
    [urlRequest setValue:[PIMDGlobalOperations getFrontlineMemberId] forHTTPHeaderField:@"member_id"];
    [urlRequest setValue:[PIMDGlobalOperations getFrontlineMemberAuthId] forHTTPHeaderField:@"auth_id"];
    DebugLog(@"%@",[PIMDGlobalOperations getFrontlineMemberId]);
    DebugLog(@"%@",[PIMDGlobalOperations getFrontlineMemberAuthId]);
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: urlRequest
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        [CommonCallback hideProgressHud];
                                                        //DebugLog(@"%@",self.splashJson);
                                                        if(self.splashJson  != nil)
                                                        {
                                                            [questionsArray removeAllObjects];
                                                            if([[self.splashJson  objectForKey:@"success"] isEqualToString:@"true"])
                                                            {
                                                                NSArray *dataArray = [self.splashJson  objectForKey:@"data"];
                                                                for (int index = 0; index < [dataArray count]; index++) {
                                                                    NSDictionary *dict = [dataArray objectAtIndex:index];
                                                                    PIMDQuestionObject *quest = [[PIMDQuestionObject alloc] init];
                                                                    quest.storeid = [dict objectForKey:@"store_id"];
                                                                    quest.categoryid = [dict objectForKey:@"category_id"];
                                                                    quest.questionid = [dict objectForKey:@"question_id"];
                                                                    quest.question = [dict objectForKey:@"question"];
                                                                    quest.questiontype = [dict objectForKey:@"question_type"];
                                                                    quest.optiontype = [dict objectForKey:@"option_type"];
                                                                    quest.options = [[NSMutableArray alloc] init];
                                                                    quest.optionsAnswers = [[NSMutableDictionary alloc] init];
                                                                    NSArray *optionsArray = [dict objectForKey:@"options"];
                                                                    NSArray *optionsAnswerArray = [dict valueForKeyPath:@"answers.answer"];
                                                                    NSLog(@"question-->%@",quest.question);
                                                                    NSLog(@"optionsArray-->%@",optionsArray);
                                                                    NSLog(@"optionsAnswerArray-->%@",optionsAnswerArray);
                                                                    NSLog(@"quest.options-->%@",quest.options);
                                                                    NSLog(@"quest.optionsAnswers-->%@",quest.optionsAnswers);
                                                                    for (int i = 0; i < [optionsArray count]; i++) {
                                                                        NSString *key = [optionsArray objectAtIndex:i];
                                                                        NSLog(@"key-->%@",key);
                                                                        if ([[key stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] > 0) {
                                                                            [quest.options addObject:key];
                                                                            if ([optionsAnswerArray count] > 0 && [optionsAnswerArray containsObject:key]) {
                                                                                [quest.optionsAnswers setObject:[NSNumber numberWithBool:YES] forKey:key];
                                                                            }else{
                                                                                [quest.optionsAnswers setObject:[NSNumber numberWithBool:NO] forKey:key];
                                                                            }
                                                                        }
                                                                    }
                                                                    NSLog(@"optionsAnswers-->%@",quest.optionsAnswers);

                                                                    if([[dict objectForKey:@"question_type"] isEqualToString:@"txt"] || [[dict objectForKey:@"question_type"] isEqualToString:@"tic-txt"])
                                                                    {
                                                                        quest.optionsText = [dict valueForKeyPath:@"answers.text"];
                                                                    } else {
                                                                        quest.optionsText = @"";
                                                                    }
                                                                    [questionsArray addObject:quest];
                                                                }
                                                                [self setUIForArray];
                                                            } else {
                                                               // DebugLog(@"Login Failed");
                                                                NSString *message = [self.splashJson objectForKey:@"message"];
                                                                if([message length] > 0)
                                                                {
                                                                    [PIMDGlobalOperations showAlert:message];
                                                                }
                                                            }
                                                            
                                                        }  else {
                                                            [PIMDGlobalOperations showAlert:@"No response from server. Please try again later."];
                                                        }
                                                        //[installationStoreTableView reloadData];
                                                        
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        //[hud hide:YES];
                                                        [CommonCallback hideProgressHud];
                                                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                                                     message:[NSString stringWithFormat:@"%@",[error localizedDescription]]
                                                                                                    delegate:nil
                                                                                           cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                        [av show];
                                                        
                                                    }];
    
    
    
    [operation start];
}

-(void)sendAnswersRequest
{
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:API_KEY];
    [httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    NSMutableDictionary *postparams = [[NSMutableDictionary alloc] init];
    /////////////////////////////////////////////////////////////////
    [postparams setObject:passedstoreid forKey:@"store_id"];
    [postparams setObject:passedcategoryid forKey:@"category_id"];
    [postparams setObject:@"1" forKey:@"approved"];
    if([aprrovalid length] > 0)
    {
         NSLog(@"approval id - > %@", aprrovalid);
        [postparams setObject:aprrovalid forKey:@"approval_id"];
    }
    [postparams setObject:approvalImageAdded forKey:@"image_approval_done"];
    [postparams setObject:inspectionImageAdded forKey:@"image_inspection_done"];
    [postparams setObject:[PIMDGlobalOperations getFrontlineMemberId] forKey:@"member_id"];
    [postparams setObject:[PIMDGlobalOperations getFrontlineMemberAuthId] forKey:@"auth_id"];
    
    /////////////////////////////////////////////////////////////////
    DebugLog(@"----%@------",postparams);
    
    NSMutableArray *fileArray = [[NSMutableArray alloc] init];
    for  (PIMDQuestionObject *quest in questionsArray)
    {
        //if([quest.questiontype isEqualToString:@"tic"])
        //DebugLog(@"%@", quest.question);
        {
            NSMutableDictionary *fileparams = [[NSMutableDictionary alloc] init];
            NSMutableArray *tempArray = [[NSMutableArray alloc] init];
            for ( NSString *key in [quest.optionsAnswers allKeys]) {
                if([[quest.optionsAnswers objectForKey:key] boolValue])
                {
                    //DebugLog(@"%@", key);
                    [tempArray addObject:key];
                }
            }
             //DebugLog(@"%@", tempArray);
            [fileparams setObject:quest.questionid forKey:@"question_id"];
            [fileparams setObject:quest.questiontype forKey:@"question_type"];
            NSMutableDictionary *answersDict = [[NSMutableDictionary alloc] init];
            if([quest.questiontype isEqualToString:@"txt"])
            {
                if([quest.optionsText length] > 0) {
                     NSLog(@"%@",quest.optionsText);
                    [answersDict setObject:quest.optionsText forKey:@"text"];
                } else {
                    [answersDict setObject:@"" forKey:@"text"];
                }
            } else if([quest.questiontype isEqualToString:@"tic"])
            {
                [answersDict setObject:tempArray forKey:@"answer"];
            } else if([quest.questiontype isEqualToString:@"tic-txt"])
            {
                if([quest.optionsText length] > 0) {
                    NSLog(@"%@",quest.optionsText);
                    [answersDict setObject:quest.optionsText forKey:@"text"];
                } else {
                    [answersDict setObject:@"" forKey:@"text"];
                }
                [answersDict setObject:tempArray forKey:@"answer"];
            }
            [fileparams setObject:answersDict forKey:@"answers"];
            //DebugLog(@"%@",fileparams);
            [fileArray addObject:fileparams];
       }
    }

    
    
   // DebugLog(@"%@",fileArray);
    
    [postparams setObject:fileArray forKey:@"answer_data"];
    
     DebugLog(@"%@",postparams);
    
    /////////////////////////////////////////////////////////////////
    [httpClient postPath:POST_INSTALLATION_FORM_QUESTIONS parameters:postparams success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"json afnetworking ->%@",json);
        if(json  != nil)
        {
            if(json != nil && [[json  objectForKey:@"success"] isEqualToString:@"true"])
            {
                [PIMDAnalyticsLogger logEvent:@"Installation_Submit_Success"];
                [PIMDGlobalOperations showAlert:@"Data uploaded successfully."];
                [self performSelector:@selector(popToModuleController) withObject:nil afterDelay:1.0];
            } else {
                if(json != nil && [[json  objectForKey:@"success"] isEqualToString:@"false"]) {
                    [PIMDAnalyticsLogger logEvent:@"Installation_Submit_Failed"];
                    if([json  objectForKey:@"code"] != nil &&  [[json  objectForKey:@"code"] isEqualToString:FRONTLINE_INVALID_USER_AUTH_CODE])
                    {
                        if([PIMDGlobalOperations isFrontlineMemberLoggedIn]){
                            [PIMDGlobalOperations showAlert:FRONTLINE_INVALID_USER_AUTH_MESSAGE];
                            [PIMDGlobalOperations clearFrontlineMemberDetails];
                            [self performSelector:@selector(popToLoginController) withObject:nil afterDelay:1.0];
                        }
                    } else {
                        NSString *message = [json objectForKey:@"message"];
                        if([message length] > 0)
                        {
                            [PIMDGlobalOperations showAlert:message];
                        }
                    }
                }
            }
        }
        else {
           [PIMDGlobalOperations showAlert:@"No response from server. Please try again later."];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        [CommonCallback hideProgressHud];
    }];
    
}

-(void)sendStoreImageRequest:(UIImage *)pickerimage
{
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:API_KEY];
    //    if((recceId != nil) && ![recceId isEqualToString:@""])
    //    {
    //        [httpClient setDefaultHeader:@"X-HTTP-Method-Override" value:@"PUT"];
    //    }
    [httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    NSMutableDictionary *postparams = [[NSMutableDictionary alloc] init];
    /////////////////////////////////////////////////////////////////
    [postparams setObject:passedstoreid forKey:@"store_id"];
    [postparams setObject:passedcategoryid forKey:@"category_id"];
    if([aprrovalid length] > 0)
    {
        NSLog(@"approval id - > %@", aprrovalid);
        [postparams setObject:aprrovalid forKey:@"approval_id"];
    }
    [postparams setObject:[PIMDGlobalOperations getFrontlineMemberId] forKey:@"member_id"];
    [postparams setObject:[PIMDGlobalOperations getFrontlineMemberAuthId] forKey:@"auth_id"];
    //////////////////////////////////////////////////////////
    bool result = CGSizeEqualToSize(pickerimage.size, CGSizeZero);
    if(result==0) //Not empty
    {
        NSData *dataObj = UIImageJPEGRepresentation(pickerimage, 0.75);
        NSString *image = [PIMD_APP_DELEGATE base64forData:dataObj];
        [postparams setObject:image forKey:@"inspection_img"];
        [postparams setObject:@"image/jpeg" forKey:@"filetype"];
    }
    /////////////////////////////////////////////////////////////////
    DebugLog(@"----%@------",postparams);
    /////////////////////////////////////////////////////////////////
    [httpClient postPath:ADD_GALLERY parameters:postparams success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
       DebugLog(@"json afnetworking ->%@",json);
        if(json  != nil)
        {
            if(json != nil && [[json  objectForKey:@"success"] isEqualToString:@"true"])
            {
                //[PIMDGlobalOperations showAlert:[json  objectForKey:@"message"]];
                [PIMDAnalyticsLogger logEvent:@"Installation_Inspection_Success"];
                NSDictionary *dict = [json  objectForKey:@"data"];
                aprrovalid = [dict objectForKey:@"id"];
                NSLog(@"approval id - > %@", aprrovalid);
                inspectionImageAdded = @"1";
                formImageView.image = pickerimage;
            }
             else if(json != nil && [[json  objectForKey:@"success"] isEqualToString:@"false"])
             {
                 inspectionImageAdded = @"0";
                [PIMDAnalyticsLogger logEvent:@"Installation_Inspection_Failed"];
                 if([json  objectForKey:@"code"] != nil &&  [[json  objectForKey:@"code"] isEqualToString:FRONTLINE_INVALID_USER_AUTH_CODE])
                 {
                     if([PIMDGlobalOperations isFrontlineMemberLoggedIn]){
                         [PIMDGlobalOperations showAlert:FRONTLINE_INVALID_USER_AUTH_MESSAGE];
                         [PIMDGlobalOperations clearFrontlineMemberDetails];
                         [self performSelector:@selector(popToLoginController) withObject:nil afterDelay:1.0];
                     } else {
                         NSString *message = [json objectForKey:@"message"];
                         if([message length] > 0)
                         {
                             [PIMDGlobalOperations showAlert:message];
                         }
                     }
                 }
             }
        } else {
            [PIMDGlobalOperations showAlert:@"No response from server. Please try again later."];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        [CommonCallback hideProgressHud];
    }];
    
}

-(void)sendStoreSignatureRequest:(UIImage *)pickerimage
{
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:API_KEY];
    //    if((recceId != nil) && ![recceId isEqualToString:@""])
    //    {
    //        [httpClient setDefaultHeader:@"X-HTTP-Method-Override" value:@"PUT"];
    //    }
    [httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    NSMutableDictionary *postparams = [[NSMutableDictionary alloc] init];
    /////////////////////////////////////////////////////////////////
    [postparams setObject:passedstoreid forKey:@"store_id"];
    [postparams setObject:passedcategoryid forKey:@"category_id"];
    if([aprrovalid length] > 0)
    {
        NSLog(@"approval id - > %@", aprrovalid);
        [postparams setObject:aprrovalid forKey:@"approval_id"];
    }
    [postparams setObject:[PIMDGlobalOperations getFrontlineMemberId] forKey:@"member_id"];
    [postparams setObject:[PIMDGlobalOperations getFrontlineMemberAuthId] forKey:@"auth_id"];
    //////////////////////////////////////////////////////////
    bool result = CGSizeEqualToSize(pickerimage.size, CGSizeZero);
    if(result==0) //Not empty
    {
        NSData *dataObj = UIImageJPEGRepresentation(pickerimage, 0.75);
        NSString *image = [PIMD_APP_DELEGATE base64forData:dataObj];
        [postparams setObject:image forKey:@"approval_img"];
        [postparams setObject:@"image/jpeg" forKey:@"filetype"];
    }
    /////////////////////////////////////////////////////////////////
    DebugLog(@"----%@------",postparams);
    /////////////////////////////////////////////////////////////////
    [httpClient postPath:ADD_GALLERY parameters:postparams success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"json afnetworking ->%@",json);
        if(json  != nil)
        {
            if(json != nil && [[json  objectForKey:@"success"] isEqualToString:@"true"])
            {
                //[PIMDGlobalOperations showAlert:[json  objectForKey:@"message"]];
                [PIMDAnalyticsLogger logEvent:@"Installation_Approval_Success"];
                NSDictionary *dict = [json  objectForKey:@"data"];
                aprrovalid = [dict objectForKey:@"id"];
                NSLog(@"approval id - > %@", aprrovalid);
                approvalImageAdded = @"1";
                self.signatureImageView.image = pickerimage;
            }
            else if(json != nil && [[json  objectForKey:@"success"] isEqualToString:@"false"])
            {
                approvalImageAdded = @"0";
                [PIMDAnalyticsLogger logEvent:@"Installation_Approval_Failed"];
                if([json  objectForKey:@"code"] != nil &&  [[json  objectForKey:@"code"] isEqualToString:   FRONTLINE_INVALID_USER_AUTH_CODE])
                {
                    if([PIMDGlobalOperations isFrontlineMemberLoggedIn]){
                        [PIMDGlobalOperations showAlert:FRONTLINE_INVALID_USER_AUTH_MESSAGE];
                        [PIMDGlobalOperations clearFrontlineMemberDetails];
                        [self performSelector:@selector(popToLoginController) withObject:nil afterDelay:1.0];
                    } else {
                        NSString *message = [json objectForKey:@"message"];
                        if([message length] > 0)
                        {
                            [PIMDGlobalOperations showAlert:message];
                        }
                    }
                }
            }
        } else {
            [PIMDGlobalOperations showAlert:@"No response from server. Please try again later."];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        [CommonCallback hideProgressHud];
    }];
    
}

-(void) popToLoginController
{
   // DebugLog(@"popToRootViewControllerAnimated:YES");
    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(void) popToModuleController
{
    for (UIViewController *controller in self.navigationController.viewControllers) {
        
        if ([controller isKindOfClass:[PIMDModuleSelectionViewController class]]) {
            
            [self.navigationController popToViewController:controller animated:YES];
            break;
        }
    }

}

#pragma PIMDSignatureViewControllerDelegate method
-(void)addImageToSignatureView:(UIImage *)editedImage
{
    //self.signatureImageView.image = editedImage;
    [self sendStoreSignatureRequest:editedImage];
}

-(void)setUIForArray
{
    for(UIView *v in multiView.subviews)
    {
        if([v isKindOfClass:[UIButton class]])
        {
            ((UIButton*)v).hidden = YES;
            [((UIButton*)v) setSelected:FALSE];
        } else if ([v isKindOfClass:[UITextView class]]) {
            ((UITextView*)v).hidden = YES;
        }
    }
    
    if(currentObjID < 0)
        return;
    PIMDQuestionObject *quest = [questionsArray objectAtIndex:currentObjID];
    multilbl.text = quest.question;
    
    
    if([quest.questiontype isEqualToString:@"tic"] || [quest.questiontype isEqualToString:@"tic-txt"])
    {
        if([quest.questiontype isEqualToString:@"tic-txt"])
        {
            [self setTextViewMovedUp:NO];
            commentsTextView.hidden = FALSE;
            commentsTextView.text = quest.optionsText;
        }
        for (int i = 0; i < [quest.options count]; i++) {
            //DebugLog(@"%d %@",[quest.options count], [quest.options objectAtIndex:i]);
            NSString *key = [quest.options objectAtIndex:i];
            switch (i) {
                case 0:
                {
                    [multiBtn1 setTitle:[quest.options objectAtIndex:i] forState:UIControlStateNormal];
                    multiBtn1.hidden = FALSE;
                    [multiBtn1 setSelected:[[quest.optionsAnswers objectForKey:key] boolValue]];
                }
                    break;
                    
                case 1:
                {
                    [multiBtn2 setTitle:[quest.options objectAtIndex:i] forState:UIControlStateNormal];
                    multiBtn2.hidden = FALSE;
                    [multiBtn2 setSelected:[[quest.optionsAnswers objectForKey:key] boolValue]];
                }
                    break;
                    
                case 2:
                {
                    [multiBtn3 setTitle:[quest.options objectAtIndex:i] forState:UIControlStateNormal];
                    multiBtn3.hidden = FALSE;
                    [multiBtn3 setSelected:[[quest.optionsAnswers objectForKey:key] boolValue]];
                }
                    break;
                    
                case 3:
                {
                    [multiBtn4 setTitle:[quest.options objectAtIndex:i] forState:UIControlStateNormal];
                    multiBtn4.hidden = FALSE;
                    [multiBtn4 setSelected:[[quest.optionsAnswers objectForKey:key] boolValue]];
                }
                    break;
                    
                case 4:
                {
                    [multiBtn5 setTitle:[quest.options objectAtIndex:i] forState:UIControlStateNormal];
                    multiBtn5.hidden = FALSE;
                    [multiBtn5 setSelected:[[quest.optionsAnswers objectForKey:key] boolValue]];
                }
                    break;
                    
                default:
                    break;
            }
        }
    } else if ([quest.questiontype isEqualToString:@"txt"]) {
        [self setTextViewMovedUp:YES];
        commentsTextView.hidden = FALSE;
        commentsTextView.text = quest.optionsText;
    }
    if(currentObjID == 0)
    {
        [prevBtn setEnabled:FALSE];
        [nextBtn setEnabled:TRUE];
    }
//    if(currentObjID > 0)
//    {
//        [prevBtn setEnabled:TRUE];
//    } else {
//        [prevBtn setEnabled:FALSE];
//    }
//    if(currentObjID == [questionsArray count])
//    {
//        [nextBtn setEnabled:FALSE];
//        [nextBtn setHidden:TRUE];
//        [submitBtn setHidden:FALSE];
//    } else {
//        [nextBtn setEnabled:TRUE];
//        [nextBtn setHidden:FALSE];
//        [submitBtn setHidden:TRUE];
//    }
}

-(void)setButtonData:(UIButton *)sender atIndex:(int)index
{
    PIMDQuestionObject *quest = [questionsArray objectAtIndex:currentObjID];
    NSString *key = [quest.options objectAtIndex:index];
    if([sender isSelected])
    {
        [sender setSelected:FALSE];
        [quest.optionsAnswers setObject:[NSNumber numberWithBool:NO] forKey:key];
    } else {
        [sender setSelected:TRUE];
        [quest.optionsAnswers setObject:[NSNumber numberWithBool:YES] forKey:key];
    }
    if([quest.optiontype isEqualToString:@"single"])
    {
        [self unsetOtherButtons:sender];
    }
}

-(void)unsetOtherButtons:(UIButton *)sender
{
    PIMDQuestionObject *quest = [questionsArray objectAtIndex:currentObjID];
    for(UIView *v in multiView.subviews)
    {
        if([v isKindOfClass:[UIButton class]])
        {
            if(![((UIButton*)v) isEqual:sender])
            {
                if(((UIButton*)v).isSelected)
                {
                    NSString *key;
                    if ([((UIButton*)v) isEqual:multiBtn1]) {
                        key = [quest.options objectAtIndex:0];
                    } else if ([((UIButton*)v) isEqual:multiBtn2]) {
                        key = [quest.options objectAtIndex:1];
                    } else if ([((UIButton*)v) isEqual:multiBtn3]) {
                        key = [quest.options objectAtIndex:2];
                    } else if ([((UIButton*)v) isEqual:multiBtn4]) {
                        key = [quest.options objectAtIndex:3];
                    } else if ([((UIButton*)v) isEqual:multiBtn5]) {
                        key = [quest.options objectAtIndex:4];
                    }
                    [((UIButton*)v) setSelected:FALSE];
                    [quest.optionsAnswers setObject:[NSNumber numberWithBool:NO] forKey:key];
                }
            }
        }
    }
}

- (IBAction)multiBtn1Pressed:(UIButton *)sender {
    [self setButtonData:sender atIndex:0];
}

- (IBAction)multiBtn2Pressed:(UIButton *)sender {
    [self setButtonData:sender atIndex:1];
}

- (IBAction)multiBtn3Pressed:(UIButton *)sender {
    [self setButtonData:sender atIndex:2];
}

- (IBAction)multiBtn4Pressed:(UIButton *)sender {
    [self setButtonData:sender atIndex:3];
}

- (IBAction)multiBtn5Pressed:(UIButton *)sender {
    [self setButtonData:sender atIndex:4];
}


- (void)textViewDidBeginEditing:(UITextView *)textView{
    
    [self setViewMovedUp:YES];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    PIMDQuestionObject *quest = [questionsArray objectAtIndex:currentObjID];
    if ([textView.text isEqualToString:@""] || [[textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0) {
        quest.optionsText = @"";
    } else {
        quest.optionsText = commentsTextView.text;
    }
    [self setViewMovedUp:NO];
    [textView resignFirstResponder];
}

- (IBAction)previousBtnPressed:(id)sender {
    if (animstart) {
        return;
    }
    DebugLog(@"%d %d",currentObjID,[questionsArray count]);
    if(currentObjID <= [questionsArray count])
    {
        currentObjID = currentObjID - 1;
        [self setUIForArray];
        [self slidefromLeftAnimation:multiView];
        [self slidefromLeftAnimation:submitView];
        [self bounceback];
        DebugLog(@"%d %d",currentObjID,[questionsArray count]);
        if(currentObjID == 0)
        {
            [prevBtn setEnabled:FALSE];
        } else {
            [prevBtn setEnabled:TRUE];
        }
        if((currentObjID + 1) == [questionsArray count])
        {
            [nextBtn setEnabled:TRUE];
            [nextBtn setHidden:FALSE];
            [multiView setHidden:FALSE];
            [submitBtn setHidden:TRUE];
            [submitView setHidden:TRUE];
        }
    }
}

- (IBAction)doneBtnPressed:(id)sender {
    if ([PIMD_APP_DELEGATE networkavailable]) {
        UIAlertView *submitAlert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"Submit this data on server ?" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"NO", nil];
        submitAlert.tag = 1;
        [submitAlert show];
    } else {
        [PIMDGlobalOperations showOfflineAlert];
    }
}


- (IBAction)nextBtnPressed:(id)sender {
    if (animstart) {
        return;
    }
    DebugLog(@"next --- > %d %d",currentObjID,[questionsArray count]);
    if(currentObjID < [questionsArray count])
    {
        currentObjID = currentObjID + 1;
        DebugLog(@"%d %d",currentObjID,[questionsArray count]);
        if(currentObjID == [questionsArray count])
        {
            [self slidefromRightAnimation:multiView];
            [self slidefromRightAnimation:submitView];
            [nextBtn setEnabled:FALSE];
            [nextBtn setHidden:TRUE];
            [multiView setHidden:TRUE];
            [submitBtn setHidden:FALSE];
            [submitView setHidden:FALSE];
            
        } else {
            [self setUIForArray];
            [self slidefromRightAnimation:multiView];
            [self bouncefwd];
            [nextBtn setEnabled:TRUE];
            [nextBtn setHidden:FALSE];
            [multiView setHidden:FALSE];
            [submitBtn setHidden:TRUE];
            [submitView setHidden:TRUE];
        }
        if(currentObjID > 0)
        {
            [prevBtn setEnabled:TRUE];
        } else {
            [prevBtn setEnabled:FALSE];
        }
    } 
}

#pragma animations
-(void)slidefromRightAnimation:(UIView *)view
{
    // set up an animation for the transition between the views
    CATransition *animation = [CATransition animation];
    [animation setDuration:0.5];
    animation.delegate = self;
    [animation setType:kCATransitionPush];
    [animation setSubtype:kCATransitionFromRight];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [animation setValue:@"SwitchFromRight" forKey:@"animationID"];
    [[view layer] addAnimation:animation forKey:@"SwitchFromRight"];
}

-(void)slidefromLeftAnimation:(UIView *)view
{
    // set up an animation for the transition between the views
    CATransition *animation = [CATransition animation];
    [animation setDuration:0.5];
    animation.delegate = self;
    [animation setType:kCATransitionPush];
    [animation setSubtype:kCATransitionFromLeft];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [animation setValue:@"SwitchFromLeft" forKey:@"animationID"];
    [[view layer] addAnimation:animation forKey:@"SwitchFromLeft"];
}

-(void)bouncefwd
{
    CABasicAnimation *bounceAnimation =
    [CABasicAnimation animationWithKeyPath:@"position.x"];
    bounceAnimation.duration = 0.2;
    bounceAnimation.fromValue = [NSNumber numberWithInt:0];
    bounceAnimation.toValue = [NSNumber numberWithInt:10];
    bounceAnimation.repeatCount = 2;
    bounceAnimation.autoreverses = YES;
    bounceAnimation.fillMode = kCAFillModeForwards;
    bounceAnimation.removedOnCompletion = YES;
    bounceAnimation.additive = YES;
    [multiView.layer addAnimation:bounceAnimation forKey:@"bounceAnimation"];
}

-(void)bounceback
{
    CABasicAnimation *bounceAnimation =
    [CABasicAnimation animationWithKeyPath:@"position.x"];
    bounceAnimation.duration = 0.2;
    bounceAnimation.fromValue = [NSNumber numberWithInt:0];
    bounceAnimation.toValue = [NSNumber numberWithInt:-10];
    bounceAnimation.repeatCount = 2;
    bounceAnimation.autoreverses = YES;
    bounceAnimation.fillMode = kCAFillModeForwards;
    bounceAnimation.removedOnCompletion = YES;
    bounceAnimation.additive = YES;
    [multiView.layer addAnimation:bounceAnimation forKey:@"bounceAnimation"];
}

- (void)animationDidStart:(CAAnimation *)anim
{
    animstart = 1;
}

- (void)animationDidStop:(CAAnimation *)theAnimation finished:(BOOL)flag
{
    animstart = 0;
    if([[theAnimation valueForKey:@"animationID"] isEqual:@"SwitchFromRight"])
    {
        [[multiView layer] removeAnimationForKey:@"SwitchFromRight"];
    } else {
        [[submitView layer] removeAnimationForKey:@"SwitchFromLeft"];
    }
}

@end
