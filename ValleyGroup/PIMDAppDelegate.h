//
//  PIMDAppDelegate.h
//  ValleyGroup
//
//  Created by Rishi on 26/03/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "constants.h"

#define PIMD_APP_DELEGATE (PIMDAppDelegate *)[[UIApplication sharedApplication] delegate]

#define FRONTLINE_TEAM      1
#define CLIENT              2

@class PIMDLoginViewController;
@class PIMDTeamLoginViewController;
@class PIMDOptionViewController;
@interface PIMDAppDelegate : UIResponder <UIApplicationDelegate>
{
    Reachability* internetReach;
    Boolean networkavailable;
    UINavigationController *clientnavigationController;
    UINavigationController *teamnavigationController;
}

@property (strong, nonatomic) UIWindow *window;
@property(nonatomic) Boolean networkavailable;
@property (strong, nonatomic) PIMDLoginViewController *clientloginviewController;
@property (strong, nonatomic) PIMDTeamLoginViewController *teamloginviewController;
@property (strong, nonatomic) PIMDOptionViewController *optionviewController;

-(void)pushViewController:(int)type;
-(void)switchViewController:(int)type;
- (NSString*)base64forData:(NSData*)theData ;

@end
