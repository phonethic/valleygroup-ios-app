//
//  PIMDOptionViewController.h
//  ValleyGroup
//
//  Created by Rishi on 18/04/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EAIntroView.h"

@interface PIMDOptionViewController : UIViewController <EAIntroDelegate> {
    
}
@property (strong, nonatomic) IBOutlet UIButton *teamBtn;
@property (strong, nonatomic) IBOutlet UIButton *clientBtn;

- (IBAction)teamBtnPressed:(id)sender;
- (IBAction)clientBtnPressed:(id)sender;
- (IBAction)phonethicsBtnPressed:(id)sender;

@end
