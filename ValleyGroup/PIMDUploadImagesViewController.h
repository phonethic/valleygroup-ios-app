//
//  PIMDUploadImagesViewController.h
//  ValleyGroup
//
//  Created by Rishi on 06/05/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "ImageFilterViewController.h"

@interface PIMDUploadImagesViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate,CropImageFilterViewControllerDelegate>
{
    int imgPickerTag;
    int selectedRow;
}
@property (nonatomic) BOOL newMedia;
@property (strong, nonatomic) NSMutableArray *uploadArray;
@property (nonatomic, copy) NSString *passedstoreid;
@property (nonatomic, copy) NSString *passeddesignid;
@property (nonatomic, strong) NSString *recceId;
@property (strong, nonatomic) IBOutlet UITableView *uploadImageTableView;

- (IBAction)nextBtnPressed:(id)sender;

@end
