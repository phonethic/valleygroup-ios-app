//
//  PIMDGlobalOperations.m
//  ValleyGroup
//
//  Created by Rishi on 21/04/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import "PIMDGlobalOperations.h"
#import "constants.h"

@implementation PIMDGlobalOperations

+(void)showAlert:(NSString*) message
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    [alertView show];
}

+(void)showOfflineAlert
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                        message:ALERT_MESSAGE_NO_NETWORK
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    [alertView show];
}


+(BOOL)isCustomerLoggedIn
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *cpass = [defaults objectForKey:@"Customer_pass"];
    NSString *cauthid = [defaults objectForKey:@"Customer_authid"];
    NSString *cauthcode = [defaults objectForKey:@"Customer_auth_code"];
    DebugLog(@"%@  ",cpass);
    if([cpass length] > 0 && [cauthid length] > 0 && [cauthcode length] > 0)
    {
        return TRUE;
    } else {
        return FALSE;
    }
}

+(void)setCustomerDetails:(NSString *)idvalue pass:(NSString *)passvalue;
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:idvalue forKey:@"Customer_id"];
    [defaults setObject:passvalue forKey:@"Customer_pass"];
    [defaults synchronize];
}

+(void)setCustomerAuthDetails:(NSString *)idvalue code:(NSString *)codevalue
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:idvalue forKey:@"Customer_authid"];
    [defaults setObject:codevalue forKey:@"Customer_auth_code"];
    [defaults synchronize];
}

+(NSString *)getCustomerId
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"Customer_id"] == nil)
        return @"";
    return [defaults objectForKey:@"Customer_id"];
}

+(NSString *)getCustomerAuthId
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"Customer_authid"] == nil)
        return @"";
    return [defaults objectForKey:@"Customer_authid"];
}

+(NSString *)getCustomerAuthCode
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"Customer_auth_code"] == nil)
        return @"";
    return [defaults objectForKey:@"Customer_auth_code"];
}

+(void)clearCustomerDetails
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"" forKey:@"Customer_pass"];
    [defaults setObject:@"" forKey:@"Customer_authid"];
    [defaults setObject:@"" forKey:@"Customer_auth_code"];
    [defaults synchronize];
}


+(BOOL)isFrontlineMemberLoggedIn
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *memid = [defaults objectForKey:@"FrontlineMember_memberid"];
    NSString *cauthid = [defaults objectForKey:@"FrontlineMember_authid"];
    DebugLog(@"%@  ", cauthid);
    if([memid length] > 0 && [cauthid length] > 0 )
    {
        return TRUE;
    } else {
        return FALSE;
    }
}

+(void)setFrontlineMemberDetails:(NSString *)idvalue pass:(NSString *)passvalue
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:idvalue forKey:@"FrontlineMember_phone"];
    [defaults setObject:passvalue forKey:@"FrontlineMember_pass"];
    [defaults synchronize];
}

+(void)setFrontlineMemberAuthDetails:(NSString *)idvalue code:(NSString *)codevalue
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:idvalue forKey:@"FrontlineMember_memberid"];
    [defaults setObject:codevalue forKey:@"FrontlineMember_authid"];
    [defaults synchronize];
}

+(NSString *)getFrontlineMemberPhone
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"FrontlineMember_phone"] == nil)
        return @"";
    return [defaults objectForKey:@"FrontlineMember_phone"];
}

+(NSString *)getFrontlineMemberId
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"FrontlineMember_memberid"] == nil)
        return @"";
    return [defaults objectForKey:@"FrontlineMember_memberid"];
}

+(NSString *)getFrontlineMemberAuthId
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"FrontlineMember_authid"] == nil)
        return @"";
    return [defaults objectForKey:@"FrontlineMember_authid"];
}

+(void)clearFrontlineMemberDetails
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"" forKey:@"FrontlineMember_pass"];
    [defaults setObject:@"" forKey:@"FrontlineMember_memberid"];
    [defaults setObject:@"" forKey:@"FrontlineMember_authid"];
    [defaults synchronize];
}
@end
