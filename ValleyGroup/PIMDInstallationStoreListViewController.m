//
//  PIMDInstallationStoreListViewController.m
//  ValleyGroup
//
//  Created by Rishi on 05/05/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import "PIMDInstallationStoreListViewController.h"
#import "constants.h"
#import "CommonCallback.h"
#import "PIMDAppDelegate.h"
#import "PIMDStoreObject.h"
#import "PIMDInstallationListViewController.h"

#define GET_INSTALLATION_STORE_LIST [NSString stringWithFormat:@"%@%@%@store_api/stores?install_module=1",LIVE_SERVER,URL_PREFIX,API_VERSION]

@interface PIMDInstallationStoreListViewController ()
@property(strong) NSDictionary *splashJson;
@end

@implementation PIMDInstallationStoreListViewController
@synthesize storeArray;
@synthesize searchResults;
@synthesize  installationStoreTableView;
@synthesize searchTextField;
@synthesize searchBtn;
@synthesize searchView;
@synthesize lblSearchResults;
@synthesize viewSearchResults;
@synthesize textPull, textRelease, textLoading, refreshHeaderView, refreshLabel, refreshArrow, refreshSpinner;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self setupStrings];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if([self respondsToSelector:@selector(setEdgesForExtendedLayout:)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        
    }
    
    UITapGestureRecognizer *viewTap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapDetected:)];
    viewTap.numberOfTapsRequired = 1;
    viewTap.cancelsTouchesInView = NO;
    [self.view  addGestureRecognizer:viewTap];
    
    storeArray = [[NSMutableArray alloc] init];
    searchResults = [[NSMutableArray alloc] init];
    
    [self addPullToRefreshHeader];
    [installationStoreTableView setBackgroundColor:[UIColor clearColor]];

    
    if ([PIMD_APP_DELEGATE networkavailable]) {
        [self sendInstallationStoreListRequest];
    } else {
        [PIMDGlobalOperations showOfflineAlert];
    }
    searchTextField.font    = DEFAULT_FONT(20);
    lblSearchResults.font   = DEFAULT_FONT(15);
    viewSearchResults.hidden = YES;
    [installationStoreTableView setFrame:CGRectMake(installationStoreTableView.frame.origin.x,CGRectGetMaxY(searchView.frame)+2, installationStoreTableView.frame.size.width, self.view.frame.size.height - (CGRectGetMaxY(searchView.frame)+2))];
}

- (void)tapDetected:(UIGestureRecognizer *)sender {
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) popToLoginController
{
    DebugLog(@"popToRootViewControllerAnimated:YES");
    [self.navigationController popToRootViewControllerAnimated:YES];
}


-(void)sendInstallationStoreListRequest
{
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:GET_INSTALLATION_STORE_LIST]];
    DebugLog(@"%@",GET_INSTALLATION_STORE_LIST);
    [urlRequest setValue:API_KEY forHTTPHeaderField:@"X-API-KEY"];
    [urlRequest setValue:[PIMDGlobalOperations getFrontlineMemberId] forHTTPHeaderField:@"member_id"];
    [urlRequest setValue:[PIMDGlobalOperations getFrontlineMemberAuthId] forHTTPHeaderField:@"auth_id"];
    DebugLog(@"%@",[PIMDGlobalOperations getFrontlineMemberId]);
    DebugLog(@"%@",[PIMDGlobalOperations getFrontlineMemberAuthId]);
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: urlRequest
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        [CommonCallback hideProgressHud];
                                                        DebugLog(@"%@",self.splashJson);
                                                        if(self.splashJson  != nil)
                                                        {
                                                            [storeArray removeAllObjects];
                                                            if(isLoading)
                                                            {
                                                                [self stopLoading];
                                                            }
                                                            if([[self.splashJson  objectForKey:@"success"] isEqualToString:@"true"])
                                                            {
                                                                NSArray *dataArray = [self.splashJson  objectForKey:@"data"];
                                                                for (int index = 0; index < [dataArray count]; index++) {
                                                                    NSDictionary *dict = [dataArray objectAtIndex:index];
                                                                    PIMDStoreObject *store = [[PIMDStoreObject alloc] init];
                                                                    store.storeid = [dict objectForKey:@"store_id"];
                                                                    store.storename = [dict objectForKey:@"store_name"];
                                                                    store.storecity = [dict objectForKey:@"city_name"];
                                                                    store.storestatus = [dict objectForKey:@"status"];
                                                                    store.storeAddress = [dict objectForKey:@"store_address"];
                                                                    store.storeDescription = [dict objectForKey:@"store_description"];
                                                                    store.projectid = [dict objectForKey:@"project_id"];
                                                                    store.projectname = [dict objectForKey:@"project_name"];
                                                                    [storeArray addObject:store];
                                                                }
                                                                
                                                                DebugLog(@"%@",((PIMDStoreObject *)[storeArray objectAtIndex:0]).storecity);
                                                            } else {
                                                                DebugLog(@"Login Failed");
                                                                if(self.splashJson != nil && [[self.splashJson  objectForKey:@"success"] isEqualToString:@"false"]) {
                                                                    if([self.splashJson  objectForKey:@"code"] != nil &&  [[self.splashJson  objectForKey:@"code"] isEqualToString:FRONTLINE_INVALID_USER_AUTH_CODE])
                                                                    {
                                                                        if([PIMDGlobalOperations isFrontlineMemberLoggedIn]){
                                                                            [PIMDGlobalOperations showAlert:FRONTLINE_INVALID_USER_AUTH_MESSAGE];
                                                                            [PIMDGlobalOperations clearFrontlineMemberDetails];
                                                                            [self performSelector:@selector(popToLoginController) withObject:nil afterDelay:1.0];
                                                                        }
                                                                    } else {
                                                                        NSString *message = [self.splashJson objectForKey:@"message"];
                                                                        if([message length] > 0)
                                                                        {
                                                                            [PIMDGlobalOperations showAlert:message];
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }  else {
                                                            [PIMDGlobalOperations showAlert:@"No response from server. Please try again later."];
                                                        }
                                                        [installationStoreTableView reloadData];
                                                        
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        //[hud hide:YES];
                                                        [CommonCallback hideProgressHud];
                                                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                                                     message:[NSString stringWithFormat:@"%@",[error localizedDescription]]
                                                                                                    delegate:nil
                                                                                           cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                        [av show];
                                                        if(isLoading)
                                                        {
                                                            [self stopLoading];
                                                        }
                                                    }];
    
    
    
    [operation start];
}

#pragma mark - Table view data source
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 140;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    DebugLog(@"searchTextField.text - > %d, searchResults - > %d",[searchTextField.text length],[searchResults count]);
    if (([searchTextField.text length] > 0 && ![searchTextField.text isEqualToString:@"Find stores"] )|| [searchResults count] > 0) {
        return [searchResults count];
    } else {
        return [storeArray count];
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UILabel *lblTitle;
    UILabel *lblDesc;
    UIView *backView;
    UIImageView *thumbImg;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.backgroundColor = [UIColor clearColor];
        
        backView = [[UIView alloc] initWithFrame:CGRectMake(0,2, 280, 136)];
        backView.tag = 101;
        backView.backgroundColor    =  [UIColor whiteColor];
        backView.clipsToBounds      = YES;
        [cell.contentView addSubview:backView];
        
        thumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(240, 5, 30, 30	)];
        thumbImg.tag = 104;
        thumbImg.contentMode = UIViewContentModeScaleAspectFit;
        //thumbImg.image   = [UIImage imageNamed:@"milestone.png"];
        [backView addSubview:thumbImg];
        
        
        lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(10,0,260, 40.0)];
        lblTitle.text                   = @"";
        lblTitle.tag                    = 102;
        lblTitle.font                   = DEFAULT_FONT(25);
        lblTitle.textAlignment          = NSTextAlignmentLeft;
        lblTitle.textColor              = DEFAULT_COLOR;
        lblTitle.highlightedTextColor   = [UIColor whiteColor];
        lblTitle.backgroundColor        = [UIColor clearColor];
        lblTitle.numberOfLines          = 1;
        [backView addSubview:lblTitle];
        
        
        lblDesc = [[UILabel alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(lblTitle.frame),260,80.0)];
        lblDesc.text                    = @"";
        lblDesc.tag                     = 103;
        lblDesc.numberOfLines           = 4;
        lblDesc.font                    = DEFAULT_FONT(14);
        lblDesc.textAlignment           = NSTextAlignmentLeft;
        lblDesc.textColor               = DEFAULT_SELECTED_COLOR;
        lblDesc.highlightedTextColor    = [UIColor whiteColor];
        lblDesc.backgroundColor         = [UIColor clearColor];
        [backView addSubview:lblDesc];
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor     = DEFAULT_SELECTED_COLOR;
        cell.selectedBackgroundView     = bgColorView;
        
    }
    
    if ([indexPath row] < [storeArray count]) {
        PIMDStoreObject *store = nil;
        if ([searchResults count] > 0) {
            store = [searchResults objectAtIndex:indexPath.row];
        } else {
            store = [storeArray objectAtIndex:indexPath.row];
        }
        
        backView        = (UILabel *)[cell viewWithTag:101];
        lblTitle        = (UILabel *)[backView viewWithTag:102];
        lblDesc         = (UILabel *)[backView viewWithTag:103];
        thumbImg        = (UIImageView *)[backView viewWithTag:104];
        if([store.storestatus isEqualToString:@"1"]) //Installation in progress
        {
            thumbImg.image   = [UIImage imageNamed:@"status_icon_installation.png"];
        } else if ([store.storestatus isEqualToString:@"2"]) { //In Tansit
            thumbImg.image   = [UIImage imageNamed:@"status_icon_transit.png"];
        } else if ([store.storestatus isEqualToString:@"3"]) { //Completed
            thumbImg.image   = [UIImage imageNamed:@"status_icon_completed.png"];
        } else {
            thumbImg.image   = [UIImage imageNamed:@"status_icon_not_begun.png"];
        }
        [lblTitle setText:store.storename];
        [lblDesc  setText:store.storeAddress];
    }
    //    cell.textLabel.text = store.storename;
    //    cell.detailTextLabel.text = store.storeAddress;
    
    return cell;
}



#pragma mark - Table view delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DebugLog(@"storeArray - > %d, resultArray - > %d",[storeArray count],[searchResults count]);
    DebugLog(@"%d",indexPath.row);
    
    PIMDStoreObject *store = nil;
    if ([searchResults count] > 0) {
        store  = [searchResults objectAtIndex:indexPath.row];
        DebugLog(@"status-->%@",store.storestatus);
    } else {
        store  = [storeArray objectAtIndex:indexPath.row];
        DebugLog(@"status-->%@",store.storestatus);
    }
    
    DebugLog(@"status-->%@",store.storestatus);
    
    
    if([store.storeAddress length] > 0 && [store.storename length] > 0)
    {
        NSDictionary *storeParams = [NSDictionary dictionaryWithObjectsAndKeys:store.storename,@"StoreName", store.storeAddress,@"StoreAdd", nil];
        DebugLog(@"%@",storeParams);
        [PIMDAnalyticsLogger logEvent:@"Installation_Selected_Store" withParams:storeParams];
    }
    
    PIMDInstallationListViewController *designController = [[PIMDInstallationListViewController alloc] initWithNibName:@"PIMDInstallationListViewController" bundle:nil] ;
    designController.title = @"Store Status";
    designController.passedstoreid = store.storeid;
    [self.navigationController pushViewController:designController animated:YES];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if ([textField isEqual:searchTextField]) {
        if ([searchTextField.text isEqualToString:@"Find stores"]) {
            searchTextField.text = @"";
        }
        [searchBtn setBackgroundImage:[UIImage imageNamed:@"close_search_icon.png"] forState:UIControlStateNormal];
        searchBtn.selected = YES;
        searchView.backgroundColor = DEFAULT_SELECTED_COLOR;
    }
    return YES;
}

//- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
//{
//    NSLog(@"--->>>%@",textField.text);
//    [searchResults removeAllObjects];
//    return YES;
//}
//
//
//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
//    NSString *searchText = [textField.text stringByReplacingCharactersInRange:range withString:string];
//    //NSLog(@"--->>>%@",searchText);
//    for (PIMDStoreObject *store in storeArray)
//    {
//        NSRange nameRange = [store.storename rangeOfString:searchText options:NSCaseInsensitiveSearch];
//        //NSRange descriptionRange = [food.description rangeOfString:text options:NSCaseInsensitiveSearch];
//        if(nameRange.location != NSNotFound)
//        {
//            [searchResults addObject:store];
//            [storeListTableView reloadData];
//        }
//    }
//    return YES;
//}
//- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
//{
//    [searchResults removeAllObjects];
//    [storeListTableView reloadData];
//    return YES;
//}


- (IBAction)searchTextChanged:(UITextField*)sender {
    DebugLog(@"--->>>%@",sender.text);
    if ([sender.text length] == 0)
    {
        [searchResults removeAllObjects];
        [installationStoreTableView reloadData];
    } else {
        [searchResults removeAllObjects];
        [installationStoreTableView reloadData];
        for (PIMDStoreObject *store in storeArray)
        {
            NSRange nameRange = [store.storename rangeOfString:sender.text options:NSCaseInsensitiveSearch];
            NSRange descriptionRange = [store.storeAddress rangeOfString:sender.text options:NSCaseInsensitiveSearch];
            if(nameRange.location != NSNotFound || descriptionRange.location != NSNotFound)
            {
                [searchResults addObject:store];
                [installationStoreTableView reloadData];
            }
        }
        DebugLog(@"storeArray - > %d, resultArray - > %d",[storeArray count],[searchResults count]);
        viewSearchResults.hidden = NO;
        [installationStoreTableView setFrame:CGRectMake(installationStoreTableView.frame.origin.x,CGRectGetMaxY(viewSearchResults.frame)+2, installationStoreTableView.frame.size.width, self.view.frame.size.height - (CGRectGetMaxY(viewSearchResults.frame)+2))];
    }
}

- (IBAction)searchBtnPressed:(UIButton *)sender {
    if (sender.selected) {
        [searchTextField resignFirstResponder];
        searchTextField.text = @"Find stores";
        [searchBtn setBackgroundImage:[UIImage imageNamed:@"search_icon.png"] forState:UIControlStateNormal];
        searchBtn.selected = NO;
        searchView.backgroundColor = DEFAULT_COLOR;
        viewSearchResults.hidden = YES;
        [installationStoreTableView setFrame:CGRectMake(installationStoreTableView.frame.origin.x,CGRectGetMaxY(searchView.frame)+2, installationStoreTableView.frame.size.width, self.view.frame.size.height - (CGRectGetMaxY(searchView.frame)+2))];
        [searchResults removeAllObjects];
        [installationStoreTableView reloadData];
    }else
    {
        [searchTextField becomeFirstResponder];
        searchTextField.text = @"";
        [searchBtn setBackgroundImage:[UIImage imageNamed:@"close_search_icon.png"] forState:UIControlStateNormal];
        searchBtn.selected = YES;
        searchView.backgroundColor = DEFAULT_SELECTED_COLOR;
    }
}


- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"name contains[c] %@", searchText];
    searchResults = [NSMutableArray arrayWithArray:[storeArray filteredArrayUsingPredicate:resultPredicate]];
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}

#pragma mark PULL TO REFRESH
- (void)setupStrings{
    textPull = @"Pull down to refresh...";
    textRelease = @"Release to refresh...";
    textLoading = @"Loading please wait...";
}

- (void)addPullToRefreshHeader {
    refreshHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0 - REFRESH_HEADER_HEIGHT, 320, REFRESH_HEADER_HEIGHT)];
    refreshHeaderView.backgroundColor = [UIColor clearColor];
    
    refreshLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, REFRESH_HEADER_HEIGHT)];
    refreshLabel.backgroundColor = [UIColor clearColor];
    refreshLabel.font = DEFAULT_BOLD_FONT(15);
    refreshLabel.textAlignment = NSTextAlignmentCenter;
    refreshLabel.textColor = DEFAULT_COLOR;
    refreshArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:PULLTOREFRESH_BARROW]];
    refreshArrow.frame = CGRectMake(floorf((REFRESH_HEADER_HEIGHT - 2) / 2),
                                    (floorf(REFRESH_HEADER_HEIGHT - 44) / 2),
                                    27, 44);
    
    refreshSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    refreshSpinner.frame = CGRectMake(floorf(floorf(REFRESH_HEADER_HEIGHT - 2) / 2), floorf((REFRESH_HEADER_HEIGHT - 20) / 2), 20, 20);
    refreshSpinner.hidesWhenStopped = YES;
    
    [refreshHeaderView addSubview:refreshLabel];
    [refreshHeaderView addSubview:refreshArrow];
    [refreshHeaderView addSubview:refreshSpinner];
    [installationStoreTableView addSubview:refreshHeaderView];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if (isLoading) return;
    if (searchBtn.selected)
    {
        refreshArrow.hidden = TRUE;
        refreshLabel.hidden = TRUE;
        return;
    }
    refreshArrow.hidden = FALSE;
    refreshLabel.hidden = FALSE;
    isDragging = YES;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if ([scrollView isEqual:installationStoreTableView]) {
        [self.view endEditing:YES];
    }
    if (searchBtn.selected)
    {
        refreshArrow.hidden = TRUE;
        refreshLabel.hidden = TRUE;
        return;
    }
    //refreshArrow.hidden = FALSE;
    refreshLabel.hidden = FALSE;
    if (isLoading) {
        // Update the content inset, good for section headers
        if (scrollView.contentOffset.y > 0)
            installationStoreTableView.contentInset = UIEdgeInsetsZero;
        else if (scrollView.contentOffset.y >= -REFRESH_HEADER_HEIGHT)
            installationStoreTableView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    } else if (isDragging && scrollView.contentOffset.y < 0) {
        // Update the arrow direction and label
        [UIView animateWithDuration:0.25 animations:^{
            if (scrollView.contentOffset.y < -REFRESH_HEADER_HEIGHT) {
                // User is scrolling above the header
                refreshLabel.text = self.textRelease;
                [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
            } else {
                // User is scrolling somewhere within the header
                refreshLabel.text = self.textPull;
                [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
            }
        }];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (isLoading) return;
    if (searchBtn.selected)
    {
        refreshArrow.hidden = TRUE;
        refreshLabel.hidden = TRUE;
        return;
    }
    //refreshArrow.hidden = FALSE;
    refreshLabel.hidden = FALSE;
    isDragging = NO;
    if (scrollView.contentOffset.y <= -REFRESH_HEADER_HEIGHT) {
        // Released above the header
        [self startLoading];
    }
}

- (void)startLoading {
    isLoading = YES;
    // Show the header
    [UIView animateWithDuration:0.3 animations:^{
        installationStoreTableView.contentInset = UIEdgeInsetsMake(REFRESH_HEADER_HEIGHT, 0, 0, 0);
        refreshLabel.text = self.textLoading;
        refreshArrow.hidden = YES;
        [refreshSpinner startAnimating];
    }];
    if ([PIMD_APP_DELEGATE networkavailable]) {
        //[storeArray removeAllObjects];
        [self sendInstallationStoreListRequest];
    } else {
        [self stopLoading];
    }
    // Refresh action!
    //[self refresh];
}

- (void)stopLoading {
    isLoading = NO;
    // Hide the header
    [UIView animateWithDuration:0.3 animations:^{
        installationStoreTableView.contentInset = UIEdgeInsetsZero;
        [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
    }
                     completion:^(BOOL finished) {
                         [self performSelector:@selector(stopLoadingComplete)];
                     }];
}

- (void)stopLoadingComplete {
    // Reset the header
    refreshLabel.text = self.textPull;
    refreshArrow.hidden = NO;
    [refreshSpinner stopAnimating];
}


@end
