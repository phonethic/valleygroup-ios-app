//
//  PIMDTransitObject.h
//  ValleyGroup
//
//  Created by Rishi on 25/04/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PIMDTransitObject : NSObject
@property (nonatomic, copy) NSString *storeid;
@property (nonatomic, copy) NSString *storename;
@property (nonatomic, copy) NSString *videourl;
@property (nonatomic, copy) NSString *trackingurl;
@property (nonatomic, copy) NSString *latitude;
@property (nonatomic, copy) NSString *longitude;
@property (nonatomic, copy) NSString *location;
@property (nonatomic, copy) NSString *status;
@end
