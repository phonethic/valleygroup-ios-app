//
//  PIMDInStatusViewController.h
//  ValleyGroup
//
//  Created by Rishi on 14/05/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PIMDInStatusViewController : UIViewController
{
    
}
@property (strong, nonatomic) IBOutlet UIImageView *ringView;
@property (nonatomic, copy) NSString *passedstoreid;
@property (nonatomic, copy) NSString *passedstorestatus;
@property (nonatomic, copy) NSString *passedstoreAdd;
@property (nonatomic, copy) NSString *passedstoreName;

@property (strong, nonatomic) IBOutlet UIImageView *statusImageView;
@property (strong, nonatomic) IBOutlet UILabel *lblstoreName;
@property (strong, nonatomic) IBOutlet UILabel *lblstoreAdd;
@property (strong, nonatomic) IBOutlet UIImageView *ringImageView;
@property (strong, nonatomic) IBOutlet UIButton *statusBtn;

- (IBAction)statusBtnPressed:(id)sender;
@end
