//
//  PIMDLoginViewController.m
//  ValleyGroup
//
//  Created by Rishi on 26/03/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import "PIMDLoginViewController.h"
#import "SMPageControl.h"
#import <QuartzCore/QuartzCore.h>
#import "PIMDAppDelegate.h"
#import "PIMDStoreObject.h"
#import "AFNetworking.h"
#import "CommonCallback.h"
#import "PIMDStoreListViewController.h"
#import "PIMDWebViewController.h"


#define NUMBER @"1234567890"
#define PASSWORD @"valleygroup"

#define LOGIN_CUSTOMER [NSString stringWithFormat:@"%@%@customer_api/login_customer",URL_PREFIX,API_VERSION]
#define MOBILE_NUMBER(CODE,NUMBER)  [NSString stringWithFormat:@"%@%@",[CODE stringByReplacingOccurrencesOfString:@"+" withString:@""],NUMBER]

@interface PIMDLoginViewController ()
@end

@implementation PIMDLoginViewController
@synthesize loginView;
@synthesize nameTextField;
@synthesize designationTextField;
@synthesize passwordTextField;
@synthesize customerloginScrollView;
@synthesize nameVIew,passView;
@synthesize loginBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [switchbutton setImage:[UIImage imageNamed:@"switch_1.png"] forState:UIControlStateNormal];
    if([PIMDGlobalOperations isCustomerLoggedIn])
    {
        PIMDStoreListViewController *storelistController = [[PIMDStoreListViewController alloc] initWithNibName:@"PIMDStoreListViewController" bundle:nil] ;
        storelistController.title = @"Store List";
        [self.navigationController pushViewController:storelistController animated:YES];
    } else {
        [nameTextField setText:[PIMDGlobalOperations getCustomerId]];
        [passwordTextField setText:@""];
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    DebugLog(@"%f %f",customerloginScrollView.contentSize.height,customerloginScrollView.contentOffset.y);
    if ([[UIScreen mainScreen] bounds].size.height < 568)
    {
        if([textField isEqual:nameTextField])
        {
            [customerloginScrollView setContentOffset:CGPointMake(0,nameVIew.frame.origin.y+30) animated:YES];
        }
        else if(textField == passwordTextField) {
            [customerloginScrollView setContentOffset:CGPointMake(0,passView.frame.origin.y+40) animated:YES];
        }
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == nameTextField) {
        [passwordTextField becomeFirstResponder];
	}
//    else if (textField == designationTextField) {
//        [passwordTextField becomeFirstResponder];
//	}
    else {
        [passwordTextField resignFirstResponder];
        [self scrollTobottom];
    }
   	return YES;
}

-(void)scrollTobottom
{
    DebugLog(@"scrollview **** - > %f %f",customerloginScrollView.contentSize.height,customerloginScrollView.contentOffset.y);
//    if(customerloginScrollView.contentSize.height > customerloginScrollView.frame.size.height)  //if scrollview content height is greater than scrollview total height then remove the added height from scrollview
//    {
//        [customerloginScrollView setContentSize: CGSizeMake(customerloginScrollView.frame.size.width, customerloginScrollView.frame.size.height - 100)];
//    }
    if ([[UIScreen mainScreen] bounds].size.height < 568)
    {
        CGPoint bottomOffset = CGPointMake(0, 0);
        [customerloginScrollView setContentOffset:bottomOffset animated:YES];
        DebugLog(@"scrollview ### - > %f %f",customerloginScrollView.contentSize.height,customerloginScrollView.frame.size.height);
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if ([self respondsToSelector:NSSelectorFromString(@"edgesForExtendedLayout")]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    switchbutton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [switchbutton setImage:[UIImage imageNamed:@"switch_1.png"] forState:UIControlStateNormal];
    [switchbutton addTarget:self action:@selector(changeViewController) forControlEvents:UIControlEventTouchUpInside];
    [switchbutton setFrame:CGRectMake(0, 5, 40, 24)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:switchbutton];
    
    UITapGestureRecognizer *viewTap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapDetected:)];
    viewTap.numberOfTapsRequired = 1;
    viewTap.cancelsTouchesInView = NO;
    [self.view  addGestureRecognizer:viewTap];
    
    loginBtn.titleLabel.font    = DEFAULT_BOLD_FONT(20);
    nameTextField.font          = DEFAULT_FONT(20);
    passwordTextField.font      = DEFAULT_FONT(20);
    
}

- (void)tapDetected:(UIGestureRecognizer *)sender {
    [self.view endEditing:YES];
    [self scrollTobottom];
}

-(void)changeViewController {
    [switchbutton setImage:[UIImage imageNamed:@"switch_2.png"] forState:UIControlStateNormal];
    [self performSelector:@selector(switchController) withObject:nil afterDelay:0.2];
}

-(void)switchController
{
    [PIMD_APP_DELEGATE switchViewController:FRONTLINE_TEAM];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)loginBtnPressed:(id)sender {
    if ([PIMD_APP_DELEGATE networkavailable]) {
        if([nameTextField.text isEqualToString:@""] || [passwordTextField.text isEqualToString:@""])
        {
            [PIMDGlobalOperations showAlert:@"Please fill all the fields."];
        }
        else if([nameTextField.text length] < 10)
        {
            [PIMDGlobalOperations showAlert:@"Mobile number must be at least 10 characters in length."];
        }
        else if([passwordTextField.text length] < 6)
        {
            [PIMDGlobalOperations showAlert:@"Password field must be at least 6 characters in length."];
        } else {
            [PIMDAnalyticsLogger logEvent:@"Customer_Login_Btn_Pressed"];
             [self sendCustomerLoginRequest];
        }
    } else {
        [PIMDGlobalOperations showOfflineAlert];
    }
}

- (IBAction)phonethicsBtnPressed:(id)sender {
    [PIMDAnalyticsLogger logEvent:@"Phonethics_Btn_Pressed"];
    PIMDWebViewController *webviewController = [[PIMDWebViewController alloc] initWithNibName:@"PIMDWebViewController" bundle:nil] ;
    webviewController.title = @"Phonethics";
    [self.navigationController pushViewController:webviewController animated:YES];
}

-(void)sendCustomerLoginRequest
{
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:API_KEY];
    [httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:nameTextField.text forKey:@"mobile"];
    [params setObject:passwordTextField.text forKey:@"password"];
    [httpClient postPath:LOGIN_CUSTOMER parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"json ->%@",json);
        if(json != nil) {
            
            if([[json  objectForKey:@"success"] isEqualToString:@"true"])
            {
                [PIMDAnalyticsLogger logEvent:@"Customer_Login_Success"];
                NSDictionary* data = [json objectForKey:@"data"];
                NSString * resultid = [data objectForKey:@"id"];
                NSString * resultcode = [data objectForKey:@"auth_code"];
                DebugLog(@"%@ %@",resultid,resultcode);
                [PIMDGlobalOperations setCustomerDetails:[nameTextField text] pass:[passwordTextField text]];
                [PIMDGlobalOperations setCustomerAuthDetails:resultid code:resultcode];
                PIMDStoreListViewController *storelistController = [[PIMDStoreListViewController alloc] initWithNibName:@"PIMDStoreListViewController" bundle:nil] ;
                storelistController.title = @"Store List";
                [self.navigationController pushViewController:storelistController animated:YES];
            } else {
                DebugLog(@"Login Failed");
                [PIMDAnalyticsLogger logEvent:@"Customer_Login_Failed"];
                NSString *message = [json objectForKey:@"message"];
                if([message length] > 0)
                {
                    [PIMDGlobalOperations showAlert:message];
                }
            }
            
        }  else {
            [PIMDGlobalOperations showAlert:@"No response from server. Please try again later."];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        [CommonCallback hideProgressHud];
    }];
}

@end
