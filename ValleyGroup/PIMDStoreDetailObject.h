//
//  PIMDStoreDetailObject.h
//  ValleyGroup
//
//  Created by Rishi on 30/04/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PIMDStoreDetailObject : NSObject
@property (nonatomic, copy) NSString *storeid;
@property (nonatomic, copy) NSString *storename;
@property (nonatomic, copy) NSString *storestatus;
@property (nonatomic, copy) NSString *storetelNo1;
@property (nonatomic, copy) NSString *storetelNo2;
@property (nonatomic, copy) NSString *storecity;
@property (nonatomic, copy) NSString *storeEmail1;
@property (nonatomic, copy) NSString *storeEmail2;
@property (nonatomic, copy) NSString *projectname;
@property (nonatomic, copy) NSString *projectdescription;
@property (nonatomic, copy) NSString *projectstartDate;
@property (nonatomic, copy) NSString *projectendDate;
@end
