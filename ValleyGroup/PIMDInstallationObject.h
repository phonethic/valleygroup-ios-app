//
//  PIMDInstallationObject.h
//  ValleyGroup
//
//  Created by Rishi on 08/05/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PIMDInstallationObject : NSObject

@property (nonatomic, copy) NSString *storeid;
@property (nonatomic, copy) NSString *categoryid;
@property (nonatomic, copy) NSString *category;

@end
