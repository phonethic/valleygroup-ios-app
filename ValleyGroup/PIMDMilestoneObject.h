//
//  PIMDMilestoneObject.h
//  ValleyGroup
//
//  Created by Rishi on 25/04/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PIMDMilestoneObject : NSObject
@property (nonatomic, copy) NSString *storeid;
@property (nonatomic, copy) NSString *storemilestone;
@end
