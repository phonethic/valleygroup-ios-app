//
//  PIMDImageObject.h
//  ValleyGroup
//
//  Created by Rishi on 28/04/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PIMDImageObject : NSObject
@property (nonatomic, copy) NSString *store_id;
@property (nonatomic, copy) NSString *store_name;
@property (nonatomic, copy) NSString *thumb_url;
@property (nonatomic, copy) NSString *image_url;
@end
