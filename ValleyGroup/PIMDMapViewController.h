//
//  PIMDMapViewController.h
//  ValleyGroup
//
//  Created by Rishi on 26/03/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LocationPickerView.h"
#import <MapKit/MapKit.h>

@interface PIMDMapViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, CLLocationManagerDelegate, LocationPickerViewDelegate>
{
    CLLocationManager *locationManager;
}

@property (nonatomic, strong) LocationPickerView *locationPickerView;

@end
