//
//  PIMDInstallationFormDetailViewController.h
//  ValleyGroup
//
//  Created by Rishi on 09/05/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "ImageFilterViewController.h"
#import "PIMDSignatureViewController.h"



@interface PIMDInstallationFormDetailViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate,CropImageFilterViewControllerDelegate,PIMDSignatureViewControllerDelegate>
{
    int imgPickerTag;
    int animstart;
}
@property (nonatomic) BOOL newMedia;
@property (strong, nonatomic) NSMutableArray *questionsArray;
@property (nonatomic, copy) NSString *passedstoreid;
@property (nonatomic, copy) NSString *passedcategoryid;
@property (nonatomic, copy) NSString *aprrovalid;
@property (nonatomic, copy) NSString *inspectionImageAdded;
@property (nonatomic, copy) NSString *approvalImageAdded;
@property (readwrite, nonatomic) NSInteger currentObjID;


@property (strong, nonatomic) IBOutlet UILabel *approvallbl;
@property (strong, nonatomic) IBOutlet UILabel *damagelbl;

@property (strong, nonatomic) IBOutlet UIView *multiView;
@property (strong, nonatomic) IBOutlet UILabel *multilbl;
@property (strong, nonatomic) IBOutlet UIButton *multiBtn1;
@property (strong, nonatomic) IBOutlet UIButton *multiBtn2;
@property (strong, nonatomic) IBOutlet UIButton *multiBtn3;
@property (strong, nonatomic) IBOutlet UIButton *multiBtn4;
@property (strong, nonatomic) IBOutlet UIButton *multiBtn5;
@property (strong, nonatomic) IBOutlet UIButton *prevBtn;
@property (strong, nonatomic) IBOutlet UIButton *nextBtn;
@property (strong, nonatomic) IBOutlet UITextView *commentsTextView;
@property (strong, nonatomic) IBOutlet UIImageView *formImageView;
@property (strong, nonatomic) IBOutlet UIImageView *signatureImageView;
@property (strong, nonatomic) IBOutlet UIButton *submitBtn;
@property (strong, nonatomic) IBOutlet UIView *submitView;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttonsCollection;

- (IBAction)multiBtn1Pressed:(UIButton *)sender;
- (IBAction)multiBtn2Pressed:(UIButton *)sender;
- (IBAction)multiBtn3Pressed:(UIButton *)sender;
- (IBAction)multiBtn4Pressed:(UIButton *)sender;
- (IBAction)multiBtn5Pressed:(UIButton *)sender;
- (IBAction)nextBtnPressed:(id)sender;
- (IBAction)previousBtnPressed:(id)sender;
- (IBAction)doneBtnPressed:(id)sender;

@end
