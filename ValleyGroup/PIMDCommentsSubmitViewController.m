//
//  PIMDCommentsSubmitViewController.m
//  ValleyGroup
//
//  Created by Rishi on 07/05/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import "PIMDCommentsSubmitViewController.h"
#import "constants.h"
#import "CommonCallback.h"
#import "PIMDAppDelegate.h"
#import "PIMDGlobalOperations.h"
#import "PIMDModuleSelectionViewController.h"

#define ADD_GALLERY [NSString stringWithFormat:@"%@%@store_api/recce_design",URL_PREFIX,API_VERSION]

@interface PIMDCommentsSubmitViewController ()

@end

@implementation PIMDCommentsSubmitViewController
@synthesize passedstoreid;
@synthesize passeddesignid;
@synthesize recceId;
@synthesize commentsTextView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@"Please enter any other comments on recce."]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor]; //optional
    }
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@""] || [[textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0) {
        textView.text = @"Please enter any other comments on recce.";
        textView.textColor = [UIColor lightGrayColor]; //optional
    }
    [textView resignFirstResponder];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if ([self respondsToSelector:NSSelectorFromString(@"edgesForExtendedLayout")]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    UITapGestureRecognizer *viewTap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapDetected:)];
    viewTap.numberOfTapsRequired = 1;
    viewTap.cancelsTouchesInView = NO;
    [self.view  addGestureRecognizer:viewTap];
    
    commentsTextView.font = DEFAULT_FONT(18);
    commentsTextView.textColor = [UIColor lightGrayColor];
    
}

- (void)tapDetected:(UIGestureRecognizer *)sender {
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)submitBtnPressed:(id)sender {
    if ([PIMD_APP_DELEGATE networkavailable]) {
        [PIMDAnalyticsLogger logEvent:@"Recce_Submit_Btn_Pressed"];
        [self sendCommentsRequest];
    } else {
        [PIMDGlobalOperations showOfflineAlert];
    }
    
}

-(void)sendCommentsRequest
{
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:API_KEY];
    [httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    NSMutableDictionary *postparams = [[NSMutableDictionary alloc] init];
    /////////////////////////////////////////////////////////////////
    [postparams setObject:passedstoreid forKey:@"store_id"];
    [postparams setObject:passeddesignid forKey:@"design_id"];
    if((recceId != nil) && ![recceId isEqualToString:@""])
    {
        [postparams setObject:recceId forKey:@"recce_id"];
    }
     if ([commentsTextView.text isEqualToString:@""] || [[commentsTextView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0) {
         [postparams setObject:@"" forKey:@"comments"];
     } else {
         [postparams setObject:commentsTextView.text forKey:@"comments"];
     }
     [postparams setObject:@"1" forKey:@"submit"];
    [postparams setObject:[PIMDGlobalOperations getFrontlineMemberId] forKey:@"member_id"];
    [postparams setObject:[PIMDGlobalOperations getFrontlineMemberAuthId] forKey:@"auth_id"];

    /////////////////////////////////////////////////////////////////
    DebugLog(@"----%@------",postparams);
    /////////////////////////////////////////////////////////////////
    [httpClient postPath:ADD_GALLERY parameters:postparams success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"json afnetworking ->%@",json);
        if(json != nil && [[json  objectForKey:@"success"] isEqualToString:@"true"])
        {
            [PIMDAnalyticsLogger logEvent:@"Recce_Submit_Success"];
            [PIMDGlobalOperations showAlert:[json  objectForKey:@"message"]];
            NSDictionary* data = [json objectForKey:@"data"];
            DebugLog(@"%@",[data objectForKey:@"recce_id"]);
            [self popBackToModuleSelectionViewController];
        } else if(json != nil && [[json  objectForKey:@"success"] isEqualToString:@"false"]) {
            [PIMDAnalyticsLogger logEvent:@"Recce_Submit_Failed"];
            if([json  objectForKey:@"code"] != nil &&  [[json  objectForKey:@"code"] isEqualToString:FRONTLINE_INVALID_USER_AUTH_CODE])
            {
                if([PIMDGlobalOperations isFrontlineMemberLoggedIn]){
                    [PIMDGlobalOperations showAlert:FRONTLINE_INVALID_USER_AUTH_MESSAGE];
                    [PIMDGlobalOperations clearFrontlineMemberDetails];
                    [self performSelector:@selector(popToLoginController) withObject:nil afterDelay:1.0];
                }
            } else {
                NSString *message = [json objectForKey:@"message"];
                if([message length] > 0)
                {
                    [PIMDGlobalOperations showAlert:message];
                }
            }
        }
         else {
             [PIMDGlobalOperations showAlert:[json  objectForKey:@"message"]];
         }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        [CommonCallback hideProgressHud];
    }];
    
}

-(void) popToLoginController
{
    DebugLog(@"popToRootViewControllerAnimated:YES");
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void) popBackToModuleSelectionViewController
{
    
    for (UIViewController *controller in self.navigationController.viewControllers) {
        if ([controller isKindOfClass:[PIMDModuleSelectionViewController class]]) {
            [self.navigationController popToViewController:controller  animated:YES];
            break;
        }
    }
    
}

@end
