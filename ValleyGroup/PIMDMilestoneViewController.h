//
//  PIMDMilestoneViewController.h
//  ValleyGroup
//
//  Created by Rishi on 22/04/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PIMDMilestoneViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITableView *milestoneTableView;
@property (strong, nonatomic) NSMutableArray *milestoneArray;
@property (nonatomic, copy) NSString *passedstoreid;

@end
