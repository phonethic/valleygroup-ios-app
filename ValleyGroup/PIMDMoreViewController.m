//
//  PIMDMoreViewController.m
//  ValleyGroup
//
//  Created by Rishi on 22/04/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import "PIMDMoreViewController.h"
#import "constants.h"
#import "PIMDMilestoneViewController.h"
#import "PIMDAppDelegate.h"
#import "CommonCallback.h"
#import "PIMDGlobalOperations.h"
#import "PIMDStoreDetailObject.h"

#define STORE_DETAILS(ID) [NSString stringWithFormat:@"%@%@%@store_api/store_details?store_id=%@",LIVE_SERVER,URL_PREFIX,API_VERSION,ID]

@interface PIMDMoreViewController ()
@property(strong) NSDictionary *splashJson;
@end

@implementation PIMDMoreViewController
@synthesize passedstoreid;
@synthesize detailArray;
@synthesize feedbackBtn;
@synthesize callstoreBtn;
@synthesize milestonesBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if ([self respondsToSelector:NSSelectorFromString(@"edgesForExtendedLayout")]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
     feedbackBtn.titleLabel.font    = DEFAULT_BOLD_FONT(20);
     callstoreBtn.titleLabel.font   = DEFAULT_BOLD_FONT(20);
     milestonesBtn.titleLabel.font  = DEFAULT_BOLD_FONT(20);
    
     detailArray = [[NSMutableArray alloc] init];
    if ([PIMD_APP_DELEGATE networkavailable]) {
        //[self sendStoreTransitRequest];
        [self sendStoreDetailRequest];
    } else {
        [PIMDGlobalOperations showOfflineAlert];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)sendStoreDetailRequest
{
     [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:STORE_DETAILS(passedstoreid)]];
    DebugLog(@"%@",STORE_DETAILS(passedstoreid));
    [urlRequest setValue:API_KEY forHTTPHeaderField:@"X-API-KEY"];
    [urlRequest setValue:[PIMDGlobalOperations getCustomerAuthId] forHTTPHeaderField:@"customer_id"];
    [urlRequest setValue:[PIMDGlobalOperations getCustomerAuthCode] forHTTPHeaderField:@"auth_id"];
    DebugLog(@"%@",[PIMDGlobalOperations getCustomerAuthId]);
    DebugLog(@"%@",[PIMDGlobalOperations getCustomerAuthCode]);
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: urlRequest
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        [CommonCallback hideProgressHud];
                                                        DebugLog(@"%@",self.splashJson);
                                                        if(self.splashJson  != nil)
                                                        {
                                                            [detailArray removeAllObjects];
                                                            if([[self.splashJson  objectForKey:@"success"] isEqualToString:@"true"])
                                                            {
                                                                NSDictionary *dict = [self.splashJson  objectForKey:@"data"];
                                                                PIMDStoreDetailObject *storedetail = [[PIMDStoreDetailObject alloc] init];
                                                                storedetail.storeid = [dict objectForKey:@"store_id"];
                                                                storedetail.storename = [dict objectForKey:@"store_name"];
                                                                storedetail.storecity = [dict objectForKey:@"city_name"];
                                                                storedetail.storestatus = [dict objectForKey:@"store_status"];
                                                                storedetail.storetelNo1 = [dict objectForKey:@"tel_no1"];
                                                                storedetail.storetelNo2 = [dict objectForKey:@"tel_no2"];
                                                                storedetail.storeEmail1 = [dict objectForKey:@"email1"];
                                                                storedetail.storeEmail2 = [dict objectForKey:@"email2"];
                                                                storedetail.projectname = [dict objectForKey:@"project_name"];
                                                                storedetail.projectdescription = [dict objectForKey:@"project_description"];
                                                                storedetail.projectstartDate = [dict objectForKey:@"project_start_date"];
                                                                storedetail.projectendDate = [dict objectForKey:@"project_end_date"];
                                                                [detailArray addObject:storedetail];
                                                                
                                                                DebugLog(@"%@",((PIMDStoreDetailObject *)[detailArray objectAtIndex:0]).storecity);
                                                            } else {
                                                                DebugLog(@"Login Failed");
                                                                NSString *message = [self.splashJson objectForKey:@"message"];
                                                                if([message length] > 0)
                                                                {
                                                                    [PIMDGlobalOperations showAlert:message];
                                                                }
                                                            }
                                                            
                                                        }  else {
                                                            [PIMDGlobalOperations showAlert:@"No response from server. Please try again later."];
                                                        }
                                                        //[storeListTableView reloadData];
                                                        
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        //[hud hide:YES];
                                                        [CommonCallback hideProgressHud];
                                                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                                                     message:[NSString stringWithFormat:@"%@",[error localizedDescription]]
                                                                                                    delegate:nil
                                                                                           cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                        [av show];
                                                        
                                                    }];
    
    
    
    [operation start];
}

- (IBAction)feedbackBtnPressed:(id)sender {
    if ([MFMailComposeViewController canSendMail]) {
        if([detailArray count] > 0)
        {
            PIMDStoreDetailObject *storedetail = [detailArray objectAtIndex:0];
            
            if([storedetail.storename length] > 0 && [storedetail.storeEmail1 length] > 0)
            {
                NSDictionary *mailstoreParams = [NSDictionary dictionaryWithObjectsAndKeys:storedetail.storename,@"StoreName", storedetail.storeEmail1,@"StoreEmail", nil];
                DebugLog(@"%@",mailstoreParams);
                [PIMDAnalyticsLogger logEvent:@"Customer_Feedback_Btn_Pressed" withParams:mailstoreParams];
            }
            
            MFMailComposeViewController *emailComposer = [[MFMailComposeViewController alloc] init];
            emailComposer.mailComposeDelegate = self;
            if ([storedetail.storeEmail2 length] > 0) {
                [emailComposer setToRecipients:[NSArray arrayWithObjects:storedetail.storeEmail1,storedetail.storeEmail2, nil]];
            } else {
                if ([storedetail.storeEmail1 length] <= 0) {
                    storedetail.storeEmail1 = @"support@thevalleygroup1.zendesk.com";
                }
                [emailComposer setToRecipients:[NSArray arrayWithObjects:storedetail.storeEmail1, nil]];
            }
            [emailComposer setSubject:storedetail.storename];
            [emailComposer setMessageBody:@"" isHTML:NO];
            emailComposer.toolbar.tag = 1;
            [self presentViewController:emailComposer animated:YES completion:nil];
        }
    }else
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:ALERT_TITLE
                                  message:@"You may not have set up mail service on your device or You may not connected to internet.\nPlease check and try again."
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}

- (IBAction)callstoreBtnPressed:(id)sender {
    NSString *deviceType = [UIDevice currentDevice].model;
    if([deviceType isEqualToString:@"iPhone"])
    {
        if([detailArray count] > 0)
        {
            UIAlertView *callStoreAlert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"Do you want to call store number ?" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
            callStoreAlert.tag = 1;
            [callStoreAlert show];
        } else {
            UIAlertView *alert = [ [ UIAlertView alloc ] initWithTitle: ALERT_TITLE
                                                               message: [NSString stringWithFormat:@"No store number to call.Please check your internet connection and try again."]
                                                              delegate: nil
                                                     cancelButtonTitle: nil
                                                     otherButtonTitles: @"OK", nil
                                  ];
            [ alert show ];
        }
    }else{
        UIAlertView *alert = [ [ UIAlertView alloc ] initWithTitle: ALERT_TITLE
                                                           message: [NSString stringWithFormat:@"This device cannot make a call."]
                                                          delegate: nil
                                                 cancelButtonTitle: nil
                                                 otherButtonTitles: @"OK", nil
                              ];
        [ alert show ];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if (alertView.tag == 1){
        if([title isEqualToString:@"Yes"])
        {
            [self callStoreNumber];
        }
    }
}

- (void)callStoreNumber
{
    if([detailArray count] > 0)
    {
        PIMDStoreDetailObject *storedetail = [detailArray objectAtIndex:0];
        
        if([storedetail.storename length] > 0 && [storedetail.storetelNo1 length] > 0)
        {
            NSDictionary *callstoreParams = [NSDictionary dictionaryWithObjectsAndKeys:storedetail.storename,@"StoreName", storedetail.storetelNo1,@"StoreNumber", nil];
            DebugLog(@"%@",callstoreParams);
            [PIMDAnalyticsLogger logEvent:@"Customer_CallStore_Btn_Pressed" withParams:callstoreParams];
        }
        
        
        UIApplication *myApp = [UIApplication sharedApplication];
        [myApp openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",storedetail.storetelNo1]]];
    }
}

- (IBAction)milestonesBtnPressed:(id)sender {
    if([detailArray count] > 0)
    {
        PIMDStoreDetailObject *storedetail = [detailArray objectAtIndex:0];
        
        NSDictionary *milestonestoreParams = [NSDictionary dictionaryWithObjectsAndKeys:storedetail.storename,@"StoreName", nil];
        DebugLog(@"%@",milestonestoreParams);
        [PIMDAnalyticsLogger logEvent:@"Customer_Installation_Details_Btn_Pressed" withParams:milestonestoreParams];
    }
    PIMDMilestoneViewController *storelistController = [[PIMDMilestoneViewController alloc] initWithNibName:@"PIMDMilestoneViewController" bundle:nil] ;
    storelistController.title = @"Installation Details";
    storelistController.passedstoreid = passedstoreid;
    DebugLog(@"%@",passedstoreid);
    [self.navigationController pushViewController:storelistController animated:YES];
}

#pragma mark Mail Compose Delegate
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    if (result == MFMailComposeResultFailed) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                        message:NSLocalizedString(@"Email failed to send. Please try again.", nil)
                                                       delegate:nil cancelButtonTitle:NSLocalizedString(@"Dismiss", nil) otherButtonTitles:nil];
		[alert show];
    }else if (result == MFMailComposeResultSent)
    {

    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
