//
//  PIMDTransitViewController.m
//  ValleyGroup
//
//  Created by Rishi on 22/04/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import "PIMDTransitViewController.h"
#import "PIMDMoreViewController.h"
#import "PIMDTransitObject.h"
#import "constants.h"
#import "CommonCallback.h"
#import "PIMDAppDelegate.h"
#import <AddressBook/AddressBook.h>

#define GET_STORE_STATUS(ID) [NSString stringWithFormat:@"%@%@%@store_api/store_status?store_id=%@",LIVE_SERVER,URL_PREFIX,API_VERSION,ID]

//#define GET_DEVICE_STATUS @"http://track.adititracking.com/track/basic_tracking_data.php?show_stopped=true&show_idling=true&show_moving=true&show_inactive=true&show_unreachable=true&user_id=5243&order_by=0&order_type=1&show_alert=true&user_name=valleyindia&hash_key=CJTMRPHTZCOIFMNL"

//#define GET_DEVICE_STATUS @"http://track.adititracking.com/track/basic_tracking_data.php?show_stopped=true&show_idling=true&show_moving=true&show_inactive=true&show_unreachable=true&user_id=5243&order_by=0&order_type=1&show_alert=true&user_name=valley&hash_key=QSNQOEVSPLNFVHZA"

/*
<georadius>
<version>0.5</version>
<result>0</result>
<message></message>
<data>
<basic_tracking device_id='6742' last_update='26-04-2014 14:16:44' latitude='13.062883' longitude='77.448095' distance='0.75' location='NH 4, Bengaluru, Karnataka, India' speed='0.00' direction='N' ignition_status='0' device_serial='2192020581' registration_no='2192020581' driver_name='A' driver_mobile='0' sensor_icon='' digital_sensor_status='0' analog_data='' alert_status='1' alert_icons='alert_battery_disconnected.gif' alert_type_name='Battery Disconnected' device_status='4'  device_tag='' gps_odometer='0' location_id='115276' status='0' fuel_level='-50' reporting_interval='25' reporting_interval_stopped='600' alert_time='2014-04-26 05:58:47' alert_id='' last_update_date='26 Apr 2014' last_update_time='02:16:44' relay_status='0'/>
<current_date>28-04-2014 18:09:25</current_date>
<unreachable_count>1</unreachable_count>
<idling_count>0</idling_count>
<stopped_count>0</stopped_count>
<moving_count>0</moving_count>
<inactive_count>0</inactive_count>
<total_count>1</total_count>
<alert_count>1</alert_count>

</data>
</georadius>
*/

@interface PIMDTransitViewController ()
@property(strong) NSDictionary *splashJson;
@end

@implementation PIMDTransitViewController
@synthesize locationMapVIew;
@synthesize transitArray;
@synthesize passedstoreid;
@synthesize storeNamelbl;
@synthesize storeAddlbl;
@synthesize moreBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if ([self respondsToSelector:NSSelectorFromString(@"edgesForExtendedLayout")]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    moreBtn.titleLabel.font    = DEFAULT_BOLD_FONT(20);
    storeNamelbl.font = DEFAULT_FONT(16);
    //storeNamelbl.backgroundColor = BROWN_OFFWHITE_COLOR;
    
    transitArray = [[NSMutableArray alloc] init];
    
    if ([PIMD_APP_DELEGATE networkavailable]) {
        [self sendStoreTransitRequest];
    } else {
        [PIMDGlobalOperations showOfflineAlert];
    }
}

-(void)addLocationOnMap
{
    if([transitArray count] > 0)
    {
        PIMDTransitObject *statusObj = [transitArray objectAtIndex:0];
        
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([statusObj.latitude doubleValue],  [statusObj.longitude doubleValue]);
        MKCoordinateSpan span;
        span.latitudeDelta = 0.01;
        span.longitudeDelta = 0.01;
        MKCoordinateRegion region;
        region.center = coordinate;
        region.span = span;
        [locationMapVIew setRegion:region animated:YES];

        if ([[locationMapVIew annotations] count] > 0) {
            [locationMapVIew removeAnnotations:[locationMapVIew annotations]];
        }
        MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
        point.coordinate = coordinate;
        point.title = @"Shipment Location";
        // point.subtitle = @"I'm here!!!";
        [locationMapVIew addAnnotation:point];
        
        storeNamelbl.text = statusObj.location;
        
        //[self getLocationFromCoordinates];
    }
}

-(void)getLocationFromCoordinates
{
    if([transitArray count] > 0)
    {
        PIMDTransitObject *statusObj = [transitArray objectAtIndex:0];
        
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];

        CLLocation *newLocation = [[CLLocation alloc]initWithLatitude:[statusObj.latitude doubleValue]
                                                            longitude:[statusObj.longitude doubleValue]];

        [geocoder reverseGeocodeLocation:newLocation
                       completionHandler:^(NSArray *placemarks, NSError *error) {
                           
                           if (error) {
                               DebugLog(@"Geocode failed with error: %@", error);
                               return;
                           }
                           
                           if (placemarks && placemarks.count > 0)
                           {
                               DebugLog(@"%@",placemarks);
                               
                               CLPlacemark *placemark = placemarks[0];
                               
                               NSDictionary *addressDictionary =
                               placemark.addressDictionary;
                               
                               NSString *address = [addressDictionary
                                                    objectForKey:(NSString *)kABPersonAddressStreetKey];
                               NSString *city = [addressDictionary
                                                 objectForKey:(NSString *)kABPersonAddressCityKey];
                               NSString *state = [addressDictionary
                                                  objectForKey:(NSString *)kABPersonAddressStateKey];
                               NSString *zip = [addressDictionary
                                                objectForKey:(NSString *)kABPersonAddressZIPKey];
                               
                               storeNamelbl.text = [NSString stringWithFormat:@"%@, %@, %@",address,city, state];

                               DebugLog(@"%@ %@ %@ %@", address,city, state, zip);
                           }
                           
                       }];
    }
}

- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views {
    MKAnnotationView *aV;
    
    for (aV in views) {
        
        // Don't pin drop if annotation is user location
        if ([aV.annotation isKindOfClass:[MKUserLocation class]]) {
            continue;
        }
        
        // Check if current annotation is inside visible map rect, else go to next one
        MKMapPoint point =  MKMapPointForCoordinate(aV.annotation.coordinate);
        if (!MKMapRectContainsPoint(self.locationMapVIew.visibleMapRect, point)) {
            continue;
        }
        
        CGRect endFrame = aV.frame;
        
        // Move annotation out of view
        aV.frame = CGRectMake(aV.frame.origin.x, aV.frame.origin.y - self.view.frame.size.height, aV.frame.size.width, aV.frame.size.height);
        
        // Animate drop
        [UIView animateWithDuration:1.5 delay:0.04*[views indexOfObject:aV] options:UIViewAnimationOptionTransitionFlipFromTop animations:^{
            
            aV.frame = endFrame;
            
            // Animate squash
        }completion:^(BOOL finished){
            if (finished) {
                [UIView animateWithDuration:0.05 animations:^{
                    aV.transform = CGAffineTransformMakeScale(1.0, 0.8);
                    
                }completion:^(BOOL finished){
                    [UIView animateWithDuration:0.1 animations:^{
                        aV.transform = CGAffineTransformIdentity;
                    }];
                }];
            }
        }];
    }
}

-(MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation {
    if ([annotation isKindOfClass:[MKUserLocation class]]) {
        return nil;
    }
    MKPinAnnotationView *pinView = nil;
    if (annotation != locationMapVIew.userLocation) {
        static NSString *defaultID = @"mapIdentifier";
        pinView = (MKPinAnnotationView *)[locationMapVIew dequeueReusableAnnotationViewWithIdentifier:defaultID];
        if (pinView == nil) {
            pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:defaultID];
        }
        pinView.canShowCallout = YES;
        //  pinView.animatesDrop = YES;
        UIImage *pinImage = [UIImage imageNamed:@"PinGreen.png"];
        [pinView setImage:pinImage];
        pinView.annotation = annotation;
    }
	return pinView;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)moreBtnPressed:(id)sender {
    [PIMDAnalyticsLogger logEvent:@"Customer_More_Btn_Pressed"];
    PIMDMoreViewController *storelistController = [[PIMDMoreViewController alloc] initWithNibName:@"PIMDMoreViewController" bundle:nil] ;
    storelistController.title = @"Store Options";
    storelistController.passedstoreid = passedstoreid;
    [self.navigationController pushViewController:storelistController animated:YES];
}

-(void) popToLoginController
{
    DebugLog(@"popToRootViewControllerAnimated:YES");
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)sendStoreTransitRequest
{
   [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:GET_STORE_STATUS(passedstoreid)]];
    DebugLog(@"%@",GET_STORE_STATUS(passedstoreid));
    [urlRequest setValue:API_KEY forHTTPHeaderField:@"X-API-KEY"];
    [urlRequest setValue:[PIMDGlobalOperations getCustomerAuthId] forHTTPHeaderField:@"customer_id"];
    [urlRequest setValue:[PIMDGlobalOperations getCustomerAuthCode] forHTTPHeaderField:@"auth_id"];
    DebugLog(@"%@",[PIMDGlobalOperations getCustomerAuthId]);
    DebugLog(@"%@",[PIMDGlobalOperations getCustomerAuthCode]);
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: urlRequest
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        [CommonCallback hideProgressHud];
                                                        DebugLog(@"%@",self.splashJson);
                                                        if(self.splashJson  != nil)
                                                        {
                                                            [transitArray removeAllObjects];
                                                            if([[self.splashJson  objectForKey:@"success"] isEqualToString:@"true"])
                                                            {
                                                                NSArray *dataArray = [self.splashJson  objectForKey:@"data"];
                                                                for (int index = 0; index < [dataArray count]; index++) {
                                                                    NSDictionary *dict = [dataArray objectAtIndex:index];
                                                                    PIMDTransitObject *statusObj = [[PIMDTransitObject alloc] init];
                                                                    statusObj.storeid = [dict objectForKey:@"store_id"];
                                                                    statusObj.storename = [dict objectForKey:@"store_name"];
                                                                    statusObj.trackingurl = [dict objectForKey:@"tracking_url"];
                                                                    [transitArray addObject:statusObj];
                                                                }
                                                                if([transitArray count] > 0)
                                                                {
                                                                    [self startJsonRequest];
                                                                }
                                                            } else {
                                                                DebugLog(@"Login Failed");
                                                                if(self.splashJson != nil && [[self.splashJson  objectForKey:@"success"] isEqualToString:@"false"]) {
                                                                    if([self.splashJson  objectForKey:@"code"] != nil &&  [[self.splashJson  objectForKey:@"code"] isEqualToString:FRONTLINE_INVALID_USER_AUTH_CODE])
                                                                    {
                                                                        if([PIMDGlobalOperations isFrontlineMemberLoggedIn]){
                                                                            [PIMDGlobalOperations showAlert:FRONTLINE_INVALID_USER_AUTH_MESSAGE];
                                                                            [PIMDGlobalOperations clearFrontlineMemberDetails];
                                                                            [self performSelector:@selector(popToLoginController) withObject:nil afterDelay:1.0];
                                                                        }
                                                                    } else {
                                                                        NSString *message = [self.splashJson objectForKey:@"message"];
                                                                        if([message length] > 0)
                                                                        {
                                                                            [PIMDGlobalOperations showAlert:message];
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            
                                                        }  else {
                                                            [PIMDGlobalOperations showAlert:@"No response from server. Please try again later."];
                                                        }
                                                        
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        //[hud hide:YES];
                                                        [CommonCallback hideProgressHud];
                                                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                                                     message:[NSString stringWithFormat:@"%@",[error localizedDescription]]
                                                                                                    delegate:nil
                                                                                           cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                        [av show];
                                                        
                                                    }];
    
    
    
    [operation start];
}

-(void) startJsonRequest
{
    DebugLog(@"%@",((PIMDTransitObject *)[transitArray objectAtIndex:0]).trackingurl);
    
    PIMDTransitObject *obj = [transitArray objectAtIndex:0];
    
    if([obj.trackingurl length] > 0)
    {
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:obj.trackingurl] cachePolicy:YES timeoutInterval:30.0];
        conn=[[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
    }
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	
	if ([response isKindOfClass:[NSHTTPURLResponse class]])
	{
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        
		status = [httpResponse statusCode];
        //DebugLog(@"status = %d ",status);
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseData==nil)
	{
		responseData = [[NSMutableData alloc] initWithLength:0];
	}
	
	[responseData appendData:data];
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    //[self logData];
    
    NSString *xmlDataFromChannelSchemes;
    
	if(responseData)
	{
		NSString *result = [[NSString alloc] initWithData:responseData encoding:NSASCIIStringEncoding];
		DebugLog(@"\n result:%@\n\n", result);
		xmlDataFromChannelSchemes = [[NSString alloc] initWithString:result];
		NSData *xmlDataInNSData = [xmlDataFromChannelSchemes dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
		xmlParser = [[NSXMLParser alloc] initWithData:xmlDataInNSData];
		[xmlParser setDelegate:self];
		[xmlParser parse];
		responseData = nil;
	}
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    
    // [self.responseIndicator stopAnimating];
}

- (void) logData
{
    if(responseData != NULL)
	{
		NSString *result = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
		DebugLog(@"\n result:%@\n\n", result);
    }
}

#pragma mark xmlParser methods
/* Called when the parser runs into an open tag (<tag>) */
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName 	attributes:(NSDictionary *)attributeDict
{
	if([elementName isEqualToString:@"georadius"])
	{
        [transitArray removeAllObjects];
	} else if([elementName isEqualToString:@"basic_tracking"]) {
        PIMDTransitObject *statusObj = [[PIMDTransitObject alloc] init];
        //statusObj.storeid = [attributeDict objectForKey:@"store_id"];
        //statusObj.storename = [attributeDict objectForKey:@"store_name"];
        statusObj.latitude = [attributeDict objectForKey:@"latitude"];
        statusObj.longitude = [attributeDict objectForKey:@"longitude"];
        statusObj.location = [attributeDict objectForKey:@"location"];
        statusObj.status = [attributeDict objectForKey:@"device_status"];
        [transitArray addObject:statusObj];
    }
}

-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    DebugLog(@"Processing Value: %@", string);
    
}

/* Called when the parser runs into a close tag (</tag>). */
- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"georadius"]) {
        DebugLog(@"%d",[transitArray count]);
        
    } else if([elementName isEqualToString:@"basic_tracking"]) {
        [self addLocationOnMap];
    }
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
    DebugLog(@"Error: %@", [parseError localizedDescription]);
}

- (void)parser:(NSXMLParser *)parser validationErrorOccurred:(NSError *)validationError
{
    DebugLog(@"Error: %@", [validationError localizedDescription]);
}


@end
