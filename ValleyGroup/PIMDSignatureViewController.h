//
//  PIMDSignatureViewController.h
//  ValleyGroup
//
//  Created by Rishi on 04/06/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SignatureView;

@protocol PIMDSignatureViewControllerDelegate <NSObject>
@required
-(void)addImageToSignatureView:(UIImage *)image;
@end


@interface PIMDSignatureViewController : UIViewController

@property (weak, nonatomic) IBOutlet SignatureView *signatureView;
@property(nonatomic,assign) id<PIMDSignatureViewControllerDelegate> delegate;

- (IBAction)clearBtnPressed:(id)sender;
- (IBAction)doneBtnPressed:(id)sender;
@end
