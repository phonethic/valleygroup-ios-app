//
//  PIMDDesignObject.h
//  ValleyGroup
//
//  Created by Rishi on 05/05/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PIMDDesignObject : NSObject

@property (nonatomic, copy) NSString *designid;
@property (nonatomic, copy) NSString *designname;
@property (nonatomic, copy) NSString *storeid;
@property (nonatomic, copy) NSString *imageUrl;
@property (nonatomic, copy) NSString *thumbUrl;

@end
