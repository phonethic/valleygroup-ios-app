//
//  PIMDGlobalOperations.h
//  ValleyGroup
//
//  Created by Rishi on 21/04/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PIMDGlobalOperations : NSObject


+(void)showAlert:(NSString*) message;
+(void)showOfflineAlert;


+(BOOL)isCustomerLoggedIn;
+(void)setCustomerDetails:(NSString *)idvalue pass:(NSString *)passvalue;
+(void)setCustomerAuthDetails:(NSString *)idvalue code:(NSString *)codevalue;
+(NSString *)getCustomerId;
+(NSString *)getCustomerAuthId;
+(NSString *)getCustomerAuthCode;
+(void)clearCustomerDetails;



+(BOOL)isFrontlineMemberLoggedIn;
+(void)setFrontlineMemberDetails:(NSString *)idvalue pass:(NSString *)passvalue;
+(void)setFrontlineMemberAuthDetails:(NSString *)idvalue code:(NSString *)codevalue;
+(NSString *)getFrontlineMemberPhone;
+(NSString *)getFrontlineMemberId;
+(NSString *)getFrontlineMemberAuthId;
+(void)clearFrontlineMemberDetails;

@end
