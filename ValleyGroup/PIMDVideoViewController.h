//
//  PIMDVideoViewController.h
//  ValleyGroup
//
//  Created by Rishi on 22/04/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@interface PIMDVideoViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIWebView *videoWebView;
@property (nonatomic, copy) NSString *passedstoreid;
@property (strong, nonatomic) IBOutlet UIButton *moreBtn;
@property (strong, nonatomic) NSMutableArray *videoArray;

- (IBAction)moreBtnPressed:(id)sender;

@end
