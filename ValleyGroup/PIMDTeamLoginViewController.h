//
//  PIMDTeamLoginViewController.h
//  ValleyGroup
//
//  Created by Rishi on 15/04/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PIMDTeamLoginViewController : UIViewController {
    UIButton *switchbutton;
}

@property (strong, nonatomic) IBOutlet UIView *loginView;
@property (strong, nonatomic) IBOutlet UIButton *loginBtn;
@property (strong, nonatomic) IBOutlet UITextField *nameTextField;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;
@property (strong, nonatomic) IBOutlet UIView *nameView;
@property (strong, nonatomic) IBOutlet UIView *passView;
@property (strong, nonatomic) IBOutlet UIScrollView *frontlineScrollView;


- (IBAction)loginBtnPressed:(id)sender;
- (IBAction)phonethicsBtnPressed:(id)sender;

@end
