//
//  PIMDQuestionObject.h
//  ValleyGroup
//
//  Created by Rishi on 16/05/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PIMDQuestionObject : NSObject

@property (nonatomic, copy) NSString *storeid;
@property (nonatomic, copy) NSString *categoryid;
@property (nonatomic, copy) NSString *questionid;
@property (nonatomic, copy) NSString *question;
@property (nonatomic, copy) NSString *questiontype;
@property (nonatomic, copy) NSString *optiontype;
@property (nonatomic, strong) NSMutableArray *options;
@property (nonatomic, copy) NSString *optionsText;
@property (nonatomic, strong) NSMutableDictionary *optionsAnswers;
@end
