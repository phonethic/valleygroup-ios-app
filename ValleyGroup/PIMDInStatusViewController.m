//
//  PIMDInStatusViewController.m
//  ValleyGroup
//
//  Created by Rishi on 14/05/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import "PIMDInStatusViewController.h"
#import "PIMDTransitViewController.h"
#import "PIMDVideoViewController.h"
#import "PIMDImagesViewController.h"
#import "constants.h"

@interface PIMDInStatusViewController ()

@end

@implementation PIMDInStatusViewController
@synthesize ringView;
@synthesize passedstoreid;
@synthesize passedstorestatus;
@synthesize statusBtn;
@synthesize ringImageView,statusImageView;
@synthesize passedstoreName;
@synthesize lblstoreName;
@synthesize lblstoreAdd;
@synthesize passedstoreAdd;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if([self respondsToSelector:@selector(setEdgesForExtendedLayout:)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    if([passedstoreName length] > 0)
    {
        lblstoreName.text = passedstoreName;
    }
    if([passedstoreAdd length] > 0)
    {
        lblstoreAdd.text = passedstoreAdd;
    }
    lblstoreName.font = DEFAULT_FONT(20);
    lblstoreAdd.font = DEFAULT_FONT(12);
    statusBtn.titleLabel.font = DEFAULT_BOLD_FONT(20);
    statusBtn.titleLabel.textAlignment = NSTextAlignmentCenter;

    if([passedstorestatus isEqualToString:@"1"] || [passedstorestatus isEqualToString:@"2"])
    {
        if([passedstorestatus isEqualToString:@"1"])  //Installation in progress
        {
            statusImageView.image   = [UIImage imageNamed:@"status_in_progress.png"];
            ringImageView.hidden    = NO;
            [statusBtn setTitle:@"View It LIVE" forState:UIControlStateNormal];
            [statusBtn setTitle:@"View It LIVE" forState:UIControlStateHighlighted];
            [statusBtn setBackgroundImage:[UIImage imageNamed:@"red_button.png"] forState:UIControlStateNormal];
            [statusBtn setBackgroundImage:[UIImage imageNamed:@"gray_button.png"] forState:UIControlStateHighlighted];
        }
        else if ([passedstorestatus isEqualToString:@"2"]) //In transit
        {
            statusImageView.image   = [UIImage imageNamed:@"status_in_transit.png"];
            ringImageView.hidden    = NO;
            [statusBtn setTitle:@"View GPS Location" forState:UIControlStateNormal];
            [statusBtn setTitle:@"View GPS Location" forState:UIControlStateHighlighted];
            [statusBtn setBackgroundImage:[UIImage imageNamed:@"gps_icon_button_off.png"] forState:UIControlStateNormal];
            [statusBtn setBackgroundImage:[UIImage imageNamed:@"gps_icon_button_on.png"] forState:UIControlStateHighlighted];
        }
        [self rotateImage:ringView];
    }
    else {  //Installation completed
        statusImageView.image   = [UIImage imageNamed:@"status_completed.png"];
        ringImageView.hidden    = YES;
        [statusBtn setTitle:@"View Images" forState:UIControlStateNormal];
        [statusBtn setTitle:@"View Images" forState:UIControlStateHighlighted];
        [statusBtn setBackgroundImage:[UIImage imageNamed:@"red_button.png"] forState:UIControlStateNormal];
        [statusBtn setBackgroundImage:[UIImage imageNamed:@"gray_button.png"] forState:UIControlStateHighlighted];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)rotateImage:(UIImageView *)lview
{
    CABasicAnimation *fullRotation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    fullRotation.fromValue = [NSNumber numberWithFloat:0];
    fullRotation.toValue = [NSNumber numberWithFloat:((360*M_PI)/180)];
    fullRotation.duration = 1.00;
    fullRotation.repeatCount = HUGE_VALF;
    fullRotation.removedOnCompletion = YES;
    [lview.layer addAnimation:fullRotation forKey:@"spinAnimation"];
    [lview.layer setSpeed:0.5];
}


- (IBAction)statusBtnPressed:(id)sender {
    if([passedstorestatus isEqualToString:@"1"])  //Installation in progress
    {
        if([passedstoreAdd length] > 0 && [passedstoreName length] > 0)
        {
            NSDictionary *livestoreParams = [NSDictionary dictionaryWithObjectsAndKeys:passedstoreName,@"StoreName", passedstoreAdd,@"StoreAdd", nil];
            DebugLog(@"%@",livestoreParams);
            [PIMDAnalyticsLogger logEvent:@"Customer_View_Live_Btn_Pressed" withParams:livestoreParams];
        }
        PIMDVideoViewController *storelistController = [[PIMDVideoViewController alloc] initWithNibName:@"PIMDVideoViewController" bundle:nil] ;
        storelistController.title = @"Live Video";
        storelistController.passedstoreid = passedstoreid;
        [self.navigationController pushViewController:storelistController animated:YES];
    } else if ([passedstorestatus isEqualToString:@"2"]) //In transit
    {
        if([passedstoreAdd length] > 0 && [passedstoreName length] > 0)
        {
            NSDictionary *transitstoreParams = [NSDictionary dictionaryWithObjectsAndKeys:passedstoreName,@"StoreName", passedstoreAdd,@"StoreAdd", nil];
            DebugLog(@"%@",transitstoreParams);
            [PIMDAnalyticsLogger logEvent:@"Customer_GPS_Location_Btn_Pressed" withParams:transitstoreParams];
        }
        PIMDTransitViewController *storelistController = [[PIMDTransitViewController alloc] initWithNibName:@"PIMDTransitViewController" bundle:nil] ;
        storelistController.title = @"GPS Location";
        storelistController.passedstoreid = passedstoreid;
        [self.navigationController pushViewController:storelistController animated:YES];
    } else {  //Installation completed
        if([passedstoreAdd length] > 0 && [passedstoreName length] > 0)
        {
            NSDictionary *completestoreParams = [NSDictionary dictionaryWithObjectsAndKeys:passedstoreName,@"StoreName", passedstoreAdd,@"StoreAdd", nil];
            DebugLog(@"%@",completestoreParams);
            [PIMDAnalyticsLogger logEvent:@"Customer_View_Images_Btn_Pressed" withParams:completestoreParams];
        }
        PIMDImagesViewController *storelistController = [[PIMDImagesViewController alloc] initWithNibName:@"PIMDImagesViewController" bundle:nil] ;
        storelistController.title = @"Store Gallery";
        storelistController.passedstoreid = passedstoreid;
        [self.navigationController pushViewController:storelistController animated:YES];
    }
}
@end
