//
//  PIMDInstallationListViewController.m
//  ValleyGroup
//
//  Created by Rishi on 08/05/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import "PIMDInstallationListViewController.h"
#import "PIMDInstallationObject.h"
#import "CommonCallback.h"
#import "constants.h"
#import "PIMDAppDelegate.h"
#import "PIMDInstallationFormDetailViewController.h"


#define INSTALLATION_CATEGORY(ID) [NSString stringWithFormat:@"%@%@%@store_api/inspection_category?store_id=%@",LIVE_SERVER,URL_PREFIX,API_VERSION,ID]

@interface PIMDInstallationListViewController ()
@property(strong) NSDictionary *splashJson;
@end

@implementation PIMDInstallationListViewController
@synthesize installationArray;
@synthesize installationListView;
@synthesize passedstoreid;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    installationArray = [[NSMutableArray alloc] init];
    if ([PIMD_APP_DELEGATE networkavailable]) {
        [self sendInstallationCategoryRequest];
    } else {
        [PIMDGlobalOperations showOfflineAlert];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)sendInstallationCategoryRequest
{
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:INSTALLATION_CATEGORY(passedstoreid)]];
    DebugLog(@"%@",INSTALLATION_CATEGORY(passedstoreid));
    [urlRequest setValue:API_KEY forHTTPHeaderField:@"X-API-KEY"];
    [urlRequest setValue:[PIMDGlobalOperations getFrontlineMemberId] forHTTPHeaderField:@"member_id"];
    [urlRequest setValue:[PIMDGlobalOperations getFrontlineMemberAuthId] forHTTPHeaderField:@"auth_id"];
    DebugLog(@"%@",[PIMDGlobalOperations getFrontlineMemberId]);
    DebugLog(@"%@",[PIMDGlobalOperations getFrontlineMemberAuthId]);
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: urlRequest
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        [CommonCallback hideProgressHud];
                                                        DebugLog(@"%@",self.splashJson);
                                                        if(self.splashJson  != nil)
                                                        {
                                                            [installationArray removeAllObjects];
                                                            if([[self.splashJson  objectForKey:@"success"] isEqualToString:@"true"])
                                                            {
                                                                NSArray *dataArray = [self.splashJson  objectForKey:@"data"];
                                                                for (int index = 0; index < [dataArray count]; index++) {
                                                                    NSDictionary *dict = [dataArray objectAtIndex:index];
                                                                    PIMDInstallationObject *store = [[PIMDInstallationObject alloc] init];
                                                                    store.storeid = [dict objectForKey:@"store_id"];
                                                                    store.categoryid = [dict objectForKey:@"category_id"];
                                                                    store.category = [dict objectForKey:@"category"];
                                                                    [installationArray addObject:store];
                                                                }
                                                                
                                                                DebugLog(@"%@",((PIMDInstallationObject *)[installationArray objectAtIndex:0]).category);
                                                            } else {
                                                                DebugLog(@"Login Failed");
                                                                NSString *message = [self.splashJson objectForKey:@"message"];
                                                                if([message length] > 0)
                                                                {
                                                                    [PIMDGlobalOperations showAlert:message];
                                                                }
                                                            }
                                                            
                                                        }  else {
                                                            [PIMDGlobalOperations showAlert:@"No response from server. Please try again later."];
                                                        }
                                                        [installationListView reloadData];
                                                        
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        //[hud hide:YES];
                                                        [CommonCallback hideProgressHud];
                                                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                                                     message:[NSString stringWithFormat:@"%@",[error localizedDescription]]
                                                                                                    delegate:nil
                                                                                           cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                        [av show];
                                                        
                                                    }];
    
    
    
    [operation start];
}

#pragma mark - Table view data source
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [installationArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UILabel *lblTitle;
    UIView *backView;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.backgroundColor = [UIColor clearColor];
        
        
        backView = [[UIView alloc] initWithFrame:CGRectMake(0,2, 280, 66)];
        backView.tag = 101;
        backView.backgroundColor    =  [UIColor whiteColor];
        backView.clipsToBounds      = YES;
        [cell.contentView addSubview:backView];
            
        lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(10,15,260, 40.0)];
        lblTitle.text                   = @"";
        lblTitle.tag                    = 102;
        lblTitle.font                   = DEFAULT_FONT(20);
        lblTitle.textAlignment          = NSTextAlignmentLeft;
        lblTitle.textColor              = DEFAULT_COLOR;
        lblTitle.highlightedTextColor   = [UIColor whiteColor];
        lblTitle.backgroundColor        = [UIColor clearColor];
        lblTitle.numberOfLines          = 1;
        [backView addSubview:lblTitle];
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor     = DEFAULT_SELECTED_COLOR;
        cell.selectedBackgroundView     = bgColorView;

    }
    if ([indexPath row] < [installationArray count]) {
        PIMDInstallationObject *installationObj = nil;
        installationObj = [installationArray objectAtIndex:indexPath.row];
        
        
        backView        = (UIView *)[cell viewWithTag:101];
        lblTitle        = (UILabel *)[backView viewWithTag:102];
        
        lblTitle.text = installationObj.category;
    }
    
    
    return cell;
}


#pragma mark - Table view delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DebugLog(@"%@",[installationArray objectAtIndex:indexPath.row]);
    PIMDInstallationObject *installationObj = [installationArray objectAtIndex:indexPath.row];
    DebugLog(@"status-->%@",installationObj.category);
    
    PIMDInstallationFormDetailViewController *designController = [[PIMDInstallationFormDetailViewController alloc] initWithNibName:@"PIMDInstallationFormDetailViewController" bundle:nil] ;
    designController.title = @"Store Status";
    designController.passedstoreid = installationObj.storeid;
    designController.passedcategoryid = installationObj.categoryid;
    [self.navigationController pushViewController:designController animated:YES];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

@end
