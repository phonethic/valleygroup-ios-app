//
//  PIMDImagesViewController.m
//  ValleyGroup
//
//  Created by Rishi on 22/04/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import "PIMDImagesViewController.h"
#import "PIMDAppDelegate.h"
#import "constants.h"
#import "CommonCallback.h"
#import "PIMDGlobalOperations.h"
#import "PIMDImageObject.h"
#import "PIMDMoreViewController.h"


#define STORE_GALLERY(ID) [NSString stringWithFormat:@"%@%@%@store_api/store_status?store_id=%@",LIVE_SERVER,URL_PREFIX,API_VERSION,ID]
#define THUMBNAIL_LINK(IMG_LINK) [NSString stringWithFormat:@"%@%@%@",LIVE_SERVER,URL_PREFIX,IMG_LINK]

@interface PIMDImagesViewController ()
@property(strong) NSDictionary *splashJson;
@end

@implementation PIMDImagesViewController
@synthesize galleryArray;
@synthesize storeId;
@synthesize newMedia;
@synthesize gridView = _gridView;
@synthesize photos = _photos;
@synthesize editedImage;
@synthesize passedstoreid;
@synthesize moreBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if ([self respondsToSelector:NSSelectorFromString(@"edgesForExtendedLayout")]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    //Add gridView
    _gridView = [[VCGridView alloc] initWithFrame:CGRectMake(0, 10, self.view.frame.size.width, self.view.frame.size.height - 110)];
    _gridView.backgroundColor = [UIColor clearColor];
	_gridView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	_gridView.delegate = self;
	_gridView.dataSource = self;
	[self.view addSubview:_gridView];
    //self.navigationItem.rightBarButtonItem = self.editButtonItem;
    galleryArray = [[NSMutableArray alloc] init];
    if ([PIMD_APP_DELEGATE networkavailable]) {
        [self sendStoreGalleryRequest];
    } else {
        [PIMDGlobalOperations showOfflineAlert];
    }
    
    moreBtn.titleLabel.font    = DEFAULT_BOLD_FONT(20);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)sendStoreGalleryRequest
{
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:STORE_GALLERY(passedstoreid)]];
    DebugLog(@"%@",urlRequest);
    [urlRequest setValue:API_KEY forHTTPHeaderField:@"X-API-KEY"];
    [urlRequest setValue:[PIMDGlobalOperations getCustomerAuthId] forHTTPHeaderField:@"customer_id"];
    [urlRequest setValue:[PIMDGlobalOperations getCustomerAuthCode] forHTTPHeaderField:@"auth_id"];
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: urlRequest
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        [CommonCallback hideProgressHud];
                                                        DebugLog(@"%@", [self.splashJson description]);
                                                        if(self.splashJson  != nil) {
                                                            if([[self.splashJson  objectForKey:@"success"] isEqualToString:@"true"])
                                                            {
                                                                [galleryArray removeAllObjects];
                                                                NSArray* jsonArray = [self.splashJson  objectForKey:@"data"];
                                                                for (int index=0; index<[jsonArray count]; index++) {
                                                                    NSDictionary *objdict = [jsonArray  objectAtIndex:index];
                                                                    //DebugLog(@"\n\n%@\n\n",objdict);
                                                                    PIMDImageObject *tempgalleryObj = [[PIMDImageObject alloc] init];
                                                                    tempgalleryObj.store_id = [objdict objectForKey:@"store_id"];
                                                                    tempgalleryObj.store_name = [objdict objectForKey:@"store_name"];
                                                                    tempgalleryObj.image_url = [objdict objectForKey:@"image_url"];
                                                                    tempgalleryObj.thumb_url = [objdict objectForKey:@"thumb_url"];
                                                                    
                                                                    [galleryArray addObject:tempgalleryObj];
                                                                    tempgalleryObj = nil;
                                                                }
                                                            } else if(self.splashJson != nil && [[self.splashJson  objectForKey:@"success"] isEqualToString:@"false"]) {
                                                           /*     if([self.splashJson  objectForKey:@"code"] != nil &&  [[self.splashJson  objectForKey:@"code"] isEqualToString:MERCHANT_INVALID_USER_AUTH_CODE])
                                                                {
                                                                    if([INUserDefaultOperations isMerchantLoggedIn]){
                                                                        [INUserDefaultOperations showAlert:MERCHANT_INVALID_USER_AUTH_MESSAGE];
                                                                        [INUserDefaultOperations clearMerchantDetails];
                                                                        [self performSelector:@selector(popStoreGalleryController) withObject:nil afterDelay:2.0];
                                                                    }
                                                                } else {
                                                                    [PIMDGlobalOperations showAlert:[self.splashJson  objectForKey:@"message"]];
                                                                }  */
                                                            }
                                                            DebugLog(@"galleryArray %@",galleryArray);
                                                            [self.gridView reloadData];
                                                        }
                                                        
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        [CommonCallback hideProgressHud];
                                                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                                                     message:[NSString stringWithFormat:@"%@",[error localizedDescription]]
                                                                                                    delegate:nil
                                                                                           cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                        [av show];
                                                        
                                                    }];
    
    
    
    [operation start];
}

#pragma mark - VCGridViewDataSource

- (NSInteger)numberOfCellsInGridView:(VCGridView*)gridView
{
    return [galleryArray count];
}

- (VCGridViewCell *)gridView:(VCGridView *)gridView cellAtIndex:(NSInteger)index
{
    UIImageView *imageView;
    
	VCGridViewCell *cell = [gridView dequeueReusableCell];
	if (!cell) {
		cell = [[VCGridViewCell alloc] initWithFrame:CGRectZero];
		
		CGRect contentFrame = CGRectInset(cell.bounds, 0, 0);
        //        CGRect contentFrame = CGRectInset(CGRectMake(0, 0, 75, 75), 0, 0);
        
		imageView = [[UIImageView alloc] initWithFrame:contentFrame];
		imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		imageView.contentMode = UIViewContentModeScaleToFill;
        imageView.tag = 222;
		//imageView.image = [UIImage imageNamed:@"cell"];
        // imageView.layer.cornerRadius = 8.0;
		[cell.contentView addSubview:imageView];
        
		UIImageView *imageView1 = [[UIImageView alloc] initWithFrame:contentFrame];
		imageView1.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		imageView1.contentMode = UIViewContentModeBottomRight;
		imageView1.image = [UIImage imageNamed:@"check"];
		cell.editingSelectionOverlayView = imageView1;
        
        
   //     cell.contentView.layer.cornerRadius = 8.0;
        cell.contentView.clipsToBounds = YES;
        
        
		cell.highlightedBackgroundView.backgroundColor = BROWN_OFFWHITE_COLOR;
        cell.highlightedBackgroundView.layer.cornerRadius = 8.0;
	}

    imageView = (UIImageView *)[cell viewWithTag:222];
    
    if (index < [galleryArray count]) {
        imageView.backgroundColor = [UIColor clearColor];
        if ([[galleryArray objectAtIndex:index] isKindOfClass:[PIMDImageObject class]]) {
            PIMDImageObject *tempgalleryObj  = [galleryArray objectAtIndex:index];
            [imageView setImageWithURL:[NSURL URLWithString:THUMBNAIL_LINK(tempgalleryObj.thumb_url)] placeholderImage:[UIImage imageNamed:@"Icon-120.png"]];
            DebugLog(@"index %d : link-%@",index,THUMBNAIL_LINK(tempgalleryObj.thumb_url));
        }else
        {
            imageView.image = [UIImage imageNamed:@"photo.png"];
        }
    }else{
        DebugLog(@"index %d : empty image ",index);
        imageView.image = nil;
        imageView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.3];
    }
	return cell;
}

- (BOOL)gridView:(VCGridView *)gridView canEditCellAtIndex:(NSInteger)index
{
    return NO;
}

#pragma mark - VCGridViewDelegate

- (void)gridView:(VCGridView*)gridView didSelectCellAtIndex:(NSInteger)index
{
#if DEBUG
	DebugLog(@"Selected %i", index);
#endif
    [PIMDAnalyticsLogger logEvent:@"Customer_View_Gallery_Image_Pressed"];
    if (index < [galleryArray count]) {
        if ([[galleryArray objectAtIndex:index] isKindOfClass:[PIMDImageObject class]]) {
            DebugLog(@"push to Browser %i", index);
            _photos = [[NSMutableArray alloc] init];
            MWPhoto *photo;
            PIMDImageObject *tempgalleryObj;
            for(int i = 0; i < [galleryArray count]; i++)
            {
                tempgalleryObj = (PIMDImageObject *)[galleryArray objectAtIndex:i];
                DebugLog(@"title--- %@",tempgalleryObj.store_name);
                photo = [[MWPhoto alloc] initWithURL:[NSURL URLWithString:THUMBNAIL_LINK(tempgalleryObj.image_url)]];
                DebugLog(@"image--- %@",THUMBNAIL_LINK(tempgalleryObj.image_url));
                [_photos addObject:photo];
                tempgalleryObj = nil;
            }
            
            // Create browser
            MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
            // Set options
            browser.displayActionButton = NO; // Show action button to allow sharing, copying, etc (defaults to YES)
            browser.wantsFullScreenLayout = YES; // iOS 5 & 6 only: Decide if you want the photo browser full screen, i.e. whether the status bar is affected (defaults to YES)
            
            browser.displayNavArrows = YES; // Whether to display left and right nav arrows on toolbar (defaults to NO)
            browser.displaySelectionButtons = NO; // Whether selection buttons are shown on each image (defaults to NO)
            browser.zoomPhotosToFill = YES; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
            browser.alwaysShowControls = NO; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
            browser.enableGrid = NO; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
            browser.startOnGrid = NO; // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
            
            // Optionally set the current visible photo before displaying
            [browser setCurrentPhotoIndex:0];
            
            // Present
            [self.navigationController pushViewController:browser animated:YES];
        }
    }else{
        DebugLog(@"You clicked empty image. :)");
    }

}

- (CGSize)sizeForCellsInGridView:(VCGridView *)gridView
{
	return CGSizeMake(95.0f, 95.0f);
}

- (CGFloat)paddingForCellsInGridView:(VCGridView *)gridView
{
	return 5.0f;
}

#pragma MWPhotoBrowser delegate methods
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser
{
    return [_photos count];
}
- (id<MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index
{
    return [_photos objectAtIndex:index];
}

- (IBAction)moreBtnPressed:(id)sender {
    [PIMDAnalyticsLogger logEvent:@"Customer_More_Btn_Pressed"];
    PIMDMoreViewController *storelistController = [[PIMDMoreViewController alloc] initWithNibName:@"PIMDMoreViewController" bundle:nil] ;
    storelistController.title = @"Store Options";
    storelistController.passedstoreid = passedstoreid;
    [self.navigationController pushViewController:storelistController animated:YES];
}
@end
