//
//  ImageFilterViewController.m
//  shoplocal
//
//  Created by Kirti Nikam on 30/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "ImageFilterViewController.h"
#import "CommonCallback.h"
#import "constants.h"
#import "ImageFilterViewController.h"

@interface ImageFilterViewController ()

@end

@implementation ImageFilterViewController
@synthesize originalImage;
@synthesize imageView;
@synthesize delegate;
//@synthesize imageCropper;
@synthesize toolBarView;
@synthesize saveBtn,cancelBtn,clockWiseRotateBtn,antiClockWiseRotateBtn;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"Edit Photo";
    }
    return self;
}

#pragma view lifecycle
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    UIButton *storeButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1111];
    [storeButton setHidden:TRUE];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    UIButton *storeButton = (UIButton *)[self.navigationController.navigationBar viewWithTag:1111];
    [storeButton setHidden:FALSE];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.view setBackgroundColor:[UIColor darkGrayColor]];
    rotationInt = 0;
    
/*    NLImageCropperView code
//    _imageCropper.hidden    = FALSE;
//    _imageCropper = [[NLImageCropperView alloc] initWithFrame:self.imageView.frame];
//    [self.view addSubview:_imageCropper];
//    [_imageCropper setImage:originalImage];
//    
//    if (originalImage.size.width > originalImage.size.height) {
//        [_imageCropper setCropRegionRect:CGRectMake(10, 10, originalImage.size.height - 20, originalImage.size.height - 20)];
//    }else{
//        [_imageCropper setCropRegionRect:CGRectMake(10, 10, originalImage.size.width - 20, originalImage.size.width - 20)];
//    }
end of NLImageCropperView code*/

/*    BJImageCropper code
 imageView.hidden        = TRUE;

    self.imageCropper = [[BJImageCropper alloc] initWithImage:originalImage andMaxSize:CGSizeMake(400, 400)];
    [self.view addSubview:self.imageCropper];
    self.imageCropper.center = self.imageView.center;
    self.imageCropper.imageView.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.imageCropper.imageView.layer.shadowRadius = 3.0f;
    self.imageCropper.imageView.layer.shadowOpacity = 0.8f;
    self.imageCropper.imageView.layer.shadowOffset = CGSizeMake(1, 1);

BJImageCropper code */
    
    imageView.image = originalImage;
    [self.view bringSubviewToFront:toolBarView];

    [self setUI];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setImageView:nil];
    [self setCancelBtn:nil];
    [self setSaveBtn:nil];
    [self setAntiClockWiseRotateBtn:nil];
    [self setClockWiseRotateBtn:nil];
    [self setToolBarView:nil];
    [super viewDidUnload];
}

#pragma UIInterfaceOrientation Methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

-(void)setUI
{
    imageView.backgroundColor               = [UIColor clearColor];
    //saveBtn.titleLabel.font                 =  DEFAULT_FONT(17);
    //cancelBtn.titleLabel.font               =  DEFAULT_FONT(17);

    [saveBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [saveBtn setTitleColor:[UIColor colorWithWhite:1.0 alpha:0.5] forState:UIControlStateHighlighted];
    
    [cancelBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cancelBtn setTitleColor:[UIColor colorWithWhite:1.0 alpha:0.5] forState:UIControlStateHighlighted];
    
    //saveBtn.backgroundColor                 = DEFAULT_COLOR;
    cancelBtn.backgroundColor               = [UIColor grayColor];
    //antiClockWiseRotateBtn.backgroundColor  = DEFAULT_COLOR;
    //clockWiseRotateBtn.backgroundColor      = DEFAULT_COLOR;
}


- (IBAction)cancelBtnClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)saveBtnClicked:(id)sender {
/*    BJImageCropper code
    UIImage *croppedImage = [self.imageCropper getCroppedImage];
    self.imageView.image = [CommonCallback resizeImage:croppedImage newSize:CGSizeMake(160, 160)];
*/
    
    DebugLog(@"rotationInt %d",rotationInt);
    [self rotateImageView];
    if(self.delegate && [self.delegate respondsToSelector:@selector(saveImageToPhotoAlbum:)])
    {
        UIImage *rotatedImage = [CommonCallback fixOrientation:imageView.image];
        [self.delegate saveImageToPhotoAlbum:rotatedImage];
    }
    [self.navigationController popViewControllerAnimated:YES];
    self.delegate = nil;
}

- (IBAction)antiClockWiseRotateBtnClicked:(id)sender {
    [UIView animateWithDuration:0.15 animations:^{
//        imageCropper.transform = CGAffineTransformRotate(imageCropper.transform,-M_PI/2);
        imageView.transform = CGAffineTransformRotate(imageView.transform,-M_PI/2);
        rotationInt--;
        if(rotationInt==0)
            rotationInt=4;
    }];
}

- (IBAction)clockWiseRotateBtnClicked:(id)sender {
    [UIView animateWithDuration:0.15 animations:^{
//        imageCropper.transform = CGAffineTransformRotate(imageCropper.transform,M_PI/2);
        imageView.transform = CGAffineTransformRotate(imageView.transform,M_PI/2);
        rotationInt++;
        if(rotationInt==4)
            rotationInt=0;
    }];
}

-(void)rotateImageView{
	switch(rotationInt)
	{
		case 0:
			imageView.image=[UIImage imageWithCGImage:(CGImageRef)imageView.image.CGImage scale:imageView.image.scale orientation:UIImageOrientationUp];
			break;
		case 1:
			
			imageView.image=[UIImage imageWithCGImage:(CGImageRef)imageView.image.CGImage scale:imageView.image.scale orientation:UIImageOrientationRight];
			break;
		case 2:
			imageView.image=[UIImage imageWithCGImage:(CGImageRef)imageView.image.CGImage scale:imageView.image.scale orientation:UIImageOrientationDown];
			break;
		case 3:
			imageView.image=[UIImage imageWithCGImage:(CGImageRef)imageView.image.CGImage scale:imageView.image.scale orientation:UIImageOrientationLeft];
			break;
	}
}
@end
