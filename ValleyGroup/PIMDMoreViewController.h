//
//  PIMDMoreViewController.h
//  ValleyGroup
//
//  Created by Rishi on 22/04/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <MessageUI/MFMessageComposeViewController.h>

@interface PIMDMoreViewController : UIViewController <MFMailComposeViewControllerDelegate>

@property (nonatomic, copy) NSString *passedstoreid;
@property (strong, nonatomic) NSMutableArray *detailArray;
@property (strong, nonatomic) IBOutlet UIButton *feedbackBtn;
@property (strong, nonatomic) IBOutlet UIButton *callstoreBtn;
@property (strong, nonatomic) IBOutlet UIButton *milestonesBtn;

- (IBAction)feedbackBtnPressed:(id)sender;
- (IBAction)callstoreBtnPressed:(id)sender;
- (IBAction)milestonesBtnPressed:(id)sender;
@end
