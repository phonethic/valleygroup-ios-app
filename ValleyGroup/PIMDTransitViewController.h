//
//  PIMDTransitViewController.h
//  ValleyGroup
//
//  Created by Rishi on 22/04/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface PIMDTransitViewController : UIViewController <NSXMLParserDelegate> {
    NSMutableData *responseData;
    NSError  *connectionError;
    NSURLConnection *conn;    
    int status;
    NSXMLParser *xmlParser;
}

@property (strong, nonatomic) IBOutlet MKMapView *locationMapVIew;
@property (strong, nonatomic) NSMutableArray *transitArray;
@property (nonatomic, copy) NSString *passedstoreid;
@property (strong, nonatomic) IBOutlet UILabel *storeNamelbl;
@property (strong, nonatomic) IBOutlet UILabel *storeAddlbl;
@property (strong, nonatomic) IBOutlet UIButton *moreBtn;


- (IBAction)moreBtnPressed:(id)sender;

@end
