//
//  PIMDAppDelegate.m
//  ValleyGroup
//
//  Created by Rishi on 26/03/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//
#import <MessageUI/MessageUI.h>

#import "PIMDAppDelegate.h"
#import "PIMDLoginViewController.h"
#import "PIMDTeamLoginViewController.h"
#import "PIMDOptionViewController.h"
#import "CommonCallback.h"
#import "UAirship.h"
#import "UAConfig.h"
#import "UAPush.h"

@implementation PIMDAppDelegate
@synthesize networkavailable;
@synthesize clientloginviewController;
@synthesize teamloginviewController;
@synthesize optionviewController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    
    [self setUpNavigationBarAppeance];
    
    //Add Reachability
    [self startCheckNetwork];
    
    //Add Flurry Crash Reporting
    [Flurry setCrashReportingEnabled:YES];
    
    //Add Flurry
    //[Flurry setDebugLogEnabled:FALSE];
    [Flurry startSession:FLURRY_APPID];
    
    //Add UrbanAirship
    UAConfig *config = [UAConfig defaultConfig];
    // Call takeOff (which creates the UAirship singleton)
    [UAirship takeOff:config];
    [UAirship setLogging:FALSE];
    [UAirship setLogLevel:UALogLevelNone];
    [UAPush shared].notificationTypes = (UIRemoteNotificationTypeBadge |
                                         UIRemoteNotificationTypeSound |
                                         UIRemoteNotificationTypeAlert);
    
    teamloginviewController = [[PIMDTeamLoginViewController alloc] initWithNibName:@"PIMDTeamLoginViewController" bundle:nil];
    teamloginviewController.title = @"Frontline Login";
    teamnavigationController = [[UINavigationController alloc] initWithRootViewController:teamloginviewController];
  //  teamnavigationController.navigationBar.translucent = NO;
    
    clientloginviewController = [[PIMDLoginViewController alloc] initWithNibName:@"PIMDLoginViewController" bundle:nil];
    clientloginviewController.title = @"Customer Login";
    clientnavigationController = [[UINavigationController alloc] initWithRootViewController:clientloginviewController];
 //   clientnavigationController.navigationBar.translucent = YES;
    
    optionviewController = [[PIMDOptionViewController alloc] initWithNibName:@"PIMDOptionViewController" bundle:nil];
    optionviewController.title = @"Welcome";
    //self.window.rootViewController = optionviewController;
    
    UINavigationController *controller = [[UINavigationController alloc] initWithRootViewController:optionviewController];
    controller.navigationBar.translucent = NO;
    self.window.rootViewController = controller;
    

    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    return YES;
}

-(void)setUpNavigationBarAppeance{
    [[UINavigationBar appearance] setBackgroundImage:[CommonCallback getFlatImage:DEFAULT_COLOR] forBarMetrics:UIBarMetricsDefault];
    
    NSDictionary *navbarTitleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                               [UIColor whiteColor],UITextAttributeTextColor,
                                               //                                               [UIColor blackColor], UITextAttributeTextShadowColor,
                                               //                                               [NSValue valueWithUIOffset:UIOffsetMake(-1, 0)], UITextAttributeTextShadowOffset,
                                               DEFAULT_BOLD_FONT(22),UITextAttributeFont,
                                               nil];
    
    [[UINavigationBar appearance] setTitleTextAttributes:navbarTitleTextAttributes];
    
    // Disable the custom font when the NavigationBar is presented in a MFMailComposeViewController
    [[UINavigationBar appearanceWhenContainedIn:[MFMailComposeViewController class], nil] setTitleTextAttributes:
     @{UITextAttributeTextColor : [UIColor whiteColor],
       UITextAttributeFont : [UIFont systemFontOfSize:18.0f]}];
    
    
    if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackOpaque];
        [[UINavigationBar appearance] setTintColor:DEFAULT_COLOR];
    }else{
        //self.window.tintColor = DEFAULT_COLOR;
        [[UINavigationBar appearance] setBarStyle:UIBarStyleBlackOpaque];
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        
        [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
        [[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];
        
        //[[UINavigationBar appearance] setBarTintColor:DEFAULT_COLOR];
        
        //set back button color
        [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
    }
}

#pragma Reachability Methods
-(void) startCheckNetwork
{
    
	[[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(reachabilityChanged:) name: kReachabilityChangedNotification object: nil];
	
    internetReach = [Reachability reachabilityForInternetConnection];
	[internetReach startNotifer];
    [self updateReachabitlityFlag:internetReach];
    
}

-(void) stopCheckNetwork
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
	internetReach = nil;
}

- (void)reachabilityChanged:(NSNotification *)note
{
	Reachability* curReach = [note object];
	NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
	[self updateReachabilityStatus: curReach];
}

- (void) updateReachabilityStatus: (Reachability*) curReach
{
	if(curReach == internetReach)
	{
		[self updateReachabitlityFlag: curReach];
    }
}

- (void) updateReachabitlityFlag: (Reachability*) curReach
{
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    BOOL connectionRequired=[curReach connectionRequired];
    
    switch (netStatus)
    {
        case NotReachable:
        {
            networkavailable =  NO;
            break;
        }
            
        case ReachableViaWWAN:
        {
			if(connectionRequired==NO)
			{
                networkavailable = YES;
			}
			else
			{
				//this is for invoking internet library
				NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:@"maps.google.com"] cachePolicy:NO timeoutInterval:15.0] ;
				
				NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self];
				if (theConnection) {
					connectionRequired	= NO;
				}
			}
			break;
        }
        case ReachableViaWiFi:
        {
            if(connectionRequired==NO)
            {
                networkavailable = YES;
            }
            break;
		}
    }
    if(connectionRequired)
    {
		networkavailable = NO;
    }
    DebugLog(@"network status = %d",networkavailable);
}

-(void)pushViewController:(int)type
{
    
    [UIView  transitionWithView:self.window duration:0.8  options:UIViewAnimationOptionTransitionCurlUp
                     animations:^(void) {
                         BOOL oldState = [UIView areAnimationsEnabled];
                         [UIView setAnimationsEnabled:NO];
                         if(type == FRONTLINE_TEAM)
                         {
                             self.window.rootViewController = teamnavigationController;
                             self.window.backgroundColor = [UIColor whiteColor];
                             [self.window makeKeyAndVisible];
                         } else {
                             self.window.rootViewController = clientnavigationController;
                             self.window.backgroundColor = [UIColor whiteColor];
                             [self.window makeKeyAndVisible];
                         }
                         [UIView setAnimationsEnabled:oldState];
                     }
                     completion:nil];
}

-(void)switchViewController:(int)type {

    [UIView  transitionWithView:self.window duration:0.8  options:UIViewAnimationOptionTransitionFlipFromLeft
                     animations:^(void) {
                         BOOL oldState = [UIView areAnimationsEnabled];
                         [UIView setAnimationsEnabled:NO];
                         if([self.window.rootViewController isEqual:clientnavigationController])
                         {
                             self.window.rootViewController = teamnavigationController;
                              self.window.backgroundColor = [UIColor whiteColor];
                             [self.window makeKeyAndVisible];
                         } else {
                             self.window.rootViewController = clientnavigationController;
                              self.window.backgroundColor = [UIColor whiteColor];
                             [self.window makeKeyAndVisible];
                         }
                         [UIView setAnimationsEnabled:oldState];
                     }
                     completion:nil];
}

- (NSString*)base64forData:(NSData*)theData {
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    [PIMDAnalyticsLogger localyticsSessionWillResignActive];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
      [PIMDAnalyticsLogger localyticsSessionDidEnterBackground];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     [PIMDAnalyticsLogger localyticsSessionWillEnterForeground];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [PIMDAnalyticsLogger localyticsSessionDidBecomeActive:LOCALYTICS_APPID];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
     [PIMDAnalyticsLogger localyticsSessionWillTerminate];
}

@end
