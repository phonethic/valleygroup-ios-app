//
//  PIMDMilestoneViewController.m
//  ValleyGroup
//
//  Created by Rishi on 22/04/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import "PIMDMilestoneViewController.h"
#import "constants.h"
#import "CommonCallback.h"
#import "PIMDAppDelegate.h"
#import "PIMDMilestoneObject.h"

#define GET_STORE_MILESTONES(ID) [NSString stringWithFormat:@"%@%@%@store_api/store_milestones?store_id=%@",LIVE_SERVER,URL_PREFIX,API_VERSION,ID]

@interface PIMDMilestoneViewController ()
@property(strong) NSDictionary *splashJson;
@end

@implementation PIMDMilestoneViewController
@synthesize milestoneTableView;
@synthesize milestoneArray;
@synthesize passedstoreid;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if ([self respondsToSelector:NSSelectorFromString(@"edgesForExtendedLayout")]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
//    milestoneTableView.layer.cornerRadius = 5.0f;
//    milestoneTableView.layer.borderColor = [UIColor lightGrayColor].CGColor;
//    milestoneTableView.layer.borderWidth = 0.5f;
    
    milestoneArray = [[NSMutableArray alloc] init];
    
//    [milestoneArray addObject:@"Milestone 1"];
//    [milestoneArray addObject:@"Milestone 2"];
//    [milestoneArray addObject:@"Milestone 3"];
    
    if ([PIMD_APP_DELEGATE networkavailable]) {
        [self sendStoreTransitRequest];
    } else {
        [PIMDGlobalOperations showOfflineAlert];
    }
}

#pragma mark - Table view data source
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return [milestoneArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UILabel *lblTitle;
    UIView *backView;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        backView = [[UIView alloc] initWithFrame:CGRectMake(0,2, 280, 66)];
        backView.tag = 101;
        backView.backgroundColor    =  [UIColor whiteColor];
        backView.clipsToBounds      = YES;
        [cell.contentView addSubview:backView];
        
        UIImageView *thumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(10, 23, 15, 20)];
        thumbImg.tag = 102;
        thumbImg.contentMode = UIViewContentModeScaleToFill;
        thumbImg.image   = [UIImage imageNamed:@"milestone.png"];
        [backView addSubview:thumbImg];
        
        lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(thumbImg.frame)+10,0,250, 70.0)];
        lblTitle.text                   = @"";
        lblTitle.tag                    = 103;
        lblTitle.font                   = DEFAULT_FONT(18);
        lblTitle.textAlignment          = NSTextAlignmentLeft;
        lblTitle.textColor              = DEFAULT_SELECTED_COLOR;
        lblTitle.highlightedTextColor   = [UIColor whiteColor];
        lblTitle.backgroundColor        = [UIColor clearColor];
        lblTitle.numberOfLines          = 1;
        [backView addSubview:lblTitle];
        
//        UIView *bgColorView = [[UIView alloc] init];
//        bgColorView.backgroundColor     = DEFAULT_SELECTED_COLOR;
//        cell.selectedBackgroundView     = bgColorView;

    }
    PIMDMilestoneObject *milestoneObj = [milestoneArray objectAtIndex:indexPath.row];
//    cell.textLabel.text = milestoneObj.storemilestone;
//    cell.textLabel.font = DEFAULT_FONT(18);
    
    backView        = (UILabel *)[cell viewWithTag:101];
    lblTitle        = (UILabel *)[backView viewWithTag:103];
    [lblTitle setText:milestoneObj.storemilestone];
    return cell;
}


#pragma mark - Table view delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //NSLog(@"%@",[milestoneArray objectAtIndex:indexPath.row]);
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)sendStoreTransitRequest
{
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:GET_STORE_MILESTONES(passedstoreid)]];
    DebugLog(@"%@",GET_STORE_MILESTONES(passedstoreid));
    [urlRequest setValue:API_KEY forHTTPHeaderField:@"X-API-KEY"];
    [urlRequest setValue:[PIMDGlobalOperations getCustomerAuthId] forHTTPHeaderField:@"customer_id"];
    [urlRequest setValue:[PIMDGlobalOperations getCustomerAuthCode] forHTTPHeaderField:@"auth_id"];
    DebugLog(@"%@",[PIMDGlobalOperations getCustomerAuthId]);
    DebugLog(@"%@",[PIMDGlobalOperations getCustomerAuthCode]);
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest: urlRequest
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        self.splashJson  = (NSDictionary *)JSON;
                                                        [CommonCallback hideProgressHud];
                                                        DebugLog(@"%@",self.splashJson);
                                                        if(self.splashJson  != nil)
                                                        {
                                                            [milestoneArray removeAllObjects];
                                                            if([[self.splashJson  objectForKey:@"success"] isEqualToString:@"true"])
                                                            {
                                                                NSArray *dataArray = [self.splashJson  objectForKey:@"data"];
                                                                for (int index = 0; index < [dataArray count]; index++) {
                                                                    NSDictionary *dict = [dataArray objectAtIndex:index];
                                                                    PIMDMilestoneObject *milestoneObj = [[PIMDMilestoneObject alloc] init];
                                                                    milestoneObj.storeid = [dict objectForKey:@"store_id"];
                                                                    milestoneObj.storemilestone = [dict objectForKey:@"milestone"];
                                                                    [milestoneArray addObject:milestoneObj];
                                                                }
                                                                
                                                                DebugLog(@"%@",((PIMDMilestoneObject *)[milestoneArray objectAtIndex:0]).storemilestone);
                                                                
                                                                [milestoneTableView reloadData];
                                                            } else {
                                                                DebugLog(@"Login Failed");
                                                                NSString *message = [self.splashJson objectForKey:@"message"];
                                                                if([message length] > 0)
                                                                {
                                                                    [PIMDGlobalOperations showAlert:message];
                                                                }
                                                            }
                                                            
                                                        }  else {
                                                            [PIMDGlobalOperations showAlert:@"No response from server. Please try again later."];
                                                        }
                                                        
                                                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        //[hud hide:YES];
                                                        [CommonCallback hideProgressHud];
                                                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                                                     message:[NSString stringWithFormat:@"%@",[error localizedDescription]]
                                                                                                    delegate:nil
                                                                                           cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                        [av show];
                                                        
                                                    }];
    
    
    
    [operation start];
}

@end
