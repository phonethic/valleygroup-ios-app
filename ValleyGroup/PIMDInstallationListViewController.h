//
//  PIMDInstallationListViewController.h
//  ValleyGroup
//
//  Created by Rishi on 08/05/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PIMDInstallationListViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITableView *installationListView;
@property (strong, nonatomic) NSMutableArray *installationArray;
@property (nonatomic, copy) NSString *passedstoreid;

@end
