//
//  PIMDUploadImagesViewController.m
//  ValleyGroup
//
//  Created by Rishi on 06/05/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import "PIMDUploadImagesViewController.h"
#import "constants.h"
#import "CommonCallback.h"
#import "PIMDAppDelegate.h"
#import "PIMDGlobalOperations.h"
#import "PIMDCommentsSubmitViewController.h"

#define ADD_GALLERY [NSString stringWithFormat:@"%@%@store_api/recce_design",URL_PREFIX,API_VERSION]

@interface PIMDUploadImagesViewController ()

@end

@implementation PIMDUploadImagesViewController
@synthesize uploadImageTableView;
@synthesize uploadArray;
@synthesize passedstoreid;
@synthesize passeddesignid;
@synthesize newMedia;
@synthesize recceId;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if([self respondsToSelector:@selector(setEdgesForExtendedLayout:)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        
    }
    uploadArray = [[NSMutableArray alloc] init];
    selectedRow = 0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addNewImage
{
    UIAlertView *addImageToGalleryAlert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"Choose your option" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Choose Existing", @"Take Picture", nil];
    addImageToGalleryAlert.tag = 3;
    [addImageToGalleryAlert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if (alertView.tag == 3){
        if ([PIMD_APP_DELEGATE networkavailable]) {
            if([title isEqualToString:@"Choose Existing"])
            {
                [self useCameraRoll:alertView.tag];
            } else if([title isEqualToString:@"Take Picture"])
            {
                [self useCamera:alertView.tag];
            } else {
                selectedRow = 0;
            }
        } else {
            [PIMDGlobalOperations showOfflineAlert];
        }
    }
}


#pragma mark - Image Picker
- (void)useCamera:(int)itag
{
    if ([UIImagePickerController isSourceTypeAvailable:
         UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate        = self;
        imagePicker.sourceType      =    UIImagePickerControllerSourceTypeCamera;
        imagePicker.mediaTypes      =    [NSArray arrayWithObjects:(NSString *) kUTTypeImage,nil];
        imagePicker.allowsEditing   =   YES;
        imgPickerTag = itag;
        [self presentViewController:imagePicker animated:YES completion:nil];
        newMedia = YES;
    }
}

- (void)useCameraRoll:(int)itag
{
    if ([UIImagePickerController isSourceTypeAvailable:
         UIImagePickerControllerSourceTypeSavedPhotosAlbum])
    {
        UIImagePickerController *imagePicker =  [[UIImagePickerController alloc] init];
        imagePicker.delegate        = self;
        imagePicker.sourceType      =    UIImagePickerControllerSourceTypePhotoLibrary;
        imagePicker.mediaTypes      =   [NSArray arrayWithObjects:(NSString *) kUTTypeImage, nil];
        imagePicker.allowsEditing   = YES;
        imgPickerTag = itag;
        [self presentViewController:imagePicker animated:YES completion:nil];
        newMedia = NO;
    }
}


-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    [self dismissViewControllerAnimated:YES completion:nil];
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
        //        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
        
        UIImage *originalImage = [CommonCallback fixOrientation:image];
        ImageFilterViewController *filterViewController = [[ImageFilterViewController alloc] initWithNibName:@"ImageFilterViewController" bundle:nil];
        filterViewController.delegate = self;
        filterViewController.originalImage = originalImage;
        
        [UIView  transitionWithView:self.navigationController.view duration:0.8  options:UIViewAnimationOptionTransitionFlipFromLeft
                         animations:^(void) {
                             BOOL oldState = [UIView areAnimationsEnabled];
                             [UIView setAnimationsEnabled:NO];
                             [self.navigationController pushViewController:filterViewController animated:YES];
                             [UIView setAnimationsEnabled:oldState];
                         }
                         completion:nil];
    }
    else if ([mediaType isEqualToString:(NSString *)kUTTypeMovie])
    {
		// Code here to support video if enabled
	}
}

-(void)image:(UIImage *)image finishedSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if (error) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"Save failed"
                              message: @"Failed to save image"
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma CropImageFilterViewControllerDelegate method
-(void)saveImageToPhotoAlbum:(UIImage *)editedImage
{
    DebugLog(@"newPost : saveToalbum %@",editedImage);
//    if (imgPickerTag == 2) {
//        iconImageView.image = editedImage;
//    }else
    if (imgPickerTag == 3) {
        [self sendAddGalleryImageIntoStoreImageRequest:editedImage];
    }
    imgPickerTag = 0;
    if (newMedia)
        UIImageWriteToSavedPhotosAlbum(editedImage,
                                       self,
                                       @selector(image:finishedSavingWithError:contextInfo:),
                                       nil);
}

-(void)sendAddGalleryImageIntoStoreImageRequest:(UIImage *)pickerimage
{
    [CommonCallback showProgressHud:HUD_TITLE subtitle:HUD_SUBTITLE];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:LIVE_SERVER]];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    [httpClient setDefaultHeader:@"X-API-KEY" value:API_KEY];
//    if((recceId != nil) && ![recceId isEqualToString:@""])
//    {
//        [httpClient setDefaultHeader:@"X-HTTP-Method-Override" value:@"PUT"];
//    }
    [httpClient setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    [httpClient setDefaultHeader:@"Accept" value:@"application/json"];
    
    NSMutableDictionary *postparams = [[NSMutableDictionary alloc] init];
    /////////////////////////////////////////////////////////////////
    [postparams setObject:passedstoreid forKey:@"store_id"];
    [postparams setObject:passeddesignid forKey:@"design_id"];
    if((recceId != nil) && ![recceId isEqualToString:@""])
    {
        [postparams setObject:recceId forKey:@"recce_id"];
    }
    [postparams setObject:[PIMDGlobalOperations getFrontlineMemberId] forKey:@"member_id"];
    [postparams setObject:[PIMDGlobalOperations getFrontlineMemberAuthId] forKey:@"auth_id"];
    //////////////////////////////////////////////////////////
    
    
    NSMutableDictionary *fileparams = [[NSMutableDictionary alloc] init];
    bool result = CGSizeEqualToSize(pickerimage.size, CGSizeZero);
    if(result==0) //Not empty
    {
        NSData *dataObj = UIImageJPEGRepresentation(pickerimage, 0.75);
        NSString *image = [PIMD_APP_DELEGATE base64forData:dataObj];
        [fileparams setObject:image forKey:@"userfile"];
        [fileparams setObject:@"post.jpg" forKey:@"filename"];
        [fileparams setObject:@"image/jpeg" forKey:@"filetype"];
        [fileparams setObject:[NSNumber numberWithInt:selectedRow] forKey:@"filecount"];
        NSMutableArray *fileArray = [[NSMutableArray alloc] initWithCapacity:1];
        [fileArray addObject:fileparams];
        DebugLog(@"%@",fileArray);
        
        [postparams setObject:fileArray forKey:@"file"];
    }
    /////////////////////////////////////////////////////////////////
    DebugLog(@"----%@------",postparams);
    /////////////////////////////////////////////////////////////////
    [httpClient postPath:ADD_GALLERY parameters:postparams success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [CommonCallback hideProgressHud];
        NSError* error;
        NSDictionary* json = [NSJSONSerialization  JSONObjectWithData:responseObject  options:kNilOptions error:&error];
        DebugLog(@"json afnetworking ->%@",json);
        if(json != nil && [[json  objectForKey:@"success"] isEqualToString:@"true"])
        {
            [PIMDGlobalOperations showAlert:[json  objectForKey:@"message"]];
            NSDictionary* data = [json objectForKey:@"data"];
            DebugLog(@"%@",[data objectForKey:@"recce_id"]);
            recceId = [data objectForKey:@"recce_id"];
            //NSString * lstoreid = [data objectForKey:@"store_id"];
            //DebugLog(@"%@ %@",lrecceid,lstoreid);
            UITableViewCell *cell = [uploadImageTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:selectedRow - 1 inSection:0]];
            
            UIView *backView = (UIView *)[cell viewWithTag:101];
            UIImageView *thumbImg = (UIImageView *)[backView viewWithTag:103];
            thumbImg.image = pickerimage;
            
            
            
        }
        /*
        else if(json != nil && [[json  objectForKey:@"success"] isEqualToString:@"false"]) {
            if([json  objectForKey:@"code"] != nil &&  [[json  objectForKey:@"code"] isEqualToString:MERCHANT_INVALID_USER_AUTH_CODE])
            {
                if([INUserDefaultOperations isMerchantLoggedIn]){
                    [PIMDGlobalOperations showAlert:MERCHANT_INVALID_USER_AUTH_MESSAGE];
                    [INUserDefaultOperations clearMerchantDetails];
                    [self performSelector:@selector(popAddUpdateController) withObject:nil afterDelay:2.0];
                }
            } else {
                [PIMDGlobalOperations showAlert:[json  objectForKey:@"message"]];
            }
        } */
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DebugLog(@"[HTTPClient Error]: %@", error.localizedDescription);
        [CommonCallback hideProgressHud];
    }];
    
}

#pragma mark - Table view data source
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 110;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UIImageView *thumbImg;
    UILabel *lblTitle;
    UIView *backView;
   // UILabel *lblDesc;

    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.backgroundColor = [UIColor clearColor];
        
        backView = [[UIView alloc] initWithFrame:CGRectMake(0, 2, 280, 106)];
        backView.tag = 101;
        backView.backgroundColor    =  [UIColor whiteColor];
        backView.clipsToBounds      = YES;
        [cell.contentView addSubview:backView];

    
        //    PIMDDesignObject *designObj = nil;
        //    designObj = [designArray objectAtIndex:indexPath.row];
    
        //cell.textLabel.text = @"RISHI";
    
        thumbImg = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,106.0,106.0)];
        thumbImg.tag = 103;
        thumbImg.image = [UIImage imageNamed:@"add_image.png"];
        thumbImg.contentMode = UIViewContentModeScaleAspectFill;
        [backView addSubview:thumbImg];
        
        lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(thumbImg.frame)+10,30,230.0,40.0)];
        lblTitle.text = [NSString stringWithFormat:@"Add Image %ld", (long)indexPath.row+1];
        lblTitle.tag = 102;
        lblTitle.font = DEFAULT_FONT(20);
        lblTitle.textAlignment = NSTextAlignmentLeft;
        lblTitle.textColor              = DEFAULT_COLOR;
        lblTitle.highlightedTextColor   = [UIColor whiteColor];
        lblTitle.backgroundColor        = [UIColor clearColor];
        lblTitle.numberOfLines = 2;
        [backView addSubview:lblTitle];
        
    }
    
//    lblDesc = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(thumbImg.frame)+5, CGRectGetMaxY(lblTitle.frame) - 5,230.0,60.0)];
//    lblDesc.text = @"";
//    lblDesc.tag = 103;
//    lblDesc.numberOfLines = 3;
//    lblDesc.textAlignment = NSTextAlignmentLeft;
//    lblDesc.textColor = [UIColor blackColor];
//    lblDesc.highlightedTextColor = [UIColor whiteColor];
//    lblDesc.backgroundColor =  [UIColor clearColor];
//    [cell addSubview:lblDesc];

    
    
//    if(designObj.thumbUrl != nil && ![designObj.thumbUrl isEqualToString:@""])
//    {
//        __weak UIImageView *thumbImg_ = cell.imageView;
//        [thumbImg_ setImageWithURL:[NSURL URLWithString:THUMBNAIL_LINK(designObj.thumbUrl)] placeholderImage:[UIImage imageNamed:@"Shoplocal_Post_Placeholder.jpg"] completed:^(UIImage *image,NSError *error, SDImageCacheType cacheType) {
//            if (error) {
//                thumbImg_.image = [UIImage imageNamed:@"Shoplocal_Post_Placeholder.jpg"];
//            }
//        }];
//    } else {
//        cell.imageView.image = [UIImage imageNamed:@"Shoplocal_Post_Placeholder.jpg"];
//    }
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    selectedRow = indexPath.row + 1;
    [self addNewImage];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (IBAction)nextBtnPressed:(id)sender {
    PIMDCommentsSubmitViewController *storelistController = [[PIMDCommentsSubmitViewController alloc] initWithNibName:@"PIMDCommentsSubmitViewController" bundle:nil] ;
    storelistController.title = @"Store Status";
    storelistController.passedstoreid = passedstoreid;
    storelistController.passeddesignid = passeddesignid;
    if((recceId != nil) && ![recceId isEqualToString:@""])
    {
        storelistController.recceId = recceId;
    }
    [self.navigationController pushViewController:storelistController animated:YES];
}

@end
