//
//  CommonCallback.h
//  ImageOrganiser
//
//  Created by Kirti Nikam on 13/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MMProgressHUD.h"
#import "MMProgressHUDOverlayView.h"

@interface CommonCallback : NSObject

+(BOOL) use24HourClock;

#pragma UIView
+(UIImage *)getFlatImage:(UIColor *)color;
+(void)roundeWithBorder:(UIView *)view;
+(void)addShadow:(UIView *)view;

#pragma UIImage
+(UIImage *)separatorImage;
+(UIImage *)fixOrientation:(UIImage *)oldImage;
+(UIImage *)resizeImage:(UIImage*)image newSize:(CGSize)newSize;

#pragma HUD
+(void)showProgressHud:(NSString *)title subtitle:(NSString *)subTitle;
+(void)showProgressHudWithCancel:(NSString *)title subtitle:(NSString *)subTitle;
+(void)hideProgressHudWithSuccess;
+(void)hideProgressHudWithError;
+(void)hideProgressHud;

+(BOOL)validateEmail: (NSString *) email;

+(void)viewtransitionInCompletion:(UIView *)view completion:(void(^)(void))lcompletion;
+(void)viewtransitionOutCompletion:(UIView *)view completion:(void(^)(void))completion;
+(void)teardown:(UIView *)view;

@end
