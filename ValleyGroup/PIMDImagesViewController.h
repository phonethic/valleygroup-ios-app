//
//  PIMDImagesViewController.h
//  ValleyGroup
//
//  Created by Rishi on 22/04/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VCGridView.h"
#import "MWPhotoBrowser.h"

@interface PIMDImagesViewController : UIViewController <VCGridViewDelegate, VCGridViewDataSource, MWPhotoBrowserDelegate>
{
    VCGridView *_gridView;
	BOOL _isInputViewVisible;
}
@property (nonatomic, copy) NSString *passedstoreid;
@property (strong, nonatomic) NSMutableArray *galleryArray;
@property (strong, nonatomic) NSMutableArray *photos;
@property (nonatomic, readwrite) NSInteger storeId;
@property (nonatomic) BOOL newMedia;
@property (nonatomic,strong) UIImage *editedImage;
@property (strong, nonatomic) IBOutlet UIButton *moreBtn;

@property (nonatomic, retain) VCGridView *gridView;

- (IBAction)moreBtnPressed:(id)sender;
@end
