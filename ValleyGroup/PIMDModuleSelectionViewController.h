//
//  PIMDModuleSelectionViewController.h
//  ValleyGroup
//
//  Created by Rishi on 30/04/14.
//  Copyright (c) 2014 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PIMDModuleSelectionViewController : UIViewController

- (IBAction)installationModuleBtnPressed:(id)sender;
- (IBAction)recceModuleBtnPressed:(id)sender;
@end
